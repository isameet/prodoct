[CmdletBinding()]
Param([string]$serviceName)

$cur_dir = $(Split-Path $script:MyInvocation.MyCommand.Path)
$service = Get-Service -Name $serviceName
if ($service.Status -eq "Running"){
    Write-Output "Service $serviceName is already running"
    return
}

Write-Output "Press Ctrl+C to stop this script and prevent $serviceName from starting"

$timer = 60
[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")
$notification = New-Object System.Windows.Forms.NotifyIcon 
$notification.Icon = "$cur_dir\favicon.ico"
$notification.BalloonTipIcon = "Error" 
$notification.BalloonTipText = "$serviceName will be started in $timer seconds" 
$notification.BalloonTipTitle = "Service is stopped"
$notification.Visible = $True 
$notification.ShowBalloonTip(10000)

do {
    Write-Host -ForeGroundColor green -noNewLine "$serviceName will be started in $timer seconds" 
    $pos = $host.UI.RawUI.get_cursorPosition()
    $pos.X = 0
    $host.UI.RawUI.set_cursorPosition($Pos)

    Start-Sleep -Seconds 1 
    $timer = $timer - 1
    Clear-Host 
    
    if ($timer % 10 -eq 0){
        $notification.BalloonTipText = "$serviceName will be started in $timer seconds" 
        $notification.Visible = $True 
        $notification.ShowBalloonTip(10000)
    }
} while ($timer -gt 0)

$service = Get-Service -Name $serviceName
if ($service.Status -ne "Running"){
    Start-Service $serviceName
}

$notification.Dispose()
Write-Output "Service $serviceName started"
