Set-StrictMode -Version latest
Import-Module "$(Split-Path $script:MyInvocation.MyCommand.Path)\AWSPowerShell\AWSPowerShell.psd1"

function Read-Settings {
    param([string]$settings_file)

    Write-Verbose "Reading settings from $settings_file"
    if ((Test-Path $settings_file) -eq $False) {
        Write-Error "$settings_file doesn't exist. Cannot read settings."
        Break
    }
    $settings_dict = @{}
    Get-Content $settings_file | % { $terms = $_.split(':', 2); if ($terms.length -ne 2) { return; } $settings_dict.add($terms[0].trim(), $terms[1].trim()); }
    $settings = new-object System.Object
    $settings | add-member -notepropertymembers $settings_dict
    $settings
}

function Backup-Database ($settings, $out) {
    if ($settings.mongodb_bin -eq $null -or (Test-Path $settings.mongodb_bin) -eq $false) {
        Write-Error "$($settings.mongodb_bin) does not exist. Cannot find path to mongodump.exe."
        Break
    }
    if ($settings.backup_path -eq $null -or (Test-Path $settings.backup_path) -eq $false) {
        New-Item $settings.backup_path -ItemType directory
    }
    
    Remove-DatabaseBackupTemp $settings
    
    Write-Verbose "Backing up the database"
    $backup_command = "$($settings.mongodb_bin)\mongodump.exe --db $($settings.database) --out $($settings.backup_path)"
    iex $backup_command
}

function Archive-DatabaseBackup ($settings) {
    $src = "$($settings.backup_path)\$($settings.database)"
    $date_format = 'yyyy_MM_dd'
    $date = get-date
    $date_str = $date.tostring($date_format)
    $date_ticks = $date.ticks
    $archive_name = "prodoc-db-$date_str.7z"

    Archive-Folder $src $settings.backup_path $archive_name
}

function Archive-Folder ($src, $dest_path, $archive_name) {
    $script_dir = Split-Path $script:MyInvocation.MyCommand.Path
    $tools_dir = Join-Path $script_dir '..\tools'
    if ((Test-Path $src) -eq $false) {
        Write-Error "$src is not a valid source directory"
        Break
    }
    if ((Test-Path $dest_path) -eq $false) {
        Write-Error "$dest_path is not a valid destination directory"
        Break
    }
    if ([string]::IsNullOrWhiteSpace($archive_name) -eq $true) {
        Write-Error "$archive_name is not a valid archive name"
        Break
    }
    
    $settings | Add-Member  -NotePropertyName "Archive" -NotePropertyValue "$dest_path\$archive_name"
    Write-Verbose "Archiving the folder"
    $archive_command = "$tools_dir\7za.exe a -t7z $($settings.Archive) $src"
    iex $archive_command
}

function Upload-DbToS3 ($settings) {
    
    $bucket = 'firmzen-data'
    $keyPrefix = 'db-backups\'
    $ak = $settings.firmzen_data_ak
    $sk = $settings.firmzen_data_sk
    $fileName = [System.IO.Path]::GetFileName($settings.Archive)
    $key = "$keyPrefix$fileName"
    
    Write-Verbose "Uploading to S3 - $bucket->$key"
    
    Write-S3Object -Bucket $bucket -Key $key -File $settings.Archive -AccessKey $ak -SecretKey $sk
}

function Delete-OldArchives ($settings) {
    $dir = Split-Path $settings.Archive
    $thr = (Get-Date).AddDays(-1).Date
    
    Write-Verbose "Deleting archives older than $thr"
    
    Get-ChildItem -Path $dir -Filter *.7z | Where LastWriteTime -lt $thr | Remove-Item
	
	Remove-DatabaseBackupTemp $settings
}

function Remove-DatabaseBackupTemp ($settings) {
    $src = "$($settings.backup_path)\$($settings.database)"
    Remove-Folder $src
}

function Remove-Folder ($path) {
    if ((Test-Path $path) -eq $false) {
        return
    }
    Write-Verbose "Deleting redundant folder: $path"
    Remove-Item $path -recurse
}