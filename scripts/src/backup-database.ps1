﻿. "$(Split-Path $script:MyInvocation.MyCommand.Path)\common.ps1"

function BackupAndArchiveToS3(){
    [CmdletBinding()]
    $VerbosePreference = "Continue"
    
    $script_dir = Split-Path $script:MyInvocation.MyCommand.Path
    $settings = Read-Settings "$script_dir\settings.txt"
    if ($settings -eq $null) {
        Write-Error "Could not read settings"
		return
    }
    Backup-Database $settings
    $dump = "$($settings.backup_path)\$($settings.database)"
    if ((Test-Path $dump) -eq $False){
        Write-Error "Did not dump database backup"
        Exit
    }
    Archive-DatabaseBackup $settings
    #Upload-DbToS3 $settings
    Delete-OldArchives $settings
}

BackupAndArchiveToS3