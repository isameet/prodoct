﻿using ProdoctBgService.Framework;
using ProdoctCommon.Logging;
using System.ServiceProcess;

namespace ProdoctBgService
{
    [WindowsService("ProdoctBgService",
      DisplayName = "ProdoctBgService",
      Description = "prodoc background service",
      EventLogSource = "ProdoctBgService",
      StartMode = ServiceStartMode.Automatic)]
    public class Service : IWindowsService
    {
        private LinkSender _linkSender;
        private InventoryTasks _inventoryTasks;

        public void Dispose()
        {
        }

        public void OnStart(string[] args)
        {
            _linkSender = new LinkSender();
            _linkSender.Start();

            _inventoryTasks = new InventoryTasks();
            _inventoryTasks.Start();
        }

        public void OnStop()
        {
        }

        public void OnPause()
        {
        }

        public void OnContinue()
        {
        }

        public void OnShutdown()
        {
        }

        public void OnCustomCommand(int command)
        {
        }
    }
}
