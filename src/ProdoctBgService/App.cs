﻿using ProdoctCommon.Logging;

namespace ProdoctBgService
{
    public class App
    {
        public static ILogger Log
        {
            get
            {
                if (_log == null)
                    _log = new NLogLogger(typeof(Service));

                return _log;
            }
        }

        private static ILogger _log;
    }
}
