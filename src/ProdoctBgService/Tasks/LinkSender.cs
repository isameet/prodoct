﻿using ProdoctCommon.Data;
using ProdoctCommon.Email;
using ProdoctCommon.Extensions;
using ProdoctCommon.Helpers;
using System;
using System.Linq;
using System.Timers;

namespace ProdoctBgService
{
    internal class LinkSender
    {
        private readonly Timer _timer;
        private bool _busy, _oneErrorRaised;
        private readonly ProdoctDatabase DB;
        private readonly Links _links;
        private readonly Mandrill _email;
        public LinkSender(int timerInterval = 5000)
        {
            _timer = new Timer(timerInterval);
            _timer.Elapsed += TimerElapsed;

            DB = new ProdoctDatabase(new DatabaseContext());
            _links = DB.Links;

            _email = new Mandrill(App.Log);
        }

        public void Start()
        {
            _timer.Enabled = true;
            App.Log.Info(string.Format("LinkSender Started. Interval : {0}", TimeSpan.FromMilliseconds(_timer.Interval)));
        }

        private void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            if (_busy)
                return;

            _busy = true;

            try
            {
                RunBackgroundTasks();
            }
            catch (Exception ex)
            {
                if (_oneErrorRaised)
                {
                    App.Log.Debug(string.Format("LinkSender Repeat Error!\n{0}", ex.Message));
                }
                else
                {
                    _oneErrorRaised = true;
                    App.Log.Fatal("LinkSender Error!", ex);
                }
            }

            _busy = false;
        }

        private void RunBackgroundTasks()
        {
            var links = _links.Get().Where(x => x.ToBeSent).Take(100).ToList();
            if (links.Count > 0)
                App.Log.Info(string.Format("Processing {0} Links", links.Count));

            foreach(var link in links)
            {
                switch (link.Type)
                {
                    case LinkType.StaffInvitation:
                        ProcessStaffInvitationLink(link);
                        continue;

                    case LinkType.UserEmailVerify:
                        ProcessUserEmailVerify(link);
                        continue;

                    case LinkType.ChangePassword:
                        ProcessChangePassword(link);
                        continue;

                    default:
                        App.Log.Error(string.Format("Invalid Link Type\n{0}", link.ToJsonIndented()));
                        ProcessErroneousLink(link);
                        continue;
                }
            }
        }

        private void ProcessUserEmailVerify(Link link)
        {
            link.Result = _email.SendUserEmailVerify(link.Email, link.Globals);
            link.ToBeSent = false;
            link.SentOn = DateTime.Now;

            _links.Update(link);
        }

        private void ProcessErroneousLink(Link link)
        {
            link.Result = new StandardResult
            {
                Error = true,
                Message = string.Format("{0} : Erroneous Link", DateTime.Now.ToString(DateTimeFormats.DateTimePicker))
            };
            link.ToBeSent = false;

            _links.Update(link);
        }

        private void ProcessStaffInvitationLink(Link link)
        {
            link.Result = _email.SendStaffInvitation(link.Email, link.Globals);
            link.ToBeSent = false;
            link.SentOn = DateTime.Now;

            _links.Update(link);
            
        }

        private void ProcessChangePassword(Link link)
        {
            link.Result = _email.SendChangePassword(link.Email, link.Globals);
            link.ToBeSent = false;
            link.SentOn = DateTime.Now;

            _links.Update(link);
        }

        public void Stop()
        {
            _timer.Enabled = false;
        }
    }
}
