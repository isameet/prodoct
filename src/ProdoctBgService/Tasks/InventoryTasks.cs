﻿using MongoDB.Driver;
using ProdoctCommon.Data;
using ProdoctCommon.Email;
using ProdoctCommon.Extensions;
using ProdoctCommon.Helpers;
using System;
using System.Linq;
using System.Timers;

namespace ProdoctBgService
{
    internal class InventoryTasks
    {
        private readonly Timer _timer;
        private bool _busy, _oneErrorRaised;
        private readonly ProdoctDatabase DB;
        private readonly Inventory _inventory;
        private readonly InventoryHistory _history;

        public InventoryTasks(int timerInterval = 60000)
        {
            _timer = new Timer(timerInterval);
            _timer.Elapsed += TimerElapsed;

            DB = new ProdoctDatabase(new DatabaseContext());
            _inventory = DB.Inventory;
            _history = DB.InventoryHistory;
        }

        public void Start()
        {
            _timer.Enabled = true;
            App.Log.Info(string.Format("InventoryTasks Started. Interval : {0}", TimeSpan.FromMilliseconds(_timer.Interval)));
        }

        private void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            if (_busy)
                return;

            _busy = true;

            try
            {
                RunBackgroundTasks();
            }
            catch (Exception ex)
            {
                if (_oneErrorRaised)
                {
                    App.Log.Debug(string.Format("InventoryTasks Repeat Error!\n{0}", ex.Message));
                }
                else
                {
                    _oneErrorRaised = true;
                    App.Log.Fatal("InventoryTasks Error!", ex);
                }
            }

            _busy = false;
        }

        private void RunBackgroundTasks()
        {
            CheckForExpiredItems();
        }

        private void CheckForExpiredItems()
        {
            var items = _inventory.GetExpiringItems().Take(100).ToList();
            if (items.Count == 0)
                return;

            App.Log.Info(string.Format("Processing {0} Expired Items", items.Count));
            WriteConcernResult result = null;

            foreach(var item in items)
            {
                var batches = _inventory.GetExpiringBatches(item);
                foreach (var batch in batches)
                {
                    result = _history.Add(ItemHistory.Expired(item, batch));
                    if(result.HasError())
                    {
                        App.Log.Error(string.Format("Error recording expired history : {0}", result.ToLogFormat()));
                        continue;
                    }

                    item.Stock.Expire(batch);
                }

                result = _inventory.Update(item);
                if (result.HasError())
                {
                    App.Log.Error(string.Format("Error updating item after recording expired history : {0}", result.ToLogFormat()));
                    continue;
                }
            }
        }

        public void Stop()
        {
            _timer.Enabled = false;
        }
    }
}
