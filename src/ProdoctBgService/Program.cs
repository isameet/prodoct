﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using ProdoctCommon.Logging;

namespace ProdoctBgService.Framework
{
    internal static class Program
    {
        static void Main(string[] args)
        {
            try
            {
                // if install was a command line flag, then run the installer at runtime.
                if (args.Contains("-install", StringComparer.InvariantCultureIgnoreCase))
                {
                    WindowsServiceInstaller.RuntimeInstall<Service>();
                }
                // if uninstall was a command line flag, run uninstaller at runtime.
                else if (args.Contains("-uninstall", StringComparer.InvariantCultureIgnoreCase))
                {
                    WindowsServiceInstaller.RuntimeUnInstall<Service>();
                }
                // otherwise, fire up the service as either console or windows service based on UserInteractive property.
                else
                {
                    var implementation = new Service();
                    // if started from console, file explorer, etc, run as console app.
                    if (Environment.UserInteractive)
                    {
                        ConsoleHarness.Run(args, implementation);
                    }
                    else // otherwise run as a windows service
                    {
                        ServiceBase.Run(new WindowsServiceHarness(implementation));
                    }
                }
            }
            catch (Exception ex)
            {
                App.Log.Fatal("Error Starting Service", ex);
            }
        }
    }
}
