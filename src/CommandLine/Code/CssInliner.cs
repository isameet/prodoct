﻿using System;
using System.IO;

namespace CommandLine
{
    public static class CssInliner
    {
        public static void Run()
        {
            var files = new string[] 
            {
                "user-email-verify", 
                "staff-invitation",
                "change-password"
            };

            foreach (var file in files)
                Process(file);
        }

        private static void Process(string fileName)
        {
            Console.WriteLine("-------------------------------");
            Console.WriteLine("Inlining {0}\n", fileName);

            var source = string.Format(@"C:\Users\Sameet\Dev\prodoct\src\Email Templates\{0}.html", fileName);
            var destination = string.Format(@"C:\Users\Sameet\Dev\prodoct\src\Email Templates\css-inlined\{0}-inline.html", fileName);

            string sourceHtml = File.ReadAllText(source);
            var result = PreMailer.Net.PreMailer.MoveCssInline(sourceHtml, true);
            Console.WriteLine("WARNINGS\n");
            result.Warnings.ForEach(x => Console.WriteLine(x));
            Console.WriteLine("-------------------------------\n\n");

            File.WriteAllText(destination, result.Html);
        }
    }
}
