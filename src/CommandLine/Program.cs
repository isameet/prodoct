﻿using ProdoctCommon;
using ProdoctCommon.Data;
using ProdoctCommon.DataMigrations;
using ProdoctCommon.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace CommandLine
{
    class Program
    {
        static void Main(string[] args)
        {
            if (ShouldExecuteAllMigrations(args))
            {
                ExecuteAction(() => new MigrationExecutor().ExecuteAll());
                return;
            }

            if (ShouldExecuteRunAdhocCode(args))
            {
                ExecuteAction(RunAdhocCode);
                return;
            }
        }



        private static void RunAdhocCode()
        {
            //CssInliner.Run();
        }



        private static void ExecuteAction(Action action)
        {
            action();
            Console.WriteLine("Done. Press any key to exit.");
            Console.ReadKey(true);
        }

        private static bool ShouldExecuteAllMigrations(IEnumerable<string> args)
        {
            return args.Any(x => string.Equals(x, "-executeallmigrations", StringComparison.OrdinalIgnoreCase));
        }

        private static bool ShouldExecuteRunAdhocCode(IEnumerable<string> args)
        {
            return args.Any(x => string.Equals(x, "-runadhoccode", StringComparison.OrdinalIgnoreCase));
        }



        private static void TestRandomness(int limit = 1000)
        {
            List<string> keys = new List<string>();
            string token = null;
            limit += 1;

            for (var i = 1; i < limit; i++)
            {
                token = AuthenticationHelper.GenerateRandomToken();
                if (keys.Contains(token))
                {
                    Console.WriteLine("Duplicate generated on attempt number {0}", i);
                    break;
                }

                keys.Add(token);
                Console.WriteLine("{0}. {1} \t Regex Match - {2}", i, token, Regex.IsMatch(token, RegexFormats.Code16) ? "Yes" : "No");
            }
        }

        private static void SeedDatabase()
        {
            var DB = new ProdoctDatabase(new DatabaseContext());
            if (DB.Practices.Get().Count() > 0)
            {
                Console.WriteLine("Database already seeded!");
                return;
            }

            CreateNewAccount();
        }

        private static void CreateNewAccount()
        {
            var DB = new ProdoctDatabase(new DatabaseContext());
            var practiceName = "demo practice";
            var userName = "Dr. Sameet";
            var title = "Periodontist";
            var mobile = "+919029026183";
            var email = "demo@prodoc.in";
            var password = "password";

            #region practice

            var practice = new Practice
            {
                Id = ObjectIdHelper.GenerateNewIdString(),
                Name = practiceName,
                Timezone = "Asia/Kolkata"
            };
            DB.Practices.Add(practice);
            Console.WriteLine("1 Practice Created: {0}", practice.Name);

            DB.SetContext(new DatabaseContext
            {
                PracticeId = practice.Id
            });

            var context = new AppContext
            {
                DB = DB,
                PracticeId = practice.Id
            };

            #endregion

            #region users & doctors

            var user = new User
            {
                Id = ObjectIdHelper.GenerateNewIdString(),
                Name = userName,
                Mobile = mobile,
                CreatedOn = DateTime.Now,
                DefaultPracticeId = practice.Id,
                PracticeIds = new List<string> { practice.Id }
            };
            user.SetEmail(email, context);
            user.SetPassword(password);
            user.Verify();
            DB.Users.Add(user);
            Console.WriteLine("1 User Created: {0}", user.Email);

            var doctor = new Doctor
            {
                Id = ObjectIdHelper.GenerateNewIdString(),
                Name = userName,
                Mobile = mobile,
                Title = title,
                PracticeId = practice.Id,
                CreatorId = user.Id,
                Privileges = Privileges.GetAdminPrivileges(),
                CalendarSettings = CalendarSettings.GetDefault(),
                CreatedOn = DateTime.Now,
                CalendarColor = "#479bcb"
            };
            doctor.SetEmail(email, context);
            doctor.SetAllowLogin(true, context);
            doctor.AcceptInvitation();
            DB.Staffs.Add(doctor);
            Console.WriteLine("1 Doctor Created: {0} ({1})", doctor.Name, doctor.Title);

            #endregion
        }
    }
}
