﻿using ProdoctCommon.Data;
using ProdoctCommon.Extensions;
using ProdoctCommon.Helpers;
using System.Linq;
using System.Web.Mvc;

namespace ProdoctWebApp.Helpers
{
    public static class LinkHelper
    {
        #region staff invitations

        internal static bool ScheduleStaffInvitation(Staff staff, UrlHelper Url)
        {
            var link = new Link()
            {
                Email = staff.Email,
                PracticeId = APP.Common.PracticeId,
                StaffId = staff.Id,
                UserId = staff.UserId,
                ToBeSent = true,
                Type = LinkType.StaffInvitation,
                CreatorId = APP.Common.UserId
            };

            var url = Url.RouteUrl(RouteName.Link, new { code = link.Code, email = link.Email }, "http");
            link.Globals.Add("INVITATIONLINK", url);
            link.Globals.Add("PRACTICENAME", APP.Common.Practice.Name);

            var result = APP.Common.DB.Links.Add(link).ToStandardResult();
            if (result.Error)
            {
                APP.Common.Log.Error(string.Format("Error adding Link to DB : {0}\n{1}", result.Message, link.ToJsonIndented()));
                return false;
            }

            return !result.Error;
        }

        internal static void CancelStaffInvitations(Staff staff)
        {
            var links = APP.Common.DB.Links.Get().Where(x => x.StaffId == staff.Id &&
                                                          x.Type == LinkType.StaffInvitation &&
                                                          x.ToBeSent).ToList();

            //why can there be multiple invites for one stafF?
            //new staff added - email id abc@xyz.com - this generates one invite
            //newly added staff's email id changed to abd@xyz.com - this generates another invite

            foreach (var link in links)
                APP.Common.DB.Links.Remove(link);
        }

        #endregion

        internal static bool ScheduleUserEmailVerify(User user, UrlHelper Url)
        {
            var link = new Link()
            {
                Email = user.Email,
                UserId = user.Id,
                ToBeSent = true,
                Type = LinkType.UserEmailVerify,
                CreatorId = APP.Common.UserId
            };

            var url = Url.RouteUrl(RouteName.Link, new { code = link.Code, email = link.Email }, "http");
            link.Globals.Add("VERIFICATIONLINK", url);

            var result = APP.Common.DB.Links.Add(link).ToStandardResult();
            if (result.Error)
            {
                APP.Common.Log.Error(string.Format("Error adding Link to DB : {0}\n{1}", result.Message, link.ToJsonIndented()));
                return false;
            }

            return true;
        }

        internal static bool ScheduleChangePassword(User user, UrlHelper Url)
        {
            var link = new Link()
            {
                Email = user.Email,
                UserId = user.Id,
                ToBeSent = true,
                Type = LinkType.ChangePassword,
                CreatorId = APP.Common.UserId
            };

            var url = Url.RouteUrl(RouteName.Link, new { code = link.Code, email = link.Email }, "http");
            link.Globals.Add("CHANGEPASSWORDLINK", url);

            var result = APP.Common.DB.Links.Add(link).ToStandardResult();
            if (result.Error)
            {
                APP.Common.Log.Error(string.Format("Error adding Link to DB : {0}\n{1}", result.Message, link.ToJsonIndented()));
                return false;
            }

            return true;
        }
    }
}