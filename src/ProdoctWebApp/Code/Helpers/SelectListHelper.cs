﻿using ProdoctCommon.Data;
using ProdoctCommon.Extensions;
using ProdoctCommon.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace ProdoctWebApp.Helpers
{
    public static class SelectListHelper
    {

        internal static List<SelectListItem> GetDoctors()
        {
            var doctors = APP.Common.DB.Staffs.Get<Doctor>(new string[] { "Name" })
                                           .Select(x => new SelectListItem { Text = x.Name, Value = x.Id })
                                           .ToList();

            doctors.Insert(0, new SelectListItem { Text = "Select a Doctor", Value = string.Empty });

            return doctors;
        }

        internal static List<SelectListItem> GetTimezones()
        {
            return DateTimeHelper.GetTimezones().Select(x => new SelectListItem
            {
                Text = x.Text,
                Value = x.Value
            }).ToList();
        }

        internal static List<SelectListItem> GetStaffRoles()
        {
            var roles = new List<SelectListItem>();

            foreach (StaffRole type in Enum.GetValues(typeof(StaffRole)))
            {
                if (type == StaffRole.Staff)
                    continue;

                roles.Add(new SelectListItem { Value = ((int)type).ToString(), Text = type.ToString() });
            }

            roles.Insert(0, new SelectListItem { Text = "Select a Role", Value = string.Empty });
            return roles;
        }

        internal static List<SelectListItem> GetIncomeCategories()
        {
            var practice = APP.Common.Practice;
            if (practice == null || practice.IncomeCategories == null || practice.IncomeCategories.Count == 0)
                return new List<SelectListItem>();

            return practice.IncomeCategories.Select(x => new SelectListItem
            {
                Value = x,
                Text = x
            }).ToList();
        }

        internal static List<SelectListItem> GetExpenseCategories()
        {
            var practice = APP.Common.Practice;
            if (practice == null || practice.ExpenseCategories == null || practice.ExpenseCategories.Count == 0)
                return new List<SelectListItem>();

            return practice.ExpenseCategories.Select(x => new SelectListItem
            {
                Value = x,
                Text = x
            }).ToList();
        }

        internal static List<SelectListItem> GetIncomeOrExpenseModes()
        {
            var practice = APP.Common.Practice;
            if (practice == null || practice.TransactionModes == null || practice.TransactionModes.Count == 0)
                return new List<SelectListItem>();

            return practice.TransactionModes.Select(x => new SelectListItem
            {
                Value = x,
                Text = x
            }).ToList();
        }

        internal static List<SelectListItem> GetItemTypes()
        {
            return new List<SelectListItem>
            {
                new SelectListItem { Text = "Drug", Value = Item.ItemType.Drug.ToString() }
            };
        }

        internal static List<SelectListItem> GetStockConsumptionTypes()
        {
            return new List<SelectListItem>
            {
                new SelectListItem { Text = "Sales", Value = "Sales" },
                new SelectListItem { Text = "Returned", Value = "Returned" }
                //new SelectListItem { Text = "Expired", Value = "Expired" }
            };
        }

        internal static List<SelectListItem> GetBatches(Item item)
        {
            var list = new List<SelectListItem>();
            var batches = item.Stock == null ? null : item.Stock.Batches;
            if (batches == null || batches.Count == 0)
                return list;

            var timezone = APP.Common.Practice.Timezone;
            var nowInZone = DateTimeHelper.NowInZone(timezone);
            list = batches.Where(x => x.Available > 0 && x.Expiry > nowInZone)
                           .Select(x => new SelectListItem { 
                               Value = x.Id,
                               Text = string.Format("{0} ({1} available). Exp: {2}", 
                                                    x.Name, 
                                                    x.Available, 
                                                    x.Expiry.InTimezone(timezone).ToString(DateTimeFormats._Mar87))
                           })
                           .ToList();

            return list;
        }

        internal static List<SelectListItem> GetAvailableMedicines()
        {
            var list = APP.Common.DB.Inventory.GetAvailableMedicines()
                          .Select(x => new SelectListItem
                           {
                              Text = string.Format("{0} ({1} available)", x.Name, x.Stock.Available),
                              Value = x.Id
                           }).ToList();

            list.Insert(0, new SelectListItem { Value = string.Empty, Text = "Select Medicine ..." });
            return list;
        }
    }
}