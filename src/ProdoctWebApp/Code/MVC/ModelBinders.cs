﻿using ProdoctCommon.Helpers;
using ProdoctWebApp.Models;
using System.Web.Mvc;

namespace ProdoctWebApp.MVC
{
    public class CalendarRequestModelBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            try
            {
                var request = controllerContext.HttpContext.Request;
                return new CalendarRequestModel
                {
                    Start = DateTimeHelper.FromCalendarRequestModel(request["start"]),
                    End = DateTimeHelper.FromCalendarRequestModel(request["end"]),
                    DoctorId = request["doctorId"]
                };
            }
            catch
            {
                return null;
            }
        }
    }

    public class TrimModelBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var shouldPerformRequestValidation = controllerContext.Controller.ValidateRequest && 
                                                 bindingContext.ModelMetadata.RequestValidationEnabled;

            var valueResult = bindingContext.GetValueFromValueProvider(shouldPerformRequestValidation);
            if (valueResult == null || string.IsNullOrEmpty(valueResult.AttemptedValue))
                return null;
            return valueResult.AttemptedValue.Trim();
        }
    }
}