﻿using ProdoctCommon.Helpers;
using System.Collections;
using System.Web;

namespace ProdoctWebApp
{
    public class APP
    {
        private APP() { }

        public static AppContext Common
        {
            get
            {
                IDictionary items = HttpContext.Current.Items;
                if (!items.Contains("ProdoctMvcContext"))
                {
                    items["ProdoctMvcContext"] = new AppContext();
                }
                return items["ProdoctMvcContext"] as AppContext;
            }
        }
    }
}