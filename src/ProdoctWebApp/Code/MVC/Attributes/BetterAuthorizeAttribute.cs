﻿using ProdoctCommon.Data;
using ProdoctCommon.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ProdoctWebApp.MVC
{
    public class BetterAuthorizeAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (AllowUnauthorized(filterContext))
                filterContext.HttpContext.Items.Add("AllowUnauthorized", true);

            base.OnAuthorization(filterContext);
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var authorized = base.AuthorizeCore(httpContext);
            if (!authorized)
                return false;

            var userId = httpContext.User.Identity.Name;
            InitAppContext(userId, ref httpContext);

            if (APP.Common.IsUnauthorized())
            {
                if (AllowUnauthorized(httpContext))
                    return true;

                return false;
            }

            return true;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            var httpContext = filterContext.HttpContext;

            if (httpContext.Request.IsAjaxRequest())
            {
                httpContext.Response.SuppressFormsAuthenticationRedirect = true;
                httpContext.Response.TrySkipIisCustomErrors = true;
                base.HandleUnauthorizedRequest(filterContext);
                return;
            }

            if (httpContext.Request.IsAuthenticated && APP.Common.IsUnauthorized())
            {
                filterContext.Result = new RedirectToRouteResult(RouteName.Settings, null);
                return;
            }

            base.HandleUnauthorizedRequest(filterContext);
        }




        private void InitAppContext(string userId, ref HttpContextBase httpContext)
        {
            if (string.IsNullOrWhiteSpace(userId) || !ObjectIdHelper.IsObjectId(userId))
                return;

            var user = APP.Common.DB.Users.GetById(userId);
            if (user == null)
                return;

            APP.Common.PracticeId = user.DefaultPracticeId;
            APP.Common.User = user;
            APP.Common.DB.SetContext(new DatabaseContext { PracticeId = user.DefaultPracticeId });
        }




        private bool AllowUnauthorized(AuthorizationContext filterContext)
        {
            return filterContext.ActionDescriptor.IsDefined(typeof(AllowUnauthorizedAttribute), true) ||
                   filterContext.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(AllowUnauthorizedAttribute), true);
        }

        private bool AllowUnauthorized(HttpContextBase httpContext)
        {
            if (httpContext.Items.Contains("AllowUnauthorized"))
                return true;

            return false;
        }
    }
}