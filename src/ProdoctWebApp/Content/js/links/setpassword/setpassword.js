﻿var pr = pr || {};

pr.setpassword = (function () {
    var controlId = '#btnSetPassword',
        events = {};

    function init() {
        setTimeout(function () { $('#NewPassword').focus(); }, 300);
        $('form').submit(formSubmit);
    }

    function formSubmit() {
        if ($(this).valid() == false)
            return;

        var $btn = $('#btnSetPassword');
        $btn.removeClass('btn-success').addClass('btn-default');
        $btn.text('Setting Password ...');
        $btn.prop('disabled', true);
    }

    return {
        init: init,
        events: events
    };
})();