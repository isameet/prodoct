﻿var pr = pr || {};
pr.autocomplete = pr.autocomplete || {};

pr.autocomplete.patients = (function () {
    var containerId = '';

    function getDefaults() {
        return  {
            onSelect: onSelect,
            transformResult: transformResult,
            deferRequestBy: 200,
            preserveInput: true,
            minChars: 2
        };
    }

    function init(cId) {
        $(document).on('click', '.patient-change', changePatientClick);
        containerId = cId;
    }

    function load(id, params) {
        $(id).autocomplete($.extend({}, getDefaults(), params));
    }

    function onSelect(patient) {
        renderPatient(patient);
        $('.doctor-id').focus();
    }

    function renderPatient(patient) {
        var $container = $(containerId);

        $container.find('.patient-input').hide();
        $container.find('.patient-summary').show();
        $container.find('.patient-input').find('input').val('');

        var $summary = $container.find('.patient-summary-content');
        $container.find('.patient-id').val(patient.data.Id);
        $summary.find('.patient-name').text(patient.data.Name);
        $summary.find('.patient-mobile').text(patient.data.Mobile || '');
        $summary.find('.patient-locality').text(patient.data.Locality || '');
    }

    function changePatientClick() {
        var $container = $(containerId);
        $container.find('.patient-id').val('');
        $container.find('.patient-summary').hide();
        $container.find('.patient-input').show();
        $container.find('.patient-name-input').focus();
    }

    function transformResult(response) {
        response = JSON.parse(response);
        return {
            suggestions: $.map(response.Patients, function (patient) {
                return {
                    value: getHtml(patient),
                    data: patient
                };
            })
        };
    }

    function getHtml(patient) {
        if (!patient || !patient.Name)
            return null;

        var html = patient.Name;
        if (patient.Locality) {
            html += ' (' + patient.Locality + ')';
        }

        if (patient.Mobile) {
            html += ' - ' + patient.Mobile;
        }

        return html;
    }

    return {
        init: init,
        load: load
    };
})();