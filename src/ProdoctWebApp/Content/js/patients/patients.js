﻿var pr = pr || {};

pr.patients = (function () {
    var events = {
        documentLoaded: 'documentLoaded',
        patientListLoaded: 'patientListLoaded'
    };

    function init() {
        list.init();
        deletePermanently.init();
    }

    var list = (function () {
        var nextSetClass = '.patients-next-set',
            events = {
                listLoadError: 'listLoadError'
            };

        function init() {
            $(document).on('click', '#patientsSort .list-item', sortByClick);
            $(document).on('click', '#patientsGetMore', getMoreClick);
            $('#patientsSearchForm').submit(searchFormSubmit);
        }

        function searchFormSubmit() {
            reload();
            return false;
        }

        function sortByClick() {
            reload();
        }

        function getMoreClick() {
            var data = getLoadParams();
            data.Skip = $(this).data('skip');
            load(data);
        }

        function reload() {
            $('#patients').html('<div class="patients-next-set"></div>');
            load();
        }

        function load(data) {
            var $target = $(nextSetClass),
                data = data || getLoadParams();

            $.ajax({
                url: '/Patients/List',
                method: 'POST',
                data: data,
                beforeSend: function () {
                    $target.html('<h3 class="patients-loading">Loading ...</h3>');
                },
                success: function (data) {
                    if (data.HasError) {
                        error(null, null, data.Message);
                        return;
                    }

                    if (data.Html) {
                        $target.replaceWith(data.Html);
                    }

                    if (data.Count) {
                        $('#allPatientsCount').text(data.Count);
                    }
                },
                error: error
            });
        }

        function error() {
            $(nextSetClass).empty();
            var message = arguments[2] || 'An Error Occured';
            $(document).trigger(events.listLoadError, [{ Message: message }]);
        }

        function getLoadParams() {
            var p = {
                SortBy: $('#patientsSort').find('.active:first').data('sort-by'),
                SearchTerm: $('#patientsSearch').val()
            };

            return p;
        }

        return {
            init: init,
            events: events,
            load: load,
            reload: reload,
            nextSetClass: nextSetClass
        };
    })();

    var add = (function () {
        var controlId = '#btnAddPatient',
            containerId = '#addPatientContainer',
            events = {
                patientAdded: 'patientAdded',
                addFormLoaded: 'addFormLoaded'
            };

        function before() {
            pr.form.ajaxBefore.call(this, {
                controlId: controlId,
                message: 'Adding Patient ...'
            });
        }

        function after(data) {
            var isError = !pr.form.ajaxAfter.apply(this, arguments);
            if (isError)
                return;

            $(document).trigger(events.patientAdded, [data]);
        }

        function error() {
            pr.form.ajaxError.apply(this, arguments);
        }

        return {
            before: before,
            after: after,
            error: error,
            events: events,
            containerId: containerId
        };
    })();

    var edit = (function () {
        var controlId = '#btnEditPatient',
            containerId = '#editPatientContainer',
            events = {
                patientEdited: 'patientEdited',
                editFormLoaded: 'editFormLoaded'
            };

        function before() {
            pr.form.ajaxBefore.call(this, {
                controlId: controlId,
                message: 'Updating ...'
            });
        }

        function after(data) {
            var isError = !pr.form.ajaxAfter.apply(this, arguments);
            if (isError)
                return;

            $(document).trigger(events.patientEdited, [data]);
        }

        function error() {
            pr.form.ajaxError.apply(this, arguments);
        }

        return {
            before: before,
            after: after,
            error: error,
            events: events,
            containerId: containerId,
            controlId: controlId
        };
    })();

    var deletePermanently = (function () {
        var events = {
            patientDeleted: 'patientDeleted'
        };

        function init() {
            $(document).on('click', '#deletePatient', deleteClick);
        }

        function deleteClick() {
            event.preventDefault();
            event.stopPropagation();

            if (pr.confirm('Delete this Patient Record permanently?') == false)
                return;

            var id = $(edit.containerId).find('#Id').val();
            deletePermanently(id);
        }

        function deletePermanently(id) {
            var $form = $(edit.containerId).find('form');

            $.ajax({
                url: '/Patients/Delete',
                data: { id: id },
                beforeSend: function () {
                    pr.form.ajaxBefore.call($form, {
                        controlId: edit.controlId,
                        message: 'Deleting ...'
                    });
                },
                success: function (data) {
                    var isError = !pr.form.ajaxAfter.apply($form, arguments);
                    if (isError)
                        return;

                    $(document).trigger(events.patientDeleted);
                },
                error: function () {
                    pr.form.ajaxError.apply($form, arguments);
                }
            });
        }

        return {
            init: init,
            events: events
        }
    })();

    $.extend(events, list.events, add.events, edit.events, deletePermanently.events);

    return {
        init: init,
        events: events,
        list: list,
        add: add,
        edit: edit,
        deletePermanently: deletePermanently
    };
})();