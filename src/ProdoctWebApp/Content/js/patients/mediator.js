﻿var pr = pr || {};
pr.patients = pr.patients || {};

pr.patients.mediator = (function () {
    function init() {
        var e = $.extend({}, pr.patients.events);

        $(document).on(e.documentLoaded, list.load);
        $(document).on(e.listLoadError, list.loadError);

        $(document).on(e.addFormLoaded, add.formLoaded);
        $(document).on(e.patientAdded, add.added);

        $(document).on(e.editFormLoaded, edit.formLoaded);
        $(document).on(e.patientEdited, edit.edited);

        $(document).on(e.patientDeleted, deletePermanently.deleted);
    }

    var list = (function () {
        function load(e, data) {
            pr.patients.list.load();
        }

        function loadError(e, data) {
            pr.flash.error(data.Message);
        }

        return {
            load: load,
            loadError: loadError
        };
    })();

    var add = (function () {
        function added(e, data) {
            pr.patients.list.reload();
            pr.modal.close();
        }

        function formLoaded(e, data) {
            pr.form.rebindValidation(pr.patients.add.containerId);
        }

        return {
            added: added,
            formLoaded: formLoaded
        };
    })();

    var edit = (function () {
        function edited(e, data) {
            pr.patients.list.reload();
        }

        function formLoaded(e, data) {
            pr.form.rebindValidation(pr.patients.edit.containerId);
        }

        return {
            edited: edited,
            formLoaded: formLoaded
        };
    })();

    var deletePermanently = (function () {
        function deleted(e, data) {
            pr.patients.list.reload();
            pr.modal.close();
        }

        return {
            deleted: deleted
        };
    })();

    return {
        init: init,
        list: list,
        add: add,
        edit: edit,
        deletePermanently: deletePermanently
    };
})();