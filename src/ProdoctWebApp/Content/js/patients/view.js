﻿//patients/view

var pr = pr || {};

$(document).ready(function () {
    //patients modules
    pr.patients.init();
    
    //commmon modules
    pr.collapsible.init();
    pr.modal.init();
    pr.flash.init();
    pr.form.init();
    pr.confirm.init();
    pr.extensions.init();
    pr.nav.init('#navPatients');
    pr.ajax.init();
    pr.helpers.init();

    //structure
    pr.patients.mediator.init();

    $(document).trigger(pr.patients.events.documentLoaded);
});