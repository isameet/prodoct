﻿var pr = pr || {};
pr.appointments = pr.appointments || {};

pr.appointments.mediator = (function () {
    function init() {
        var e = $.extend({ }, pr.appointments.events,
                              pr.calendar.events,
                              pr.modal.events);

        $(document).on(e.addAppointmentFormLoad, add.formLoad);
        $(document).on(e.appointmentAdded, add.appointmentAdded);

        $(document).on(e.doctorClicked, calendar.doctorClicked);
        $(document).on(e.calendarLoaded, calendar.loaded);
        $(document).on(e.calendarEventRendered, calendar.eventRendered);

        $(document).on(e.appointmentLoaded, edit.appointmentLoaded);
        $(document).on(e.appointmentEdited, edit.appointmentEdited);

        $(document).on(e.cancelationStatusChanged, cancel.statusChanged)
    }

    var add = (function () {
        function formLoad(e, data) {
            pr.datetimepicker.render(data);
        }

        function appointmentAdded(e, data) {
            pr.calendar.reload();
            pr.modal.close(data.modalId);
        }

        return {
            formLoad: formLoad,
            appointmentAdded: appointmentAdded
        }
    })();

    var calendar = (function () {
        function doctorClicked() {
            pr.calendar.reload();
        }

        function loaded(e, data) {
            pr.appointments.stats.reload(data.stats);
        }

        function eventRendered(e, data) {
            var $el = data.$el,
                event = data.event,
                modalId = pr.appointments.edit.modalId;

            $el.addClass('modal-link');
            $el.data('modal', modalId.replace('#', ''));
            $el.data('target', modalId);
            $el.attr('data-get', '/Appointments/Edit/' + event.id);
            $el.data('loading-message', 'Loading Appointment ...');
            $el.data('broadcast-on-load', 'appointmentLoaded');
        }

        return {
            doctorClicked: doctorClicked,
            loaded: loaded,
            eventRendered: eventRendered
        };
    })();

    var edit = (function () {
        function appointmentLoaded(e, data) {
            pr.datetimepicker.init(pr.appointments.edit.modalId);
        }

        function appointmentEdited(e, data) {
            pr.calendar.reload();
        }

        return {
            appointmentLoaded: appointmentLoaded,
            appointmentEdited: appointmentEdited
        };
    })();

    var cancel = (function () {
        function statusChanged(e, data) {
            pr.calendar.reload();
            pr.modal.close(data.modalId);
        }

        return {
            statusChanged: statusChanged
        };
    })();

    return {
        init: init
    };
})();