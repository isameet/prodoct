﻿//appointments/view

var pr = pr || {};

$(document).ready(function () {
    //commmon modules
    pr.collapsible.init();
    pr.modal.init();
    pr.datetimepicker.init();
    pr.flash.init();
    pr.form.init();
    pr.confirm.init();
    pr.extensions.init();
    pr.nav.init('#navAppointments');
    pr.ajax.init();
    pr.helpers.init();

    //appointments modules
    pr.appointments.init();
    pr.calendar.init();

    //structure
    pr.appointments.mediator.init();
});