﻿var pr = pr || {};

pr.appointments = (function () {
    var events = {};

    function init() {
        add.init();
        edit.init();
        cancel.init();
        autocomplete.init();
        stats.init();
        medicine.init();
    }

    var autocomplete = (function () {
        function init() {
            pr.autocomplete.patients.init(add.modalLink);
            pr.autocomplete.patients.load('#add_PatientName', { serviceUrl: '/Patients/Name' });
            pr.autocomplete.patients.load('#add_PatientMobile', { serviceUrl: '/Patients/Mobile', minChars: 4 });
        }

        return {
            init: init
        };
    })();

    var add = (function () {
        var modalLink = '#addAppointmentContainer',
            controlId = '#btnAddAppointment',
            events = {
                addAppointmentFormLoad: 'addAppointmentFormLoad',
                appointmentAdded: 'appointmentAdded'
            };

        function init() {
            $('.add-appointment').click(addAppointmentClick);
        }

        function before() {
            pr.form.ajaxBefore.call(this, {
                controlId: controlId,
                message: 'Saving ...'
            });
        }

        function after(data) {
            var isError = !pr.form.ajaxAfter.apply(this, arguments);
            if (isError) 
                return;

            $(document).trigger(events.appointmentAdded, [{ modalId: modalLink }]);
            resetAddUI();
        }

        function error() {
            pr.form.ajaxError.apply(this, arguments);
        }

        function addAppointmentClick() {
            renderAddUI();
        }

        function renderAddUI() {
            var $modal = $(modalLink);

            resetAddUI();

            $(document).trigger(events.addAppointmentFormLoad, [{
                datetimepicker: $modal.find('#add_AppointmentDate'),
                date: new Date()
            }]);
        }

        function resetAddUI() {
            var $modal = $(modalLink);
            $modal.find('form')[0].reset();
            $modal.find('#PatientId').val('');
            $modal.find('.patient-summary').hide();
            $modal.find('.patient-input').show();
        }

        return {
            init: init,
            events: events,
            before: before,
            after: after,
            error: error,
            modalLink: modalLink,
            controlId: controlId
        };
    })();

    var edit = (function () {
        var modalId = '#editAppointmentContainer',
            controlId = '#btnEditAppointment',
            events = {
                appointmentLoaded: 'appointmentLoaded',
                appointmentEdited: 'appointmentEdited'
            };

        function init() {
            $(document).on('click', '#addFees', addFeesClick);
        }

        function before() {
            pr.form.ajaxBefore.call(this, { message: 'Updating ...', controlId: controlId });
        }

        function after(data) {
            var isError = !pr.form.ajaxAfter.apply(this, arguments);
            if (isError)
                return;

            $(document).trigger(events.appointmentEdited);
            pr.modal.close(modalId);
        }

        function error() {
            pr.form.ajaxError.apply(this, arguments);
        }

        function addFeesClick(event) {
            event.preventDefault();
            event.stopPropagation();
        }

        return {
            init: init,
            modalId: modalId,
            controlId: controlId,
            events: events,
            before: before,
            after: after,
            error: error
        };
    })();

    var cancel = (function () {
        var events = {
            cancelationStatusChanged: 'cancelationStatusChanged'
        };

        function init() {
            $(document).on('click', '.cancel-appointment', cancelClick);
            $(document).on('click', '.confirm-appointment', confirmClick);
            $(document).on('click', '.delete-appointment', deleteClick);
        }

        function cancelClick(event) {
            event.preventDefault();
            event.stopPropagation();

            if (pr.confirm('Cancel this Appointment?') == false)
                return;

            var id = $(edit.modalId).find('#AppointmentId').val();
            cancelAppointment(id);
        }

        function cancelAppointment(id) {
            var $form = $(edit.modalId).find('form');

            $.ajax({
                url: '/Appointments/Cancel',
                data: { id: id },
                beforeSend: function () {
                    pr.form.ajaxBefore.call($form, {
                        controlId: edit.controlId,
                        message: 'Canceling ...'
                    });
                },
                success: function (data) {
                    var isError = !pr.form.ajaxAfter.apply($form, arguments);
                    if (isError)
                        return;

                    $(document).trigger(events.cancelationStatusChanged, [{ modalId: edit.modalId }]);
                },
                error: function () {
                    pr.form.ajaxError.apply($form, arguments);
                }
            });
        }

        function confirmClick(event) {
            event.preventDefault();
            event.stopPropagation();

            if (pr.confirm('Confirm this Appointment again?') == false)
                return;

            var id = $(edit.modalId).find('#AppointmentId').val();
            confirmAppointment(id);

        }

        function confirmAppointment(id) {
            var $form = $(edit.modalId).find('form');

            $.ajax({
                url: '/Appointments/Confirm',
                data: { id: id },
                beforeSend: function () {
                    pr.form.ajaxBefore.call($form, {
                        controlId: edit.controlId,
                        message: 'Confirming ...'
                    });
                },
                success: function (data) {
                    var isError = !pr.form.ajaxAfter.apply($form, arguments);
                    if (isError)
                        return;

                    $(document).trigger(events.cancelationStatusChanged, [{ modalId: edit.modalId }]);
                },
                error: function () {
                    pr.form.ajaxError.apply($form, arguments);
                }
            });
        }

        function deleteClick(event) {
            event.preventDefault();
            event.stopPropagation();

            if (pr.confirm('Are you sure you want to PERMANENTLY DELETE this Appointment?') == false)
                return;

            var id = $(edit.modalId).find('#AppointmentId').val();
            deleteAppointment(id);

        }

        function deleteAppointment(id) {
            var $form = $(edit.modalId).find('form');

            $.ajax({
                url: '/Appointments/Delete',
                data: { id: id },
                beforeSend: function () {
                    pr.form.ajaxBefore.call($form, {
                        controlId: edit.controlId,
                        message: 'Deleting ...'
                    });
                },
                success: function (data) {
                    var isError = !pr.form.ajaxAfter.apply($form, arguments);
                    if (isError)
                        return;

                    $(document).trigger(events.cancelationStatusChanged, [{ modalId: edit.modalId }]);
                },
                error: function () {
                    pr.form.ajaxError.apply($form, arguments);
                }
            });
        }

        return {
            init: init,
            events: events
        };
    })();

    var stats = (function () {
        var doctorsStats = '#doctorsStats',
            events = {
                doctorClicked: 'doctorClicked'
            };

        function init() {
            $(document).on('click', '.doctor-stats', doctorStatsClick);
        }

        function reload(stats) {
            renderDoctorStats(stats.Doctors);
        }

        function renderDoctorStats(doctors) {
            var selectedDoctorId = getSelectedDoctorId();

            clearDoctorStats();
            if (doctors.length == 0)
                return;

            var $stats = $('<div></div>');
            $.each(doctors, function (i, doctor) {
                $stats.append(getDoctorStatsHtml(doctor));
            });

            $(doctorsStats).html($stats.html());

            if (selectedDoctorId)
                $(doctorsStats).find('[data-doctor-id="' + selectedDoctorId + '"]').addClass('active');
            else
                $(doctorsStats).find('.list-item:first').addClass('active');
        }

        function getDoctorStatsHtml(doctor) {
            return '<div class="list-item doctor-stats" data-doctor-id="' + doctor.Id + '">' + doctor.Name +
                         '<span class="badge" style="background: ' + doctor.Color + ';">' +
                            doctor.Appointments +
                        '</span>' +
                    '</div>';
        }

        function clearDoctorStats() {
            var $stats = $(doctorsStats);
            $stats.empty();
        }

        function getSelectedDoctorId() {
            return $(doctorsStats).find('.active').data('doctor-id');
        }

        function doctorStatsClick() {
            $(document).trigger(events.doctorClicked);
        }

        return {
            init: init,
            events: events,
            reload: reload,
            getSelectedDoctorId: getSelectedDoctorId
        };
    })();

    var medicine = (function () {
        var containerId = '.medicines-list-container',
            contentId = '.medicines-list-content:first',
            events = { };

        function init() {
            $(document).on('click', '.issue-medicine', issueClick);
            $(document).on('click', '.medicines-list-close', closeList);
            $(document).on('change', '.issue-medicine-select', changed);
        }

        function issueClick(event) {
            event.preventDefault();
            event.stopPropagation();
            loadList();
        }

        function loadList() {
            var $container = $(containerId),
                $content = $container.find(contentId),
                $form = $(edit.modalId).find('form');

            $.ajax({
                url: '/Inventory/GetMedicines',
                beforeSend: function () {
                    $content.html('<p class="p-loading">Loading Medicines ...</p>');
                    $container.show();
                },
                success: function (data) {
                    var isError = !pr.form.ajaxAfter.apply($form, arguments);
                    if (isError)
                        return;

                    if (data.Html)
                        $content.html(data.Html);
                },
                error: function () {
                    pr.form.ajaxError.apply($form, arguments);
                }
            });
        }

        function closeList() {
            var $container = $(containerId),
                $content = $container.find(contentId);

            $content.empty();
            $container.hide();
        }

        function changed() {
            var $container = $(containerId),
                $a = $container.find('.issue-medicine-a'),
                val = $(this).val();

            if (val == '') {
                $a.hide();
                return;
            }

            var appointmentId = $(edit.modalId).find('#AppointmentId').val();
            $a.prop('href', 'medicine/' + val + '/' + appointmentId);
            $a.show();
        }

        return {
            init: init,
            events: events
        };
    })();

    $.extend(events, add.events, edit.events, cancel.events, stats.events, medicine.events);

    return {
        init: init,
        events: events,
        add: add,
        edit: edit,
        cancel: cancel,
        stats: stats,
        medicine: medicine
    };
})();