﻿var pr = pr || {};

pr.calendar = (function () {
    var selector = '#calendar';

    var events = {
        calendarLoaded: 'calendarLoaded',
        calendarEventRendered: 'calendarEventRendered'
    };

    function init() {
        var mode = 'basic'; //basic or agenda

        $(selector).fullCalendar({
            defaultView: mode + 'Week',
            firstDay: 1,
            fixedWeekCount: false,
            eventLimit: true,
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,' + mode + 'Week,' + mode + 'Day'
            },
            lazyFetching: false,
            eventSources: [{
                url: '/Appointments/GetCalendar',
                data: function() {
                    return {
                        doctorId: pr.appointments.stats.getSelectedDoctorId()
                    };
                },
                success: eventsFetched,
                error: function () {
                    console.log('Error');
                    console.log(arguments);
                }
            }],
            eventRender: eventRender,
            eventClick: eventClick
        });

        $(window).resize(resize);

        resize();
    }

    function resize() {
        var $sidebar = $('.sidebar'),
            height = $sidebar.height() - $('.navbar').height() - 100,
            width = $('.content').width();

        $('#calendar').fullCalendar('option', 'aspectRatio', width / height);
    }

    function reload() {
        $(selector).fullCalendar('refetchEvents');
    }

    function eventsFetched(data) {
        if (!data.Events)
            return;

        $(selector).fullCalendar('removeEvents');
        $(selector).fullCalendar('addEventSource', data.Events);

        if (data.Stats) {
            $(document).trigger(events.calendarLoaded, [{ stats: data.Stats }]);
        }
    }

    function eventRender(event, $el) {
        $el.prop('title', event.description);

        if (event.isCanceled) {
            $el.addClass('canceled');
            $el.append('<span class="glyphicon glyphicon-ban-circle"></span>');

            var title = $el.prop('title');
            if (title) {
                title = 'CANCELED - ' + title;
            }

            $el.prop('title', title);
        }

        $(document).trigger(events.calendarEventRendered, [{ $el: $el, event: event }]);
    }

    function eventClick(calEvent, jsEvent, view) {
        //$(document).trigger(events.appointmentClicked, [{ appointment: calEvent }]);
    }

    return {
        init: init,
        events: events,
        reload: reload
    };
})();