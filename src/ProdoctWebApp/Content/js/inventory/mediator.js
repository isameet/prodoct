﻿var pr = pr || {};
pr.inventory = pr.inventory || {};

pr.inventory.mediator = (function () {
    function init() {
        var e = $.extend({}, pr.inventory.events);

        $(document).on(e.documentLoaded, documentLoaded);
        $(document).on(e.itemAdded, item.add.added);
        $(document).on(e.itemListLoadError, item.list.loadError);

        $(document).on(e.editItemLoaded, item.edit.loaded);
        $(document).on(e.itemEdited, item.edit.edited);
        $(document).on(e.itemDeleted, item.deletePermanently.deleted);

        $(document).on(e.stockListLoadError, stock.list.loadError);
        $(document).on(e.addStockLoaded, stock.add.loaded);
        $(document).on(e.stockAdded, stock.add.added);
        $(document).on(e.consumeStockLoaded, stock.consume.loaded);
        $(document).on(e.stockConsumed, stock.consume.consumed);
        $(document).on(e.stockDeleted, stock.deletePermanently.deleted);

        $(document).on(e.historyListLoadError, history.list.loadError);
        $(document).on(e.historyReversed, history.reverse.reversed);

        $(document).on(e.issueMedicineLoaded, issueMedicine.loaded);
        $(document).on(e.medicineIssued, issueMedicine.issued);
    }

    function documentLoaded() {
        if ($('#issueMedicineContainer').length > 0) {
            $(document).trigger(pr.inventory.issueMedicine.events.issueMedicineLoaded);
        }
    }

    var item = (function () {
        var add = (function () {
            function added() {
                pr.inventory.item.list.reload();
                pr.modal.close({ empty: true });
            }

            return {
                added: added
            };
        })();

        var list = (function () {
            function loadError(e, data) {
                pr.flash.error(data.Message);
            }

            return {
                loadError: loadError
            };
        })();

        var edit = (function () {
            function loaded() {
                var container = pr.inventory.item.edit.containerId;
                //$('#itemBasicsTabLink').click();
                $(container).find('a[role="tab"].active').click();
            }

            function edited(e, data) {
                pr.inventory.item.list.reload();

                if (data.Item) {
                    var $title = $(pr.inventory.item.edit.containerId).find('.pr-modal-title:first');
                    $title.find('h3').text(data.Item.Name);
                    $title.find('.pr-modal-sub-title').text(data.Item.Code);
                }
            }

            return {
                loaded: loaded,
                edited: edited
            };
        })();

        var deletePermanently = (function () {
            function deleted(e, data) {
                pr.inventory.item.list.reload();
                pr.modal.close({ empty: true });
            }

            return {
                deleted: deleted
            };
        })();

        return {
            add: add,
            list: list,
            edit: edit,
            deletePermanently: deletePermanently
        };
    })();

    var stock = (function () {
        var list = (function () {
            function loadError(e, data) {
                pr.flash.error(data.Message);
            }

            return {
                loadError: loadError
            };
        })();

        var add = (function () {
            function loaded() {
                var containerId = pr.inventory.stock.add.containerId;
                pr.datepicker.reload(containerId);
            }

            function added() {
                pr.inventory.item.list.reload();
                pr.flash.preventClick();
                $('#itemStockTabLink').click();
            }

            return {
                added: added,
                loaded: loaded
            };
        })();

        var consume = (function () {
            function loaded() {
                var containerId = pr.inventory.stock.consume.containerId;
                pr.datepicker.reload(containerId);
            }

            function consumed() {
                pr.inventory.item.list.reload();
                pr.flash.preventClick();
                $('#itemStockTabLink').click();
            }

            return {
                consumed: consumed,
                loaded: loaded
            };
        })();

        var deletePermanently = (function () {
            function deleted(e, data) {
                pr.inventory.stock.list.reload();
                pr.inventory.item.list.reload();
            }

            return {
                deleted: deleted
            };
        })();

        return {
            list: list,
            add: add,
            consume: consume,
            deletePermanently: deletePermanently
        };
    })();

    var history = (function () {
        var list = (function () {
            function loadError(e, data) {
                pr.flash.error(data.Message);
            }

            return {
                loadError: loadError
            };
        })();

        var reverse = (function () {
            function reversed(e, data) {
                if (data.HistoryId) {
                    var $row = $('#' + data.HistoryId);
                    $row.fadeOut(function () {
                        $row.remove();
                    });
                }

                pr.inventory.item.list.reload();
            }

            return {
                reversed: reversed
            };
        })();

        return {
            list: list,
            reverse: reverse
        };
    })();

    var issueMedicine = (function () {
        function loaded() {
            pr.nav.mainMenu('#navAppointments');
            $('.focus-on-load').focus();
        }

        function issued() {
            var $container = $(pr.inventory.issueMedicine.containerId);
            $container.find('.issue-medicine').remove();
            $container.find('.medicine-issued').show();
        }

        return {
            loaded: loaded,
            issued: issued
        };
    })();

    return {
        init: init,
        item: item,
        stock: stock,
        history: history,
        issueMedicine: issueMedicine
    };
})();