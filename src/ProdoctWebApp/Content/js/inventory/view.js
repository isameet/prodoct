﻿//inventory/view

var pr = pr || {};

$(document).ready(function () {
    //inventory modules
    pr.inventory.init();
    
    //commmon modules
    pr.modal.init();
    pr.flash.init();
    pr.form.init();
    pr.confirm.init();
    pr.extensions.init();
    pr.nav.init('#navInventory');
    pr.ajax.init();
    pr.helpers.init();

    //structure
    pr.inventory.mediator.init();

    $(document).trigger(pr.inventory.events.documentLoaded);
});