﻿var pr = pr || {};

pr.inventory = (function () {
    var events = {
        documentLoaded: 'documentLoaded'
    };

    function init() {
        item.init();
        stock.init();
        history.init();
        issueMedicine.init();
    }

    var item = (function () {
        var events = {};

        function init() {
            list.init();
            deletePermanently.init();
        }

        var add = (function () {
            var controlId = '#btnAddItem',
                containerId = '#addItemContainer',
                events = {
                    itemAdded: 'itemAdded'
                };

            function before() {
                pr.form.ajaxBefore.call(this, {
                    controlId: controlId,
                    message: 'Adding Item ...'
                });
            }

            function after(data) {
                var isError = !pr.form.ajaxAfter.apply(this, arguments);
                if (isError)
                    return;

                $(document).trigger(events.itemAdded, [data]);
            }

            function error() {
                pr.form.ajaxError.apply(this, arguments);
            }

            return {
                before: before,
                after: after,
                error: error,
                events: events,
                containerId: containerId
            };
        })();

        var list = (function () {
            var nextSetClass = '.next-set',
                events = {
                    itemListLoadError: 'itemListLoadError'
                };

            function init() {
                $(document).on('click', '#itemsGetMore', getMoreClick);
                $('#itemsSearchForm').submit(searchFormSubmit);
                $('.stock-level > .list-group-item').click(stockLevelChanged);
            }

            function searchFormSubmit() {
                reload();
                return false;
            }

            function stockLevelChanged() {
                setTimeout(function () {
                    reload();
                }, 200);
            }

            function reload() {
                $('#items').html('<tr class="get-more-container next-set"><td colspan="7"></td></tr>');
                load();
            }

            function getMoreClick() {
                var data = getLoadParams();
                data.Skip = $(this).data('skip');
                load(data);
            }

            function getLoadParams() {
                var p = {
                    SearchTerm: $('#itemsSearch').val(),
                    StockLevel: $('.stock-level').find('.active:first').data('value')
                };

                return p;
            }

            function load(data) {
                var $target = $(nextSetClass),
                    data = data || getLoadParams();

                $.ajax({
                    url: '/Inventory/ListItems',
                    method: 'POST',
                    data: data,
                    beforeSend: function () {
                        $target.find('td').html('<div class="rows-loading">Loading ...</div>');
                    },
                    success: function (data) {
                        if (data.HasError) {
                            error(null, null, data.Message);
                            return;
                        }

                        if (data.Html) {
                            $target.replaceWith(data.Html);
                        }
                    },
                    error: error
                });
            }

            function error() {
                $(nextSetClass).find('td').empty();
                var message = arguments[2] || 'An Error Occured';
                $(document).trigger(events.itemListLoadError, [{ Message: message }]);
            }

            return {
                init: init,
                events: events,
                reload: reload
            };
        })();

        var edit = (function () {
            var containerId = '#editItemContainer',
                controlId = '#btnEditItem',
                events = {
                    editItemLoaded: 'editItemLoaded',
                    itemEdited: 'itemEdited'
                };

            function init() {
            }

            function before() {
                pr.form.ajaxBefore.call(this, {
                    controlId: controlId,
                    message: 'Updating ...'
                });
            }

            function after(data) {
                var isError = !pr.form.ajaxAfter.apply(this, arguments);
                if (isError)
                    return;

                $(document).trigger(events.itemEdited, [data]);
            }

            function error() {
                pr.form.ajaxError.apply(this, arguments);
            }

            return {
                init: init,
                events: events,
                containerId: containerId,
                controlId: controlId,
                before: before,
                after: after,
                error: error
            };
        })();

        var deletePermanently = (function () {
            var events = {
                itemDeleted: 'itemDeleted'
            };

            function init() {
                $(document).on('click', '#deleteItem', deleteClick);
            }

            function deleteClick() {
                event.preventDefault();
                event.stopPropagation();

                if (pr.confirm('Delete this Item permanently?') == false)
                    return;

                var id = $(edit.containerId).find('#Id').val();
                deletePermanently(id);
            }

            function deletePermanently(id) {
                var $form = $(edit.containerId).find('form');

                $.ajax({
                    url: '/Inventory/DeleteItem',
                    data: { id: id },
                    beforeSend: function () {
                        pr.form.ajaxBefore.call($form, {
                            controlId: edit.controlId,
                            message: 'Deleting ...'
                        });
                    },
                    success: function (data) {
                        var isError = !pr.form.ajaxAfter.apply($form, arguments);
                        if (isError)
                            return;

                        $(document).trigger(events.itemDeleted);
                    },
                    error: function () {
                        pr.form.ajaxError.apply($form, arguments);
                    }
                });
            }

            return {
                init: init,
                events: events
            }
        })();

        $.extend(events, add.events, list.events, edit.events, deletePermanently.events);

        return {
            init: init,
            events: events,
            add: add,
            list: list,
            edit: edit,
            deletePermanently: deletePermanently
        };
    })();

    var stock = (function () {
        var events = {};

        function init() {
            list.init();
            deletePermanently.init();
        }

        var list = (function () {
            var nextSetClass = '.next-set',
                events = {
                    stockListLoadError: 'stockListLoadError'
                };

            function init() {
                $(document).on('click', '#batchGetMore', getMoreClick);
            }

            function reload() {
                $('#stock').html('<tr class="get-more-container next-set"><td colspan="7"></td></tr>');
                load();
            }

            function getMoreClick() {
                var data = getLoadParams();
                data.Skip = $(this).data('skip');
                load(data);
            }

            function getLoadParams() {
                var p = {
                    ItemId: $('#editItemContainerContent').data('item-id')
                };

                return p;
            }

            function load(data) {
                var $target = $(nextSetClass),
                    data = data || getLoadParams();

                $.ajax({
                    url: '/Inventory/ListStock',
                    method: 'POST',
                    data: data,
                    beforeSend: function () {
                        $target.find('td').html('<div class="rows-loading">Loading ...</div>');
                    },
                    success: function (data) {
                        if (data.HasError) {
                            error(null, null, data.Message);
                            return;
                        }

                        if (data.Html) {
                            $target.replaceWith(data.Html);
                        }
                    },
                    error: error
                });
            }

            function error() {
                $(nextSetClass).find('td').empty();
                var message = arguments[2] || 'An Error Occured';
                $(document).trigger(events.stockListLoadError, [{ Message: message }]);
            }

            return {
                init: init,
                events: events,
                reload: reload
            };
        })();

        var add = (function () {
            var controlId = '#btnAddStock',
                containerId = '#addStockForm',
                events = {
                    stockAdded: 'stockAdded',
                    addStockLoaded: 'addStockLoaded'
                };

            function before() {
                pr.form.ajaxBefore.call(this, {
                    controlId: controlId,
                    message: 'Adding Stock ...'
                });
            }

            function after(data) {
                var isError = !pr.form.ajaxAfter.apply(this, arguments);
                if (isError)
                    return;

                $(document).trigger(events.stockAdded, [data]);
            }

            function error() {
                pr.form.ajaxError.apply(this, arguments);
            }

            return {
                before: before,
                after: after,
                error: error,
                events: events,
                containerId: containerId
            };
        })();

        var consume = (function () {
            var controlId = '#btnConsumeStock',
                containerId = '#consumeStockForm',
                events = {
                    stockConsumed: 'stockConsumed',
                    consumeStockLoaded: 'consumeStockLoaded'
                };

            function before() {
                pr.form.ajaxBefore.call(this, {
                    controlId: controlId,
                    message: 'Updating Stock ...'
                });
            }

            function after(data) {
                var isError = !pr.form.ajaxAfter.apply(this, arguments);
                if (isError)
                    return;

                $(document).trigger(events.stockConsumed, [data]);
            }

            function error() {
                pr.form.ajaxError.apply(this, arguments);
            }

            return {
                before: before,
                after: after,
                error: error,
                events: events,
                containerId: containerId
            };
        })();

        var deletePermanently = (function () {
            var events = {
                stockDeleted: 'stockDeleted'
            };

            function init() {
                $(document).on('click', '.delete-stock', deleteClick);
            }

            function deleteClick() {
                event.preventDefault();
                event.stopPropagation();

                if (pr.confirm('Delete this Batch permanently?') == false)
                    return;

                var itemId = $('#editItemContainerContent').data('item-id'),
                    batchId = $(this).closest('tr').data('batch-id');

                deletePermanently(itemId, batchId);
            }

            function deletePermanently(itemId, batchId) {
                var $form = $(consume.containerId).find('form');

                $.ajax({
                    url: '/Inventory/DeleteBatch',
                    data: { itemId: itemId, batchId: batchId },
                    beforeSend: function () {
                        pr.form.ajaxBefore.call($form, {
                            controlId: consume.controlId,
                            message: 'Deleting ...'
                        });
                    },
                    success: function (data) {
                        var isError = !pr.form.ajaxAfter.apply($form, arguments);
                        if (isError)
                            return;

                        $(document).trigger(events.stockDeleted);
                    },
                    error: function () {
                        pr.form.ajaxError.apply($form, arguments);
                    }
                });
            }

            return {
                init: init,
                events: events
            }
        })();

        $.extend(events, list.events, add.events, consume.events, deletePermanently.events);

        return {
            init: init,
            events: events,
            list: list,
            add: add,
            consume: consume,
            deletePermanently: deletePermanently
        };
    })();

    var history = (function () {
        var events = {};

        function init() {
            list.init();
            reverse.init();
        }

        var list = (function () {
            var nextSetClass = '.next-set',
                events = {
                    historyListLoadError: 'historyListLoadError'
                };

            function init() {
                $(document).on('click', '#historyGetMore', getMoreClick);
            }

            function reload() {
                $('#history').html('<tr class="get-more-container next-set"><td colspan="4"></td></tr>');
                load();
            }

            function getMoreClick() {
                var data = getLoadParams();
                data.Skip = $(this).data('skip');
                load(data);
            }

            function getLoadParams() {
                var p = {
                    ItemId: $('#editItemContainerContent').data('item-id')
                };

                return p;
            }

            function load(data) {
                var $target = $(nextSetClass),
                    data = data || getLoadParams();

                $.ajax({
                    url: '/Inventory/ListHistory',
                    method: 'POST',
                    data: data,
                    beforeSend: function () {
                        $target.find('td').html('<div class="rows-loading">Loading ...</div>');
                    },
                    success: function (data) {
                        if (data.HasError) {
                            error(null, null, data.Message);
                            return;
                        }

                        if (data.Html) {
                            $target.replaceWith(data.Html);
                        }
                    },
                    error: error
                });
            }

            function error() {
                $(nextSetClass).find('td').empty();
                var message = arguments[2] || 'An Error Occured';
                $(document).trigger(events.historyListLoadError, [{ Message: message }]);
            }

            return {
                init: init,
                events: events,
                reload: reload
            };
        })();

        var reverse = (function () {
            var events = {
                historyReversed: 'historyReversed'
            };

            function init() {
                $(document).on('click', '.reverse-history', reverseClick);
            }

            function reverseClick() {
                event.preventDefault();
                event.stopPropagation();

                if (pr.confirm('Reverse this transaction?') == false)
                    return;

                var historyId = $(this).closest('tr').data('history-id');
                reverseHistory(historyId);
            }

            function reverseHistory(historyId) {
                var $form = $('form:first');

                $.ajax({
                    url: '/Inventory/Reverse',
                    data: { id: historyId },
                    beforeSend: function () {
                        pr.form.ajaxBefore.call($form, {
                            message: 'Reversing ...'
                        });
                    },
                    success: function (data) {
                        var isError = !pr.form.ajaxAfter.apply($form, arguments);
                        if (isError)
                            return;

                        $(document).trigger(events.historyReversed, [data]);
                    },
                    error: function () {
                        pr.form.ajaxError.apply($form, arguments);
                    }
                });
            }

            return {
                init: init,
                events: events
            }
        })();

        $.extend(events, list.events, reverse.events);

        return {
            init: init,
            events: events,
            list: list,
            reverse: reverse
        };
    })();

    var issueMedicine = (function () {
        var controlId = '#btnIssueMedicine',
            containerId = '#issueMedicineContainer',
            events = {
                issueMedicineLoaded: 'issueMedicineLoaded',
                medicineIssued: 'medicineIssued'
            };

        function init() {
        }

        function before() {
            pr.form.ajaxBefore.call(this, {
                controlId: controlId,
                message: 'Issuing Medicine ...'
            });
        }

        function after(data) {
            var isError = !pr.form.ajaxAfter.apply(this, arguments);
            if (isError)
                return;

            $(document).trigger(events.medicineIssued, [data]);
        }

        function error() {
            pr.form.ajaxError.apply(this, arguments);
        }

        //$.extend(events, ...);

        return {
            init: init,
            events: events,
            containerId: containerId,
            before: before,
            after: after,
            error: error
        };
    })();

    $.extend(events, item.events, stock.events, history.events, issueMedicine.events);

    return {
        init: init,
        events: events,
        item: item,
        stock: stock,
        history: history,
        issueMedicine: issueMedicine
    };
})();