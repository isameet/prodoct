﻿var pr = pr || {};

pr.forgotpassword = (function () {
    var controlId = '#btnResetPassword',
        events = {

    };

    function init() {
    }

    function before() {
        pr.form.ajaxBefore.call(this, {
            controlId: controlId,
            message: 'Submitting request ...',
            noFlash: true
        });
    }

    function after(data) {
        var isError = !pr.form.ajaxAfter.apply(this, arguments);
        if (isError)
            return;

        $('.forgotPassword').hide();
        $('.requestSubmitted').show();
    }

    function error() {
        pr.form.ajaxError.apply(this, arguments);
    }

    return {
        init: init,
        events: events,
        before: before,
        after: after,
        error: error
    };
})();