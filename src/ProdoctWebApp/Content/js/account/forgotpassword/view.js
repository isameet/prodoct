﻿//forgotpassword/view

var pr = pr || {};

$(document).ready(function () {
    //forgotpassword modules
    pr.forgotpassword.init();
    
    //commmon modules
    pr.flash.init();
    pr.form.init();
    pr.ajax.init();

    //structure
    pr.forgotpassword.mediator.init();
});