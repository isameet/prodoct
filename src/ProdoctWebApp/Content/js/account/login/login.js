﻿var pr = pr || {};

pr.login = (function () {
    var events = {

    };

    function init () {
        $('form').submit(formSubmit);
    }

    function formSubmit() {
        if ($(this).valid() == false)
            return;

        var $btn = $('#btnLogin');
        $btn.removeClass('btn-success').addClass('btn-default');
        $btn.text('Signing in ...');
        $btn.prop('disabled', true);
    }

    return {
        init: init,
        events: events
    };
})();