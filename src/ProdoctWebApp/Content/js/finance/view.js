﻿//finance/view

var pr = pr || {};

$(document).ready(function () {
    //finance modules
    pr.finance.init();
    pr.charts.init();

    //commmon modules
    pr.collapsible.init();
    pr.modal.init();
    pr.flash.init();
    pr.form.init();
    pr.confirm.init();
    pr.extensions.init();
    pr.nav.init('#navFinance');
    pr.ajax.init();
    pr.helpers.init();
    pr.datepicker.init();
    pr.daterangepicker.init();

    //structure
    pr.finance.mediator.init();

    $(document).trigger(pr.finance.events.documentLoaded);
});