﻿var pr = pr || {};
pr.finance = pr.finance || {};

pr.finance.mediator = (function () {
    function init() {
        var e = $.extend({}, pr.finance.events,
                             pr.daterangepicker.events,
                             pr.modal.events);

        $(document).on(e.documentLoaded, documentLoaded);
        $(document).on(e.reportsLoaded, reportsLoaded);
        $(document).on(e.dateRangeChanged, dateRangeChanged);
        $(document).on(e.modalClosed, modalClosed);

        $(document).on(e.profitabilityReportsLoaded, profitability.loaded);
        
        $(document).on(e.incomeReportsLoaded, income.loaded);
        $(document).on(e.reportLoadedAllIncome, income.loadedAllIncome);
        $(document).on(e.reportLoadedPerDoctor, income.loadedPerDoctor);
        $(document).on(e.reportLoadedByCategory, income.loadedByCategory);
        $(document).on(e.addIncomeLoaded, income.add.loaded);
        $(document).on(e.incomeAdded, income.add.added);
        $(document).on(e.incomeListLoadError, income.list.loadError);
        $(document).on(e.incomeEditFormLoaded, income.edit.loaded);
        $(document).on(e.incomeEdited, income.edit.edited);
        $(document).on(e.feesEditFormLoaded, income.editFees.loaded);
        $(document).on(e.feesEdited, income.editFees.edited);
        $(document).on(e.incomeDeleted, income.deleted);

        $(document).on(e.expensesReportsLoaded, expenses.loaded);
        $(document).on(e.reportLoadedAllExpenses, expenses.loadedAllExpenses);
        $(document).on(e.reportLoadedExpensesByCategory, expenses.loadedByCategory);
        $(document).on(e.addExpenseLoaded, expenses.add.loaded);
        $(document).on(e.expenseAdded, expenses.add.added);
        $(document).on(e.expenseListLoadError, expenses.list.loadError);
        $(document).on(e.expenseEditFormLoaded, expenses.edit.loaded);
        $(document).on(e.expenseEdited, expenses.edit.edited);
        $(document).on(e.expenseDeleted, expenses.deleted);

        $(document).on(e.addFeesLoaded, addFees.loaded);
        $(document).on(e.feesAdded, addFees.added);

        $('#reportsList .list-item').click(reportChanging);
    }

    function documentLoaded() {
        if ($('#addFeesContainer').length > 0) {
            $(document).trigger(pr.finance.addFees.events.addFeesLoaded);
            return;
        }

        reloadReports();
        route();
    }

    function route() {
        var getUrlParam = pr.helpers.getUrlParam,
            route = getUrlParam('r');

        if (route == 'AddFees') {
            var id = getUrlParam('id'),
                modalId = pr.finance.income.add.containerId,
                $a = $('<a></a>');

            $a.addClass('modal-link hide');
            $a.prop('id', id);
            $a.attr('data-modal', modalId.replace('#', ''));
            $a.attr('data-target', modalId);
            $a.attr('data-get', '/Income/AddFees/' + id);
            $a.attr('data-loading-message', 'Loading Add Fees ...');
            $a.attr('data-broadcast-on-load', 'addIncomeLoaded');
            $('body').append($a); 
            $('#' + id).click();
        }
    }

    function dateRangeChanged() {
        reloadActiveReport();
    }

    function modalClosed() {
        History.replaceState(null, null, '/finance');
    }

    function reportsLoaded(e, data) {
        pr.daterangepicker.reload('.pr-modal:visible');
    }

    function reloadReports() {
        $('#reportsList').find('.active:first').click();
    }

    function reloadActiveReport() {
        $('#reportSelector').trigger('change');
    }

    function reportChanging() {
        pr.modal.close({ empty: true });
    }

    var profitability = (function () {

        function loaded() {
            pr.charts.profitability.render();
        }


        return {
            loaded: loaded
        };
    })();

    var income = (function () {
        function loaded() {
            $('#reportSelector').change();
        }

        function loadedAllIncome(e, data) {
            pr.charts.income.renderAllIncome(data);
        }

        function loadedPerDoctor(e, data) {
            pr.charts.income.renderPerDoctor(data);
        }

        function loadedByCategory(e, data) {
            pr.charts.income.renderByCategory(data);
        }

        function deleted() {
            reloadActiveReport();
            var containerId = pr.finance.income.edit.containerId;
            pr.modal.close({ modal: containerId, empty: true });
        }

        var add = (function () {
            function loaded() {
                var containerId = pr.finance.income.add.containerId;
                pr.datepicker.reload(containerId);
                pr.finance.income.autocomplete.init();

                $('#add_IncomeCategory').change();
            }

            function added() {
                reloadActiveReport();
                var containerId = pr.finance.income.add.containerId;
                pr.modal.close({ modal: containerId, empty: true });
            }

            return {
                loaded: loaded,
                added: added
            };
        })();

        var edit = (function () {
            function loaded() {
                var containerId = pr.finance.income.edit.containerId;
                pr.datepicker.reload(containerId);
            }

            function edited() {
                reloadActiveReport();
            }

            return {
                loaded: loaded,
                edited: edited
            };
        })();

        var editFees = (function () {
            function loaded() {
                var containerId = pr.finance.income.edit.containerId;
                pr.datepicker.reload(containerId);
            }

            function edited() {
                reloadActiveReport();
            }

            return {
                loaded: loaded,
                edited: edited
            };
        })();

        var list = (function () {
            function loadError(e, data) {
                pr.flash.error(data.Message);
            }

            return {
                loadError: loadError
            };
        })();

        return {
            loaded: loaded,
            loadedAllIncome: loadedAllIncome,
            loadedPerDoctor: loadedPerDoctor,
            loadedByCategory: loadedByCategory,
            add: add,
            edit: edit,
            editFees: editFees,
            deleted: deleted,
            list: list
        };
    })();

    var expenses = (function () {
        function loaded() {
            $('#reportSelector').change();
        }

        function loadedAllExpenses(e, data) {
            pr.charts.expenses.renderAllExpenses(data);
        }

        function loadedByCategory(e, data) {
            pr.charts.expenses.renderByCategory(data);
        }

        function deleted() {
            reloadActiveReport();
            var containerId = pr.finance.expenses.edit.containerId;
            pr.modal.close({ modal: containerId, empty: true });
        }

        var add = (function () {
            function loaded() {
                var containerId = pr.finance.expenses.add.containerId;
                pr.datepicker.reload(containerId);
            }

            function added() {
                reloadActiveReport();
                var containerId = pr.finance.expenses.add.containerId;
                pr.modal.close({ modal: containerId, empty: true });
            }

            return {
                loaded: loaded,
                added: added
            };
        })();

        var edit = (function () {
            function loaded() {
                var containerId = pr.finance.expenses.edit.containerId;
                pr.datepicker.reload(containerId);
            }

            function edited() {
                reloadActiveReport();
            }

            return {
                loaded: loaded,
                edited: edited
            };
        })();

        var list = (function () {
            function loadError(e, data) {
                pr.flash.error(data.Message);
            }

            return {
                loadError: loadError
            };
        })();

        return {
            loaded: loaded,
            loadedAllExpenses: loadedAllExpenses,
            loadedByCategory: loadedByCategory,
            add: add,
            edit: edit,
            deleted: deleted,
            list: list
        };
    })();

    var addFees = (function () {
        function loaded() {
            pr.nav.mainMenu('#navAppointments');
            var containerId = pr.finance.addFees.containerId;
            pr.datepicker.reload(containerId);
            $('#add_FeesCategory').change();
            $('.focus-on-load').focus();
        }

        function added() {
            var $container = $(pr.finance.addFees.containerId);
            $container.find('.add-fees').remove();
            $container.find('.fees-added').show();
        }

        return {
            loaded: loaded,
            added: added
        };
    })();

    return {
        init: init
    };
})();