﻿var pr = pr || {};

pr.charts = (function () {
    var events = {};

    function init() {
        setDefaults();
    }

    function setDefaults() {
        Chart.defaults.global.responsive = true;
    }

    var profitability = (function () {
        function render() {
            var data = {
                labels: ["January", "February", "March", "April", "May", "June", "July"],
                datasets: [
                    {
                        label: "My First dataset",
                        fillColor: "rgba(220,220,220,0.2)",
                        strokeColor: "rgba(220,220,220,1)",
                        pointColor: "rgba(220,220,220,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: [65, 59, 80, 81, 56, 55, 40]
                    },
                    {
                        label: "My Second dataset",
                        fillColor: "rgba(151,187,205,0.2)",
                        strokeColor: "rgba(151,187,205,1)",
                        pointColor: "rgba(151,187,205,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(151,187,205,1)",
                        data: [28, 48, 40, 19, 86, 27, 90]
                    }
                ]
            };
            var ctx = $("#myChart").get(0).getContext("2d");
            var myLineChart = new Chart(ctx).Line(data);
        }

        return {
            render: render
        };
    })();

    var income = (function () {
        function renderPerDoctor(data) {
            data = JSON.parse(data);
            data = data.Slices;

            var ctx = $("#myChart").get(0).getContext("2d");
            var chart = new Chart(ctx).Doughnut(data);
        }

        function renderByCategory(data) {
            data = JSON.parse(data);
            data = data.Slices;

            var ctx = $("#myChart").get(0).getContext("2d");
            var chart = new Chart(ctx).Pie(data);
        }

        function renderAllIncome(data) {
            data = JSON.parse(data);
            data = {
                labels: data.Labels,
                datasets: [
                    {
                        label: "Incomes",
                        fillColor: "rgba(151,187,205,0.5)",
                        strokeColor: "rgba(151,187,205,0.8)",
                        highlightFill: "rgba(151,187,205,0.75)",
                        highlightStroke: "rgba(151,187,205,1)",
                        data: data.Data
                    }
                ]
            };

            var ctx = $("#myChart").get(0).getContext("2d");
            var chart = new Chart(ctx).Bar(data);
        }

        return {
            renderAllIncome: renderAllIncome,
            renderPerDoctor: renderPerDoctor,
            renderByCategory: renderByCategory
        };
    })();

    var expenses = (function () {
        function renderByCategory(data) {
            data = JSON.parse(data);
            data = data.Slices;

            var ctx = $("#myChart").get(0).getContext("2d");
            var chart = new Chart(ctx).Pie(data);
        }

        function renderAllExpenses(data) {
            data = JSON.parse(data);
            data = {
                labels: data.Labels,
                datasets: [
                    {
                        label: "Expenses",
                        fillColor: "rgba(236,60,60,0.5)",
                        strokeColor: "rgba(236,60,60,0.65)",
                        highlightFill: "rgba(236,60,60,0.75)",
                        highlightStroke: "rgba(236,60,60,.85)",
                        data: data.Data
                    }
                ]
            };

            var ctx = $("#myChart").get(0).getContext("2d");
            var chart = new Chart(ctx).Bar(data);
        }

        return {
            renderAllExpenses: renderAllExpenses,
            renderByCategory: renderByCategory
        };
    })();

    //$.extend(events);

    return {
        init: init,
        events: events,
        profitability: profitability,
        income: income,
        expenses: expenses
    };
})();