﻿var pr = pr || {};

pr.finance = (function () {
    var events = {
        documentLoaded: 'documentLoaded',
        reportsLoaded: 'reportsLoaded'
    };

    function init() {
        income.init();
        expenses.init();
        addFees.init();
    }

    function getReportParams() {
        var $container = $('.report-duration');

        return {
            Start: $container.find('#Start').val(),
            End: $container.find('#End').val()
        };
    }

    var profitability = (function () {
        var events = {
            profitabilityReportsLoaded: 'profitabilityReportsLoaded'
        };

        function init() {

        }

        return {
            init: init,
            events: events
        };
    })();

    var income = (function () {
        var events = {
            incomeReportsLoaded: 'incomeReportsLoaded',
            reportLoadedAllIncome: 'reportLoadedAllIncome',
            reportLoadedPerDoctor: 'reportLoadedPerDoctor',
            reportLoadedByCategory: 'reportLoadedByCategory'
        };

        function init() {
            add.init();
            deletePermanently.init();
            list.init();
        }

        var autocomplete = (function () {
            function init() {
                pr.autocomplete.patients.init(add.containerId);
                pr.autocomplete.patients.load('#add_IncomePatientNameOrMobile', { serviceUrl: '/Patients/NameOrMobile', minChars: 3 });
            }

            return {
                init: init
            };
        })();

        var add = (function () {
            var controlId = '#btnAddIncome',
                containerId = '#addIncomeContainer',
                events = {
                    addIncomeLoaded: 'addIncomeLoaded',
                    incomeAdded: 'incomeAdded'
                };

            function init() {
                $(document).on('change', '#add_IncomeCategory', categoryChange);
            }

            function categoryChange() {
                if (isCategoryPatientFees()) {
                    showPatientFees();
                }
                else {
                    $(containerId).find('.patient-fees').hide();
                }
            }

            function isCategoryPatientFees() {
                if ($('#add_IncomeCategory').val().toLowerCase() == 'fees')
                    return true;

                return false;
            }

            function showPatientFees() {
                $(containerId).find('.patient-fees').show();
                $('#add_IncomePatientNameOrMobile').focus();
            }

            function before() {
                pr.form.ajaxBefore.call(this, {
                    controlId: controlId,
                    message: 'Adding Income ...'
                });
            }

            function after(data) {
                var isError = !pr.form.ajaxAfter.apply(this, arguments);
                if (isError)
                    return;

                $(document).trigger(events.incomeAdded, [data]);
            }

            function error() {
                pr.form.ajaxError.apply(this, arguments);
            }

            return {
                init: init,
                containerId: containerId,
                events: events,
                before: before,
                after: after,
                error: error
            };
        })();

        var edit = (function () {
            var controlId = '#btnEditIncome',
                containerId = '#editIncomeContainer',
                events = {
                    incomeEdited: 'incomeEdited',
                    incomeEditFormLoaded: 'incomeEditFormLoaded'
                };

            function before() {
                pr.form.ajaxBefore.call(this, {
                    controlId: controlId,
                    message: 'Updating Record ...'
                });
            }

            function after(data) {
                var isError = !pr.form.ajaxAfter.apply(this, arguments);
                if (isError)
                    return;

                $(document).trigger(events.incomeEdited, [data]);
            }

            function error() {
                pr.form.ajaxError.apply(this, arguments);
            }

            return {
                before: before,
                after: after,
                error: error,
                events: events,
                containerId: containerId,
                controlId: controlId
            };
        })();

        var editFees = (function () {
            var controlId = '#btnEditFees',
                containerId = '#editIncomeContainer',
                events = {
                    feesEdited: 'feesEdited',
                    feesEditFormLoaded: 'feesEditFormLoaded'
                };

            function before() {
                pr.form.ajaxBefore.call(this, {
                    controlId: controlId,
                    message: 'Updating Record ...'
                });
            }

            function after(data) {
                var isError = !pr.form.ajaxAfter.apply(this, arguments);
                if (isError)
                    return;

                $(document).trigger(events.feesEdited, [data]);
            }

            function error() {
                pr.form.ajaxError.apply(this, arguments);
            }

            return {
                before: before,
                after: after,
                error: error,
                events: events,
                containerId: containerId,
                controlId: controlId
            };
        })();

        var deletePermanently = (function () {
            var events = {
                incomeDeleted: 'incomeDeleted'
            };

            function init() {
                $(document).on('click', '.delete-income', deleteClick);
            }

            function deleteClick() {
                event.preventDefault();
                event.stopPropagation();

                if (pr.confirm('Delete this Income Record permanently?') == false)
                    return;

                var id = $(edit.containerId).find('input[name="Id"]').val();
                deletePermanently(id);
            }

            function deletePermanently(id) {
                var $form = $(edit.containerId).find('form');

                $.ajax({
                    url: '/Income/Delete',
                    data: { id: id },
                    beforeSend: function () {
                        pr.form.ajaxBefore.call($form, {
                            controlId: edit.controlId,
                            message: 'Deleting ...'
                        });
                    },
                    success: function (data) {
                        var isError = !pr.form.ajaxAfter.apply($form, arguments);
                        if (isError)
                            return;

                        $(document).trigger(events.incomeDeleted);
                    },
                    error: function () {
                        pr.form.ajaxError.apply($form, arguments);
                    }
                });
            }

            return {
                init: init,
                events: events
            }
        })();

        var list = (function () {
            var nextSetClass = '.next-set',
                events = {
                    incomeListLoadError: 'incomeListLoadError'
                };

            function init() {
                $(document).on('click', '#incomesGetMore', getMoreClick);
            }

            function getMoreClick() {
                var data = getLoadParams();
                data.Skip = $(this).data('skip');
                load(data);
            }

            function getLoadParams() {
                var p = {
                    Start: $('#Start').val(),
                    End: $('#End').val()
                };

                return p;
            }

            function load(data) {
                var $target = $(nextSetClass),
                    data = data || getLoadParams();

                $.ajax({
                    url: '/Income/List',
                    method: 'POST',
                    data: data,
                    beforeSend: function () {
                        $target.find('td').html('<div class="rows-loading">Loading ...</div>');
                    },
                    success: function (data) {
                        if (data.HasError) {
                            error(null, null, data.Message);
                            return;
                        }

                        if (data.Html) {
                            $target.replaceWith(data.Html);
                        }
                    },
                    error: error
                });
            }

            function error() {
                $(nextSetClass).find('td').empty();
                var message = arguments[2] || 'An Error Occured';
                $(document).trigger(events.incomeListLoadError, [{ Message: message }]);
            }

            return {
                init: init,
                events: events
            };
        })();

        $.extend(events, add.events, edit.events, editFees.events, deletePermanently.events, list.events);

        return {
            init: init,
            events: events,
            add: add,
            edit: edit,
            editFees: editFees,
            autocomplete: autocomplete
        };
    })();

    var expenses = (function () {
        var events = {
            expensesReportsLoaded: 'expensesReportsLoaded',
            reportLoadedAllExpenses: 'reportLoadedAllExpenses',
            reportLoadedExpensesByCategory: 'reportLoadedExpensesByCategory'
        };

        function init() {
            add.init();
            deletePermanently.init();
            list.init();
        }

        var add = (function () {
            var controlId = '#btnAddExpense',
                containerId = '#addExpenseContainer',
                events = {
                    addExpenseLoaded: 'addExpenseLoaded',
                    expenseAdded: 'expenseAdded'
                };

            function init() {
            }

            function before() {
                pr.form.ajaxBefore.call(this, {
                    controlId: controlId,
                    message: 'Adding Expense ...'
                });
            }

            function after(data) {
                var isError = !pr.form.ajaxAfter.apply(this, arguments);
                if (isError)
                    return;

                $(document).trigger(events.expenseAdded, [data]);
            }

            function error() {
                pr.form.ajaxError.apply(this, arguments);
            }

            return {
                init: init,
                containerId: containerId,
                events: events,
                before: before,
                after: after,
                error: error
            };
        })();

        var edit = (function () {
            var controlId = '#btnEditExpense',
                containerId = '#editExpenseContainer',
                events = {
                    expenseEdited: 'expenseEdited',
                    expenseEditFormLoaded: 'expenseEditFormLoaded'
                };

            function before() {
                pr.form.ajaxBefore.call(this, {
                    controlId: controlId,
                    message: 'Updating Record ...'
                });
            }

            function after(data) {
                var isError = !pr.form.ajaxAfter.apply(this, arguments);
                if (isError)
                    return;

                $(document).trigger(events.expenseEdited, [data]);
            }

            function error() {
                pr.form.ajaxError.apply(this, arguments);
            }

            return {
                before: before,
                after: after,
                error: error,
                events: events,
                containerId: containerId,
                controlId: controlId
            };
        })();

        var deletePermanently = (function () {
            var events = {
                expenseDeleted: 'expenseDeleted'
            };

            function init() {
                $(document).on('click', '.delete-expense', deleteClick);
            }

            function deleteClick() {
                event.preventDefault();
                event.stopPropagation();

                if (pr.confirm('Delete this Expense Record permanently?') == false)
                    return;

                var id = $(edit.containerId).find('input[name="Id"]').val();
                deletePermanently(id);
            }

            function deletePermanently(id) {
                var $form = $(edit.containerId).find('form');

                $.ajax({
                    url: '/Expenses/Delete',
                    data: { id: id },
                    beforeSend: function () {
                        pr.form.ajaxBefore.call($form, {
                            controlId: edit.controlId,
                            message: 'Deleting ...'
                        });
                    },
                    success: function (data) {
                        var isError = !pr.form.ajaxAfter.apply($form, arguments);
                        if (isError)
                            return;

                        $(document).trigger(events.expenseDeleted);
                    },
                    error: function () {
                        pr.form.ajaxError.apply($form, arguments);
                    }
                });
            }

            return {
                init: init,
                events: events
            }
        })();

        var list = (function () {
            var nextSetClass = '.next-set',
                events = {
                    expenseListLoadError: 'expenseListLoadError'
                };

            function init() {
                $(document).on('click', '#expensesGetMore', getMoreClick);
            }

            function getMoreClick() {
                var data = getLoadParams();
                data.Skip = $(this).data('skip');
                load(data);
            }

            function getLoadParams() {
                var p = {
                    Start: $('#Start').val(),
                    End: $('#End').val()
                };

                return p;
            }

            function load(data) {
                var $target = $(nextSetClass),
                    data = data || getLoadParams();

                $.ajax({
                    url: '/Expenses/List',
                    method: 'POST',
                    data: data,
                    beforeSend: function () {
                        $target.find('td').html('<div class="rows-loading">Loading ...</div>');
                    },
                    success: function (data) {
                        if (data.HasError) {
                            error(null, null, data.Message);
                            return;
                        }

                        if (data.Html) {
                            $target.replaceWith(data.Html);
                        }
                    },
                    error: error
                });
            }

            function error() {
                $(nextSetClass).find('td').empty();
                var message = arguments[2] || 'An Error Occured';
                $(document).trigger(events.expenseListLoadError, [{ Message: message }]);
            }

            return {
                init: init,
                events: events
            };
        })();

        $.extend(events, add.events, edit.events, deletePermanently.events, list.events);

        return {
            init: init,
            events: events,
            add: add,
            edit: edit
        };
    })();

    var addFees = (function () {
        var controlId = '#btnAddFees',
            containerId = '#addFeesContainer',
            events = {
                addFeesLoaded: 'addFeesLoaded',
                feesAdded: 'feesAdded'
            };

        function init() {
            $(document).on('change', '#add_FeesCategory', categoryChange);
        }

        function categoryChange() {
            if (isCategoryPatientFees()) {
                showPatientFees();
            }
            else {
                $(containerId).find('.patient-fees').hide();
            }
        }

        function isCategoryPatientFees() {
            if ($('#add_FeesCategory').val().toLowerCase() == 'fees')
                return true;

            return false;
        }

        function showPatientFees() {
            $(containerId).find('.patient-fees').show();
            $('#add_IncomePatientNameOrMobile').focus();
        }

        function before() {
            pr.form.ajaxBefore.call(this, {
                controlId: controlId,
                message: 'Adding Fees ...'
            });
        }

        function after(data) {
            var isError = !pr.form.ajaxAfter.apply(this, arguments);
            if (isError)
                return;

            $(document).trigger(events.feesAdded, [data]);
        }

        function error() {
            pr.form.ajaxError.apply(this, arguments);
        }

        //$.extend(events, ...);

        return {
            init: init,
            containerId: containerId,
            events: events,
            before: before,
            after: after,
            error: error
        };
    })();

    $.extend(events, profitability.events, income.events, expenses.events, addFees.events);

    return {
        init: init,
        events: events,
        profitability: profitability,
        income: income,
        expenses: expenses,
        getReportParams: getReportParams,
        addFees: addFees
    };
})();