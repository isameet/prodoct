﻿var pr = pr || {};
pr.settings = pr.settings || {};

pr.settings.mediator = (function () {
    function init() {
        var e = $.extend({}, pr.settings.events,
                             pr.tabs.events,
                             pr.ajax.events);

        $(document).on(e.documentLoaded, documentLoaded);
        $(document).on(e.tabChanging, tabChanging);
        
        $(document).on(e.staffAdded, staff.added);
        $(document).on(e.editingAfterStaffAdded, staff.editingAfterStaffAdded);
        $(document).on(e.staffEditLoaded, staff.editLoaded);
        $(document).on(e.staffBasicInfoLoaded, staff.basicInfoLoaded);
        $(document).on(e.staffEdited, staff.edited);
        $(document).on(e.staffPrivilegesEdited, staff.privilegesEdited);
        $(document).on(e.staffDeleted, staff.deleted);

        $(document).on(e.doctorAdded, doctor.added);
        $(document).on(e.doctorAddLoaded, doctor.addLoaded);
        $(document).on(e.editingAfterDoctorAdded, doctor.editingAfterDoctorAdded);
        $(document).on(e.doctorEditLoaded, doctor.editLoaded);
        $(document).on(e.doctorBasicInfoLoaded, doctor.basicInfoLoaded);
        $(document).on(e.doctorEdited, doctor.edited);
        $(document).on(e.doctorPrivilegesEdited, doctor.privilegesEdited);
        $(document).on(e.doctorDeleted, doctor.deleted);
    }

    function documentLoaded () {
        pr.ajax.load({ get: '/Settings/MyProfile', target: '#profile' });
    }

    function tabChanging(event, data) {
        pr.modal.close({ speed: 0, empty: true });
    }

    var staff = (function () {
        function added(event, data) {
            reloadList();
            pr.modal.close();
            if (data.Id) {
                var editModalId = '#editStaffContainer'
                pr.ajax.load({
                    get: '/Settings/StaffEdit/' + data.Id,
                    target: editModalId,
                    loadingText: 'Loading Staff ...',
                    broadcastOnLoad: 'EditingAfterStaffAdded'
                });
                pr.modal.show(editModalId);
            }
        }

        function editingAfterStaffAdded() {
            $('#staffPrivilegesTabLink').click();
        }

        function editLoaded () {
            $('#staffBasicsTabLink').click();
        }

        function basicInfoLoaded() {
        }

        function edited (event, data) {
            reloadList();
        }

        function privilegesEdited(event, data) {
            reloadList();
        }

        function deleted(event, data) {
            reloadList();
            pr.modal.close();
        }

        function reloadList() {
            pr.ajax.load({ get: '/Settings/Staff', target: '#staff' });
        }

        return {
            added: added,
            editingAfterStaffAdded: editingAfterStaffAdded,
            editLoaded: editLoaded,
            basicInfoLoaded: basicInfoLoaded,
            edited: edited,
            privilegesEdited: privilegesEdited,
            deleted: deleted
        };
    })();

    var doctor = (function () {
        function added(event, data) {
            reloadList();
            pr.modal.close();
            if (data.Id) {
                var editModalId = '#editStaffContainer'
                pr.ajax.load({
                    get: '/Settings/DoctorEdit/' + data.Id,
                    target: editModalId,
                    loadingText: 'Loading Doctor ...',
                    broadcastOnLoad: 'EditingAfterDoctorAdded'
                });
                pr.modal.show(editModalId);
            }
        }

        function addLoaded() {
            $('.calendar-color').colorpicker();
        }

        function editingAfterDoctorAdded() {
            $('#doctorPrivilegesTabLink').click();
        }

        function editLoaded() {
            $('#doctorBasicsTabLink').click();
        }

        function basicInfoLoaded() {
            $('.calendar-color').colorpicker();
        }

        function edited(event, data) {
            reloadList();
        }

        function privilegesEdited(event, data) {
            reloadList();
        }

        function reloadList() {
            pr.ajax.load({ get: '/Settings/Staff', target: '#staff' });
        }

        function deleted(event, data) {
            reloadList();
            pr.modal.close();
        }

        return {
            added: added,
            addLoaded: addLoaded,
            editingAfterDoctorAdded: editingAfterDoctorAdded,
            editLoaded: editLoaded,
            basicInfoLoaded: basicInfoLoaded,
            edited: edited,
            privilegesEdited: privilegesEdited,
            deleted: deleted
        };
    })();

    return {
        init: init
    }
})();