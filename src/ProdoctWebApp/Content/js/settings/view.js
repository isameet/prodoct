﻿//settings/view

var pr = pr || {};

$(document).ready(function () {
    //settings modules
    pr.settings.init();
    
    //commmon modules
    pr.flash.init();
    pr.form.init();
    pr.confirm.init();
    pr.extensions.init();
    pr.nav.init('#navSettings');
    pr.tabs.init();
    pr.ajax.init();
    pr.modal.init();
    pr.helpers.init();

    //structure
    pr.settings.mediator.init();
});