﻿var pr = pr || {};

pr.settings = (function () {
    var events = {
        documentLoaded: 'documentLoaded'
    };

    function init() {
        myInfo.init();
        staff.init();
        doctor.init();

        $(function () {
            $(document).trigger(events.documentLoaded);
        });
       
    }

    var myInfo = (function () {
        var controlId = '#btnUpdateMyInfo',
            events = {};

        function init () {

        }

        function before() {
            pr.form.ajaxBefore.call(this, {
                controlId: controlId,
                message: 'Updating ...'
            });
        }

        function after(data) {
            var isError = !pr.form.ajaxAfter.apply(this, arguments);
            if (isError)
                return;
        }

        function error() {
            pr.form.ajaxError.apply(this, arguments);
        }

        return {
            init: init,
            before: before,
            after: after,
            error: error,
            events: events
        };
    })();

    var password = (function () {
        var containerId = '#password',
            controlId = '#btnChangePassword',
            events = {};

        function init() {
        }

        function before() {
            pr.form.ajaxBefore.call(this, {
                controlId: controlId,
                message: 'Changing ...'
            });
        }

        function after(data) {
            var isError = !pr.form.ajaxAfter.apply(this, arguments);
            if (isError)
                return;

            $(containerId).find('form')[0].reset();
        }

        function error() {
            pr.form.ajaxError.apply(this, arguments);
        }

        return {
            init: init,
            before: before,
            after: after,
            error: error,
            events: events
        };
    })();

    var practice = (function () {
        var controlId = '#btnUpdatePracticeProfile',
            events = {};

        function init() {
        }

        function before() {
            pr.form.ajaxBefore.call(this, {
                controlId: controlId,
                message: 'Updating ...'
            });
        }

        function after(data) {
            var isError = !pr.form.ajaxAfter.apply(this, arguments);
            if (isError)
                return;
        }

        function error() {
            pr.form.ajaxError.apply(this, arguments);
        }

        return {
            init: init,
            before: before,
            after: after,
            error: error,
            events: events
        };
    })();

    var calendar = (function () {
        var controlId = '#btnUpdateCalendarSettings',
            events = {};

        function init() {
        }

        function before() {
            pr.form.ajaxBefore.call(this, {
                controlId: controlId,
                message: 'Updating ...'
            });
        }

        function after(data) {
            var isError = !pr.form.ajaxAfter.apply(this, arguments);
            if (isError)
                return;
        }

        function error() {
            pr.form.ajaxError.apply(this, arguments);
        }

        return {
            init: init,
            before: before,
            after: after,
            error: error,
            events: events
        };
    })();

    var staff = (function () {
        var controlId = '#btnUpdateStaffSettings'.
            events = { };

        function init() {
            editPrivileges.init();
            deletePermanently.init();
        }

        function before() {
            pr.form.ajaxBefore.call(this, {
                controlId: controlId,
                message: 'Updating ...'
            });
        }

        function after(data) {
            console.log(data);

            var isError = !pr.form.ajaxAfter.apply(this, arguments);
            if (isError)
                return;
        }

        function error() {
            pr.form.ajaxError.apply(this, arguments);
        }

        var add = (function () {
            var controlId = '#btnAddStaff',
                events = {
                    staffAdded: 'staffAdded',
                    editingAfterStaffAdded: 'EditingAfterStaffAdded'
                };

            function before() {
                pr.form.ajaxBefore.call(this, {
                    controlId: controlId,
                    message: 'Adding ...'
                });
            }

            function after(data) {
                var isError = !pr.form.ajaxAfter.apply(this, arguments);
                if (isError)
                    return;

                $(document).trigger(events.staffAdded, [data]);
            }

            function error() {
                pr.form.ajaxError.apply(this, arguments);
            }

            return {
                before: before,
                after: after,
                error: error,
                events: events
            };
        })();

        var editBasics = (function () {
            var containerId = '#editStaffContainer',
                controlId = '#btnStaffBasics',
                events = {
                    staffEditLoaded: 'StaffEditLoaded',
                    staffEdited: 'staffEdited',
                    staffBasicInfoLoaded: 'staffBasicInfoLoaded'
                };

            function before() {
                pr.form.ajaxBefore.call(this, {
                    controlId: controlId,
                    message: 'Updating ...'
                });
            }

            function after(data) {
                var isError = !pr.form.ajaxAfter.apply(this, arguments);
                if (isError)
                    return;

                $(document).trigger(events.staffEdited);
            }

            function error() {
                pr.form.ajaxError.apply(this, arguments);
            }

            return {
                before: before,
                after: after,
                error: error,
                events: events,
                controlId: controlId,
                containerId: containerId
            };
        })();

        var editPrivileges = (function () {
            var containerId = '#staffprivileges',
                controlId = '#btnStaffPrivileges',
                events = {
                    staffPrivilegesEdited: 'staffPrivilegesEdited'
                };

            function init() {
                $(document).on('change', '#staff_IsAdmin', isAdminCheckboxChange);
            }

            function before() {
                pr.form.ajaxBefore.call(this, {
                    controlId: controlId,
                    message: 'Updating ...'
                });
            }

            function after(data) {
                var isError = !pr.form.ajaxAfter.apply(this, arguments);
                if (isError)
                    return;

                $(document).trigger(events.staffPrivilegesEdited);
            }

            function error() {
                pr.form.ajaxError.apply(this, arguments);
            }

            function isAdminCheckboxChange() {
                var isAdmin = $(this).is(':checked'),
                    $privileges = $(containerId).find('.privileges:first');

                if (isAdmin)
                    $privileges.addClass('admin');
                else
                    $privileges.removeClass('admin');
            }

            return {
                init: init,
                before: before,
                after: after,
                error: error,
                events: events
            };
        })();

        var deletePermanently = (function () {
            var events = {
                staffDeleted: 'staffDeleted'
            };

            function init() {
                $(document).on('click', '#deleteStaff', deleteClick);
            }

            function deleteClick () {
                event.preventDefault();
                event.stopPropagation();

                if (pr.confirm('Delete this account permanently?') == false)
                    return;

                var id = $(editBasics.containerId).find('#Id').val();
                deletePermanently(id);
            }

            function deletePermanently(id) {
                var $form = $(editBasics.containerId).find('form');

                $.ajax({
                    url: '/Settings/StaffDelete',
                    data: { id: id },
                    beforeSend: function () {
                        pr.form.ajaxBefore.call($form, {
                            controlId: editBasics.controlId,
                            message: 'Deleting ...'
                        });
                    },
                    success: function (data) {
                        var isError = !pr.form.ajaxAfter.apply($form, arguments);
                        if (isError)
                            return;

                        $(document).trigger(events.staffDeleted);
                    },
                    error: function () {
                        pr.form.ajaxError.apply($form, arguments);
                    }
                });
            }

            return {
                init: init,
                events: events
            }
        })();

        $.extend(events, add.events, editBasics.events,
                         editPrivileges.events, deletePermanently.events);

        return {
            init: init,
            before: before,
            after: after,
            error: error,
            add: add,
            editBasics: editBasics,
            editPrivileges: editPrivileges,
            deletePermanently: deletePermanently,
            events: events
        };
    })();

    var doctor = (function () {
        var events = {};

        function init() {
            editPrivileges.init();
            deletePermanently.init();
        }

        var add = (function () {
            var controlId = '#btnAddDoctor',
                events = {
                    doctorAdded: 'doctorAdded',
                    editingAfterDoctorAdded: 'EditingAfterDoctorAdded',
                    doctorAddLoaded: 'doctorAddLoaded'
                };

            function before() {
                pr.form.ajaxBefore.call(this, {
                    controlId: controlId,
                    message: 'Adding ...'
                });
            }

            function after(data) {
                var isError = !pr.form.ajaxAfter.apply(this, arguments);
                if (isError)
                    return;

                $(document).trigger(events.doctorAdded, [data]);
            }

            function error() {
                pr.form.ajaxError.apply(this, arguments);
            }

            return {
                before: before,
                after: after,
                error: error,
                events: events
            };
        })();

        var editBasics = (function () {
            var containerId = '#editStaffContainer',
                controlId = '#btnDoctorBasics',
                events = {
                    doctorEditLoaded: 'DoctorEditLoaded',
                    doctorEdited: 'doctorEdited',
                    doctorBasicInfoLoaded: 'doctorBasicInfoLoaded'
                };

            function init () {
            }

            function before() {
                pr.form.ajaxBefore.call(this, {
                    controlId: controlId,
                    message: 'Updating ...'
                });
            }

            function after(data) {
                var isError = !pr.form.ajaxAfter.apply(this, arguments);
                if (isError)
                    return;

                $(document).trigger(events.doctorEdited);
            }

            function error() {
                pr.form.ajaxError.apply(this, arguments);
            }

            return {
                before: before,
                after: after,
                error: error,
                events: events,
                containerId: containerId,
                controlId: controlId
            };
        })();

        var editPrivileges = (function () {
            var containerId = '#doctorprivileges',
                controlId = '#btnDoctorPrivileges',
                events = {
                    doctorPrivilegesEdited: 'doctorPrivilegesEdited'
                };

            function init() {
                $(document).on('change', '#doctor_IsAdmin', isAdminCheckboxChange);
            }

            function isAdminCheckboxChange() {
                var isAdmin = $(this).is(':checked'),
                    $privileges = $(containerId).find('.privileges:first');

                if (isAdmin)
                    $privileges.addClass('admin');
                else
                    $privileges.removeClass('admin');
            }

            function before() {
                pr.form.ajaxBefore.call(this, {
                    controlId: controlId,
                    message: 'Updating ...'
                });
            }

            function after(data) {
                var isError = !pr.form.ajaxAfter.apply(this, arguments);
                if (isError)
                    return;

                $(document).trigger(events.doctorPrivilegesEdited);
            }

            function error() {
                pr.form.ajaxError.apply(this, arguments);
            }

            return {
                init: init,
                before: before,
                after: after,
                error: error,
                events: events
            };
        })();

        var deletePermanently = (function () {
            var events = {
                doctorDeleted: 'doctorDeleted'
            };

            function init() {
                $(document).on('click', '#deleteDoctor', deleteClick);
            }

            function deleteClick() {
                event.preventDefault();
                event.stopPropagation();

                if (pr.confirm('Delete this account permanently?') == false)
                    return;

                var id = $(editBasics.containerId).find('#Id').val();
                deletePermanently(id);
            }

            function deletePermanently(id) {
                var $form = $(editBasics.containerId).find('form');

                $.ajax({
                    url: '/Settings/DoctorDelete',
                    data: { id: id },
                    beforeSend: function () {
                        pr.form.ajaxBefore.call($form, {
                            controlId: editBasics.controlId,
                            message: 'Deleting ...'
                        });
                    },
                    success: function (data) {
                        var isError = !pr.form.ajaxAfter.apply($form, arguments);
                        if (isError)
                            return;

                        $(document).trigger(events.doctorDeleted);
                    },
                    error: function () {
                        pr.form.ajaxError.apply($form, arguments);
                    }
                });
            }

            return {
                init: init,
                events: events
            }
        })();

        $.extend(events, add.events, editBasics.events,
                         editPrivileges.events, deletePermanently.events);

        return {
            init: init,
            add: add,
            editBasics: editBasics,
            editPrivileges: editPrivileges,
            deletePermanently: deletePermanently,
            events: events
        };
    })();

    $.extend(events, myInfo.events, password.events,
             practice.events, calendar.events, staff.events,
             doctor.events);

    return {
        init: init,
        events: events,
        myInfo: myInfo,
        password: password,
        practice: practice,
        calendar: calendar,
        staff: staff,
        doctor: doctor
    }
})();