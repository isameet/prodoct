﻿var pr = pr || {};

pr.nav = (function () {
    function init(id) {
        setActive(id);
        $(document).on('click', '.list-group-horizontal > .list-group-item', listGroupItemClick);
    }

    function listGroupItemClick(event) {
        event.preventDefault();

        var $this = $(this),
            $list = $this.closest('.list-group');

        $list.find('.active').removeClass('active');
        $this.addClass('active');
    }

    function setActive(id) {
        $(id).addClass('active');
    }

    function mainMenu(id) {
        var $menu = $('.menu-nav');
        $menu.find('.active').removeClass('active');
        $menu.find(id).addClass('active');
    }

    return {
        init: init,
        mainMenu: mainMenu
    }
})();