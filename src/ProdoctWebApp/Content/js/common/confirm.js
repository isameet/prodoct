﻿var pr = pr || {};

pr.confirm = function (params) {
    return pr.confirm.internal.run(params);
}

pr.confirm.init = function () {
    pr.confirm.internal.init();
}

pr.confirm.internal = (function () {
    function init() {

    }

    function run(params) {
        if (pr.helpers.isString(params)) {
            return confirmModal(params);
        }

        return false;
    }

    function confirmModal(msg) {
        if (confirm(msg)) {
            return true;
        }

        return false;
    }

    return {
        init: init,
        run: run
    };
})();