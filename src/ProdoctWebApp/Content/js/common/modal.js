﻿var pr = pr || {};

pr.modal = (function () {
    var events = {
        beforeLoadingModal: 'beforeLoadingModal',
        afterLoadingModal: 'afterLoadingModal',
        modalClosed: 'modalClosed'
    };

    function init() {
        $(document).on('click', '.modal-link', modalLinkClick);
        $(document).on('click', '.modal-close', modalCloseClick);
    }

    function modalLinkClick() {
        var modalId = '#' + $(this).data('modal');
        show(modalId);
    }

    function modalCloseClick() {
        var $modal = $(this).closest('.pr-modal');
        close($modal);
    }

    function show(modal) {
        var $modal = tryGetModal(modal);
        if ($modal) {
            $modal.fadeIn(150, function () {
                pr.form.focusRelevantInput($modal);
            });
        }
    }

    function close(params) {
        if (pr.helpers.isString(params) || params instanceof jQuery)
            params = { modal: params };

        var settings = $.extend({}, {
            modal: '.pr-modal:visible',
            speed: 100,
            empty: false
        }, params);

        var $modal = tryGetModal(settings.modal);
        if ($modal) {
            if (settings.speed > 0)
                $modal.fadeOut(settings.speed);
            else
                $modal.hide();

            if (settings.empty)
                $modal.empty();

            $(document).trigger(events.modalClosed);
        }
    }

    function tryGetModal(modal) {
        var $modal = (modal instanceof jQuery) ? modal : $(modal);
        if ($modal.length == 0)
            return null;

        if ($modal.hasClass('pr-modal') == false)
            $modal = $modal.find('.pr-modal');

        if ($modal.length == 0)
            return null;

        return $modal;
    }

    function ajaxBefore(params) {
        var settings = $.extend({}, { heading: 'Loading ...', html: '' }, params);
        ajaxProgress(settings);
        show(settings.modalId);
    }

    function ajaxAfter(params) {
        var settings = $.extend({}, { heading: 'Loaded', html: '' }, params);
        ajaxProgress(settings);
    }

    function ajaxError(params) {
        var settings = $.extend({}, { heading: 'An Error Occured', html: '' }, params);
        ajaxProgress(settings);
    }

    function ajaxProgress(settings) {
        var $modal = tryGetModal(settings.modalId);
        if ($modal.length == 0)
            return;

        $modal.find('.modal-ajax-content').html(settings.html);
        $modal.find('.pr-modal-title').find('h3').text(settings.heading);
    }

    return {
        init: init,
        close: close,
        show: show,
        ajaxBefore: ajaxBefore,
        ajaxAfter: ajaxAfter,
        ajaxError: ajaxError,
        events: events
    };
})();