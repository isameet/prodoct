﻿var pr = pr || {};

pr.form = (function () {
    function init() {
        initValidationFlash();
    }

    function initValidationFlash() {
        try {
            var originalForm = $.validator.prototype.form;
            $.validator.prototype.form = function () {
                var returnValue = originalForm.call(this);
                if (!returnValue) {
                    showValidationFlash(this);
                }
                return returnValue;
            };
        }
        catch (err) { }
    }

    function showValidationFlash(validator) {
        pr.flash.error({ message: validator.errorList[0].message });
    }

    function rebindValidation (form) {
        var $form = tryGetForm(form);
        if($form == null)
            return;

        $form.removeData('validator');
        $form.removeData('unobtrusiveValidation');
        $.validator.unobtrusive.parse($form);
    }

    function tryGetForm (selector) {
        var $el = selector instanceof jQuery ? selector : $(selector);
        var $form = $el.is('form') ? $el : $el.find('form');
        return $form.length == 0 ? null : $form;
    }

    function ajaxBefore(params) {
        var settings = $.extend({}, {
            noFlash: false,
            controlId: '',
            message: ''
        }, params);

        disableSubmitter($(this), settings);

        if (settings.noFlash)
            return;

        pr.flash.loading({ message: params.message });
    }

    function ajaxAfter() {
        enableSubmitter($(this));

        var data = arguments[0];
        if (!data) {
            pr.flash.error();
            return false;
        }

        if (data.HasError) {
            pr.flash.error({ message: data.Message || 'An Error Occured' });
            return false;
        }

        if (data.Message) {
            pr.flash.success({ message: data.Message });
        }
        else {
            pr.flash.remove();
        }

        return true;
    }

    function ajaxError() {
        enableSubmitter($(this));
        var message = arguments[2] || 'An Error Occured';
        pr.flash.error({ message: message });
    }

    function disableSubmitter($form, params) {
        if ($form.is('form') == false)
            return;

        var $submitters = $form.find('.submitter'),
            $submitter,
            controlId = (params.controlId || '').replace('#', '');

        $.each($submitters, function (i, submitter) {
            $submitter = $(submitter);

            $submitter.removeClass($submitter.data('remove-on-submit'));
            $submitter.addClass($submitter.data('add-on-submit'));

            if ($submitter.is('button')) {
                $submitter.prop('disabled', true);
            }

            if ($submitter.prop('id') == controlId) {
                $submitter.data('text-replaced', $submitter.text());
                $submitter.text(params.message);
            }
        });
        
    }

    function enableSubmitter($form) {
        if ($form.is('form') == false)
            return;

        var $submitters = $form.find('.submitter'),
            $submitter;

        $.each($submitters, function (i, submitter) {
            $submitter = $(submitter);

            $submitter.removeClass($submitter.data('add-on-submit'));
            $submitter.addClass($submitter.data('remove-on-submit'));

            if ($submitter.is('button')) {
                $submitter.prop('disabled', false);
            }

            var originalText = $submitter.data('text-replaced');
            if (originalText) {
                $submitter.data('text-replaced', '');
                $submitter.text(originalText);
            }
        });
    }

    function focusRelevantInput (selector) {
        var $form = tryGetForm(selector);
        if ($form == null)
            return;

        var $specifiedField = $form.find('.focus-on-load:first');
        if ($specifiedField.length == 0)
            return;

        $specifiedField.focus();
    }

    return {
        init: init,
        ajaxBefore: ajaxBefore,
        ajaxAfter: ajaxAfter,
        ajaxError: ajaxError,
        rebindValidation: rebindValidation,
        focusRelevantInput: focusRelevantInput
    };
})();