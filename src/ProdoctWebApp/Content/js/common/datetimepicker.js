﻿var pr = pr || {};

pr.datetimepicker = (function () {
    var clss = '.datetimepicker';

    function init(selector) {
        selector = selector || clss;

        if (selector.indexOf(clss) == -1)
            selector = selector + ' ' + clss;

        $(selector).datetimepicker({
            sideBySide: true,
            widgetPositioning: {
                horizontal: 'left'
            }
        });
    }

    function render(data) {
        var $datetimepicker = tryGetDateTimePicker(data.datetimepicker);
        if ($datetimepicker == null)
            return;

        $datetimepicker.data('DateTimePicker').date(data.date || new Date())
    }

    function tryGetDateTimePicker(datetimepicker) {
        var $datetimepicker = (datetimepicker instanceof $) ? datetimepicker : $(datetimepicker);
        if ($datetimepicker.length == 0 || $datetimepicker.hasClass('datetimepicker') == false)
            return null;

        return $datetimepicker;
    }

    return {
        init: init,
        render: render
    };
})();