﻿var pr = pr || {};

pr.helpers = (function () {
    function init() {
    }

    function isString(x) {
        if (x instanceof jQuery)
            return false;

        return (typeof x == 'string' || x instanceof String);
    }

    function isFunction(x) {
        if ($.isFunction(x))
            return true;

        return false;
    }

    function getObjectFromString(s, o) {
        if (!s || !o)
            return;

        s = s.replace(/\[(\w+)\]/g, '.$1');
        s = s.replace(/^\./, '');           
        var a = s.split('.');
        while (a.length) {
            var n = a.shift();
            if (n in o) {
                o = o[n];
            } else {
                return;
            }
        }
        return o;
    }

    function getUrlParam(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    function getDateParts(date) {
        date = date == undefined ? new Date() : date;

        date = new Date(date);

        var year = date.getFullYear(),
            day = date.getDate(),
            monthNumber = date.getMonth(),
            month = getMonthName(monthNumber),
            hour = date.getHours(),
            minutes = date.getMinutes(),
            seconds = date.getSeconds(),
            am = 'AM';

        if (hour >= 12) {
            if(hour > 12)
                hour -= 12;

            am = 'PM';
        }
        else if (hour == 0)
            hour = 12;

        if (minutes < 10) {
            minutes = '0' + minutes;
        }

        if (seconds < 10) {
            seconds = '0' + seconds;
        }

        return {
            year: year,
            yearShort: year.toString().substr(2,2),
            month: month,
            monthShort: month.substring(0, 3),
            monthNumber: monthNumber + 1,
            day: day,
            hour: hour,
            minutes: minutes,
            seconds: seconds,
            am: am
        };
    }

    function getMonthName(month, short) {
        var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

        if (short == undefined) {
            short = false;
        }

        if (short) {
            return months[month].substring(0, 3);
        }

        return months[month];
    }

    return {
        init: init,
        isString: isString,
        isFunction: isFunction,
        getUrlParam: getUrlParam,
        getObjectFromString: getObjectFromString,
        getDateParts: getDateParts
    };
})();