﻿var pr = pr || {};

pr.tabs = (function () {
    var events = {
        tabChanging: 'tabChanging'
    };

    function init() {
        $(document).on('shown.bs.tab', 'a[data-toggle="tab"]', tabChanging);
    }

    function tabChanging(event) {
        var $tab = $(this);
        
        if ($tab.data('fire-change') == false)
            return;

        $(document).trigger(events.tabChanging);
    }

    return {
        init: init,
        events: events
    };
})();