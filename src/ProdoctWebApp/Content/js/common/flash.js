﻿var pr = pr || {};

pr.flash = (function () {
    var ignoreClick = false;

    var showDefaults = {
        duration: 8000,
        speed: 300
    };

    var removeDefaults = {
        speed: 300,
        selector: '.flash'
    };

    function init() {
        $(document).on('click', 'body', click);
    }

    function loading(params) {
        var settings = $.extend({},
                                showDefaults,
                                { message: 'Loading ...', css: 'loading' },
                                params);
        show(settings);
    }

    function info(params) {
        var settings = $.extend({},
                                showDefaults,
                                { css: 'info' },
                                params);
        show(settings);
    }

    function error(params) {
        var settings = $.extend({},
                                showDefaults,
                                { message: 'Error!', css: 'error' },
                                params);
        show(settings);
    }

    function success(params) {
        var settings = $.extend({},
                                showDefaults,
                                { message: 'Successful', css: 'success' },
                                params);
        show(settings);
    }

    function show(params) {
        params.message = $.trim(params.message);
        if (!params.message)
            return;

        removeImmediately();

        var flash = '<div class="flash ' + params.css + '">' + params.message + '</div>';
        var $flash = $(flash).appendTo('body');
        $flash.css('left', ($(window).width() / 2) - ($flash.width() / 2));
        $flash.slideDown(params.speed);
    }

    function remove(params) {
        var settings = $.extend({}, removeDefaults, params),
            $flash = $(settings.selector);

        if ($flash.length > 0 && $flash.hasClass('flash')) {
            $flash.slideUp(settings.speed, function () { $flash.remove() });
        }
    }

    function removeImmediately(params) {
        var settings = $.extend({}, removeDefaults, { speed: 0 }, params);
        remove(settings);
    }

    function click() {
        if (ignoreClick) {
            ignoreClick = false;
            return;
        }

        var $flash = $(removeDefaults.selector);

        if ($flash.length > 0 && $flash.hasClass('loading') == false) {
            remove();
        }
    }

    function preventClick() {
        ignoreClick = true;
    }

    return {
        init: init,
        loading: loading,
        info: info,
        error: error,
        success: success,
        remove: remove,
        preventClick: preventClick
    };
})();