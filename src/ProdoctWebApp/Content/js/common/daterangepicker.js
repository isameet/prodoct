﻿var pr = pr || {};

pr.daterangepicker = (function () {
    var clss = '.input-daterange',
        events = {
            dateRangeChanged: 'dateRangeChanged'
        };

    function init() {
    }

    function reload(selector) {
        var $dateRangePicker = tryGetDateRangePicker(selector);
        if ($dateRangePicker == null)
            return;

        $dateRangePicker.datepicker({
            autoclose: true,
            format: 'd M, yy',
            todayHighlight: true,
            weekStart: 1,
            keyboardNavigation: false,
            forceParse: false
        }).on('changeDate', dateChanged);

        var $parent = $dateRangePicker.parent();
        $parent.find('.stepper.next:first').click(nextClick);
        $parent.find('.stepper.previous:first').click(previousClick);
    }

    function dateChanged(e) {
        var $this = $(this),
            start = new Date($this.find('.start:first').val()),
            end = new Date($this.find('.end:first').val());

        if (!start || !end) {
            console.log('Invalid dates : ' + start + ', ' + end);
            return;
        }

        if (start > end)
            return;

        var days = Math.floor((end - start) / 86400000);
        if (days < 0 || days > 31) {
            console.log('Range limit exceeded : ' + end + ' - ' + start + ' = ' + days);
            return;
        }

        $(document).trigger(events.dateRangeChanged);
    }

    function nextClick() {
        step.call(this, 1);
    }

    function previousClick() {
        step.call(this, -1);
    }

    function step(multiplier) {
        var $range = $(this).parent().find(clss),
            $start = $range.find('.start:first'),
            $end = $range.find('.end:first'),
            start = new Date($start.val()),
            end = new Date($end.val());

        if (!start || !end) {
            console.log('Invalid dates : ' + start + ', ' + end);
            return;
        }

        if (start > end)
            return;

        var days = Math.floor((end - start) / 86400000) * multiplier;
        $start.val(getDateInFormat(addDays(start, days + multiplier)));
        $end.val(getDateInFormat(addDays(end, days + multiplier)));

        $(document).trigger(events.dateRangeChanged);
    }

    function addDays(date, daysToAdd) {
        date.setDate(date.getDate() + daysToAdd);
        return date;
    }

    function getDateInFormat(date) {
        var parts = pr.helpers.getDateParts(date);
        return parts.day.toString() + ' ' + parts.monthShort + ', ' + parts.yearShort;
    }

    function tryGetDateRangePicker(daterangepicker) {
        if (!daterangepicker)
            daterangepicker = clss;

        var $daterangepicker = (daterangepicker instanceof jQuery) ? daterangepicker : $(daterangepicker);
        if ($daterangepicker.length == 0)
            return null;

        if ($daterangepicker.hasClass('input-daterange') == false)
            $daterangepicker = $daterangepicker.find(clss);

        if ($daterangepicker.length == 0)
            return null;

        return $daterangepicker;
    }

    return {
        init: init,
        events: events,
        reload: reload
    };
})();