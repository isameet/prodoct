﻿var pr = pr || {};

pr.datepicker = (function () {
    var clss = '.datepicker';

    function init() {
    }

    function reload(selector) {
        var $datepicker = tryGetDatepicker(selector);
        if ($datepicker == null)
            return;

        $datepicker.datepicker({
            autoclose: true,
            format: 'd MM, yyyy',
            todayHighlight: true,
            weekStart: 1,
            keyboardNavigation: false,
            forceParse: false
        });

        var $el;
        $.each($datepicker, function (i, datepicker) {
            $el = $(datepicker);
            if ($el.val() == '' && $el.hasClass('no-default') == false)
                $el.datepicker('setDate', new Date());
        });
    }

    function tryGetDatepicker(datepicker) {
        if (!datepicker)
            datepicker = '.datepicker';

        var $datepicker = (datepicker instanceof jQuery) ? datepicker : $(datepicker);
        if ($datepicker.length == 0)
            return null;

        if ($datepicker.hasClass('datepicker') == false)
            $datepicker = $datepicker.find('.datepicker');

        if ($datepicker.length == 0)
            return null;

        return $datepicker;
    }

    return {
        init: init,
        reload: reload
    };
})();