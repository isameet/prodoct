﻿var pr = pr || {};

pr.collapsible = (function () {
    function init() {
        $('.collapsible-header').click(toggle);
        $(document).on('click', '.list-item', listItemClick);
    }

    function toggle() {
        var $collapsible = $(this).parent();
        if ($collapsible.hasClass('collapsible')) {
            $collapsible.toggleClass('collapsed');
            $collapsible.toggleClass('expanded');
        }
    }

    function listItemClick() {
        var $item = $(this),
            $list = $item.parent();

        $list.find('.active').removeClass('active');
        $item.addClass('active');
    }

    return {
        init: init
    };
})();