﻿var pr = pr || {};

pr.ajax = (function () {
    var events = {
        ajaxDataLoaded: 'ajaxDataLoaded'
    };

    function init() {
        $(document).on('click', '[data-get]', ajaxGetterClick);
        $(document).on('change', 'select.ajax-getter', ajaxGetterSelectChange);
        $(document).ajaxError(onAjaxError);
        $(document).ajaxSuccess(onAjaxSuccess);
    }

    function ajaxGetterClick(event) {
        var $el = $(this);

        if (event) {
            if ($el.data('prevent-click-load'))
                return;
        }

        var get = $el.data('get'),
            $target = $($el.data('target')),
            broadcastOnLoad = $el.data('broadcast-on-load'),
            dataFunction = pr.helpers.getObjectFromString($el.data('data-function'), pr),
            data = {};

        if (!get || $target.length == 0)
            return;

        if (pr.helpers.isFunction(dataFunction)) {
            data = dataFunction();
        }

        load({
            get: get, 
            target: $target, 
            loadingText: $el.data('loading-message'), 
            broadcastOnLoad: broadcastOnLoad,
            data: data
        });
    }

    function ajaxGetterSelectChange() {
        var $option = $(this).find('option:selected');
        ajaxGetterClick.call($option);
    }

    function load(params) {
        var p = $.extend({}, {
            get: null,
            target: null,
            loadingText: 'Loading ...',
            broadcastOnLoad: null,
            data: {}
        }, params);

        if (!p.get || !p.target) {
            console.log('Invalid params : ' + p);
            return;
        }

        var $target = (p.target instanceof jQuery) ? p.target : $(p.target);

        $target.empty();
        $target.html(getLoadingHtml(p.loadingText));

        $.get(p.get, p.data, function (html) {
            $target.html(html);

            pr.form.rebindValidation($target);
            pr.form.focusRelevantInput($target);

            $(document).trigger(events.ajaxDataLoaded, [{ $target: $target }]);

            if (p.broadcastOnLoad) {
                var eventsToBroadcast = p.broadcastOnLoad.split(' ');
                $.each(eventsToBroadcast, function (i, eventToBroadcast) {
                    eventToBroadcast = $.trim(eventToBroadcast);
                    if(eventsToBroadcast)
                        $(document).trigger(eventToBroadcast);
                });
            }

            ajaxGetterClick.call($target);
        });
    }

    function getLoadingHtml(loadingMessage) {
        loadingMessage = loadingMessage || 'Loading ...';

        return '<div class="tab-loading">' +
                    '<h3>' + loadingMessage + '</h3>' +
               '</div>';
    }

    function onAjaxError (event, jqXhr, ajaxSettings, thrownError) {
        var urlParts = ajaxSettings.url.split('?'),
            url = urlParts[0],
            queryString = urlParts.length > 1 ? urlParts[1] : '';

        var message = {
            url: '[' + ajaxSettings.type + ']' + ': ' + url,
            query: queryString,
            httpStatus: jqXhr.status,
            error: thrownError || jqXhr.statusText
        };

        console.log(message);

        if (message.httpStatus == 401) {
            window.location = '/settings';
        }
    }

    function onAjaxSuccess (event, jqXHR, ajaxSettings, data ) {
        if (data.RedirecUrl) {
            window.location = data.RedirecUrl;
        }
    }

    return {
        init: init,
        load: load,
        events: events
    };
})();