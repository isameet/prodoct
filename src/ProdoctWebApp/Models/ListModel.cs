﻿
namespace ProdoctWebApp.Models
{
    public class ListModel
    {
        public int Skip { get; set; }

        public bool MoreExist { get; set; }

        public ListModel()
        {
        }
    }
}