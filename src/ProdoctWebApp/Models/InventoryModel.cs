﻿using ProdoctCommon.Data;
using ProdoctCommon.Extensions;
using ProdoctCommon.Helpers;
using ProdoctWebApp.Helpers;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

namespace ProdoctWebApp.Models
{
    public class ItemAddModel
    {
        [Required]
        public string Name { get; set; }
        public string Code { get; set; }
        public string Manufacturer { get; set; }

        [Required(ErrorMessage="Stocking Unit required")]
        public string StockingUnit { get; set; }

        [Required(ErrorMessage="Reorder Level required")]
        public int ReorderLevel { get; set; }

        //public string Type { get; set; }
        //public List<SelectListItem> AllTypes { get; set; }

        public ItemAddModel()
        {
            //AllTypes = SelectListHelper.GetItemTypes();
        }

        internal Item ToItem()
        {
            return new Item
            {
                Id = ObjectIdHelper.GenerateNewIdString(),
                CreatorId = APP.Common.UserId,
                PracticeId = APP.Common.PracticeId,
                Name = Name,
                Code = Code,
                Manufacturer = Manufacturer,
                StockingUnit = StockingUnit,
                Type = Item.ItemType.Drug,
                ReorderLevel = ReorderLevel
            };
        }
    }




    public class ItemListModel : ListModel
    {
        public List<ItemModel> Items { get; set; }

        public ItemListModel()
        {
            Items = new List<ItemModel>();
        }
    }

    public class ItemModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public int ReorderLevel { get; set; }
        public int TotalStock { get; set; }
        public int AvailableStock { get; set; }
        public int ExpiredStock { get; set; }
        public string StockingUnit { get; set; }

        public ItemModel()
        {
        }

        public ItemModel(Item item) : this()
        {
            Id = item.Id;
            Name = item.Name;
            Code = item.Code;
            ReorderLevel = item.ReorderLevel;
            TotalStock = item.Stock.Total;
            AvailableStock = item.Stock.Available;
            ExpiredStock = item.Stock.Expired;
            StockingUnit = item.StockingUnit;
        }
    }




    public class InventoryIndexModel
    {
        public ItemListModel ItemList { get; set; }

        public InventoryIndexModel()
        {
            ItemList = new ItemListModel();
        }
    }




    public class ItemEditContainerModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }

        public string DefaultTab { get; set; }

        public ItemEditContainerModel()
        {
        }

        public ItemEditContainerModel(Item item)
        {
            Id = item.Id;
            Name = item.Name;
            Code = item.Code;
            DefaultTab = item.Stock.Available > 0 ? "consumeStockTabLink" : "addStockTabLink";
        }
    }

    public class ItemEditModel
    {
        public string Id { get; set; }

        [Required]
        public string Name { get; set; }
        public string Code { get; set; }
        public string Manufacturer { get; set; }

        [Required(ErrorMessage = "Stocking Unit required")]
        public string StockingUnit { get; set; }

        [Required(ErrorMessage = "Reorder Level required")]
        public int ReorderLevel { get; set; }

        public ItemEditModel()
        {
        }

        public ItemEditModel(Item item) : this()
        {
            Id = item.Id;
            Name = item.Name;
            Code = item.Code;
            Manufacturer = item.Manufacturer;
            StockingUnit = item.StockingUnit;
            ReorderLevel = item.ReorderLevel;
        }

        internal void ToItem(Item item)
        {
            item.Name = Name;
            item.Code = Code;
            item.Manufacturer = Manufacturer;
            item.StockingUnit = StockingUnit;
            item.ReorderLevel = ReorderLevel;
        }
    }




    public class StockAddModel
    {
        public string ItemId { get; set; }

        [Required(ErrorMessage="Please provide batch name")]
        public string BatchName { get; set; }
        
        public string Vendor { get; set; }
        
        [Required(ErrorMessage="Please specify date when stock added")]
        public string AddedDate { get; set; }

        [Required(ErrorMessage = "Please specify expiry date")]
        public string ExpiryDate { get; set; }

        [Required(ErrorMessage = "Please specify stock size")]
        public int? TotalStock { get; set; }
        public string StockingUnit { get; set; }

        public StockAddModel()
        {
        }

        public StockAddModel(Item item)
        {
            ItemId = item.Id;
            StockingUnit = item.StockingUnit;
        }

        internal ItemBatch ToBatch()
        {
            return new ItemBatch
            {
                Id = ObjectIdHelper.GenerateNewIdString(),
                Name = BatchName,
                Vendor = Vendor,
                Added = DateTimeHelper.FromDatePicker(AddedDate),
                Expiry = DateTimeHelper.FromDatePicker(ExpiryDate),
                Total = TotalStock.Value,
                Available = TotalStock.Value
            };
        }

        internal ItemHistory ToItemHistory(Item item, ItemBatch batch)
        {
            return new ItemHistory
            {
                Id = ObjectIdHelper.GenerateNewIdString(),
                Type = ItemHistory.TransactionType.Added,
                ItemId = item.Id,
                BatchId = batch.Id,
                CreatorId = APP.Common.UserId,
                PracticeId = APP.Common.PracticeId,
                Date = batch.Added,
                Description = "Added",
                Quantity = batch.Total
            };
        }
    }




    public class StockListModel : ListModel
    {
        public string StockingUnit { get; set; }

        public List<StockModel> Batches { get; set; }

        public StockListModel()
        {
            Batches = new List<StockModel>();
        }
    }

    public class StockModel
    {
        public string BatchId { get; set; }
        public string BatchName { get; set; }
        public string Vendor { get; set; }
        public string AddedDate { get; set; }
        public string ExpiryDate { get; set; }
        public int Total { get; set; }
        public int Available { get; set; }
        public int Expired { get; set; }

        public StockModel()
        {
        }

        public StockModel(ItemBatch batch)
        {
            var timezone = APP.Common.Practice.Timezone;
            BatchId = batch.Id;
            BatchName = batch.Name;
            Vendor = batch.Vendor;
            AddedDate = batch.Added.InTimezone(timezone).ToString(DateTimeFormats._8Mar87);
            ExpiryDate = batch.Expiry.InTimezone(timezone).ToString(DateTimeFormats._March1987);
            Total = batch.Total;
            Available = batch.Available;
            Expired = batch.Expired;
        }
    }




    public class StockConsumeModel
    {
        public string ItemId { get; set; }
        public string StockingUnit { get; set; }

        [Required(ErrorMessage = "Please select the batch")]
        public string BatchId { get; set; }
        public List<SelectListItem> AllBatches { get; set; }

        [Required(ErrorMessage="Please select type of consumption")]
        public string Type { get; set; }
        public List<SelectListItem> AllTypes { get; set; }

        [Required(ErrorMessage = "Please specify date of consumption")]
        public string Date { get; set; }

        [Required(ErrorMessage = "Please specify quantity consumed")]
        public int? Quantity { get; set; }

        public StockConsumeModel()
        {
        }

        public StockConsumeModel(Item item)
        {
            ItemId = item.Id;
            StockingUnit = item.StockingUnit;
            AllBatches = SelectListHelper.GetBatches(item);
            AllTypes = SelectListHelper.GetStockConsumptionTypes();
        }

        internal ItemHistory ToItemHistory(Item item, ItemBatch batch)
        {
            return new ItemHistory(item, batch)
            {
                Type = ItemHistory.TransactionType.Consumed,
                CreatorId = APP.Common.UserId,
                Date = DateTimeHelper.FromDatePicker(Date),
                Description = string.Format("{0}", Type),
                Quantity = Quantity.HasValue ? Quantity.Value : 0
            };
        }
    }




    public class ItemHistoryListModel : ListModel
    {
        public string StockingUnit { get; set; }

        public List<ItemHistoryModel> Transactions { get; set; }

        public ItemHistoryListModel()
        {
            Transactions = new List<ItemHistoryModel>();
        }
    }

    public class ItemHistoryModel
    {
        public string Id { get; set; }
        public string Date { get; set; }
        public string Description { get; set; }
        public string BatchName { get; set; }
        public int Quantity { get; set; }

        public ItemHistoryModel()
        {
        }

        public ItemHistoryModel(ItemHistory history, Item item, List<Doctor> doctors, List<Patient> patients)
        {
            Id = history.Id;

            var timezone = APP.Common.Practice.Timezone;
            Date = history.Date.InTimezone(timezone).ToString(DateTimeFormats._8Mar87);

            var multiplier = history.Type == ItemHistory.TransactionType.Added ? 1 : -1;
            Quantity = history.Quantity * multiplier;

            var batch = item.Stock.Batches.FirstOrDefault(x => x.Id == history.BatchId);
            BatchName = batch == null ? string.Empty : batch.Name;

            Description = GetDescription(history, item, batch, doctors, patients);
        }

        private string GetDescription(ItemHistory history, Item item, ItemBatch batch, List<Doctor> doctors, List<Patient> patients)
        {
            var description = history.Description;

            if (history.Type == ItemHistory.TransactionType.Consumed)
            {
                var doctor = doctors.FirstOrDefault(x => x.Id == history.DoctorId);
                var patient = patients.FirstOrDefault(x => x.Id == history.PatientId);
                if (doctor != null && patient != null)
                {
                    description = string.Format("{0} ({1} / {2})", history.Description, doctor.Name, patient.Name);
                }
            }
            else if (history.Type == ItemHistory.TransactionType.Added)
            {
                if (batch != null && !string.IsNullOrWhiteSpace(batch.Vendor))
                    description = string.Format("Added (Vendor: {0})", batch.Vendor);
            }

            return description;
        }
    }




    public class GetMedicinesModel
    {
        public string MedicineId { get; set; }
        public List<SelectListItem> AllMedicines { get; set; }

        public GetMedicinesModel()
        {
            AllMedicines = SelectListHelper.GetAvailableMedicines();
        }
    }

    public class IssueMedicineModel : StockConsumeModel
    {
        public string MedicineName { get; set; }
        public string AppointmentId { get; set; }
        public string PatientId { get; set; }
        public string PatientName { get; set; }
        public string PatientMobile { get; set; }
        public string PatientLocality { get; set; }
        public string DoctorId { get; set; }
        public List<SelectListItem> AllDoctors { get; set; }

        public IssueMedicineModel()
        {
        }

        public IssueMedicineModel(Item item, Appointment appointment) : base(item)
        {
            MedicineName = item.Name;
            var timezone = APP.Common.Practice.Timezone;
            Date = appointment.Date.InTimezone(timezone).ToString(DateTimeFormats.DatePicker);
            AppointmentId = appointment.Id;
            PatientId = appointment.PatientId;
            DoctorId = appointment.DoctorId;
            AllDoctors = SelectListHelper.GetDoctors();

            var patient = APP.Common.DB.Patients.GetById(PatientId);
            if(patient != null)
            {
                PatientName = patient.Name;
                PatientMobile = patient.Mobile;
                PatientLocality = patient.Locality;
            }
        }

        internal ItemHistory ToItemHistory(Item item, ItemBatch batch, Appointment appointment)
        {
            var history = base.ToItemHistory(item, batch);
            history.AppointmentId = appointment.Id;
            history.PatientId = appointment.PatientId;
            history.DoctorId = appointment.DoctorId;

            return history;
        }
    }
}