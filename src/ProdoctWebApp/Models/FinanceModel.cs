﻿using ProdoctCommon.Data;
using ProdoctCommon.Extensions;
using ProdoctCommon.Helpers;
using ProdoctWebApp.Helpers;
using ProdoctWebApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace ProdoctWebApp.Models
{
    public static class FinanceModel
    {
        public static string GetViewName(string viewName)
        {
            return string.Format("~/Views/Finance/{0}.cshtml", viewName);
        }
    }




    public class IncomeAddModel
    {
        [Required]
        public string Category { get; set; }
        public List<SelectListItem> AllCategories { get; set; }

        [Required]
        public string Date { get; set; }
        
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:#.##}")]
        [Required]
        public decimal Amount { get; set; }
        
        [Required]
        public string Mode { get; set; }
        public List<SelectListItem> AllModes { get; set; }

        public string Notes { get; set; }

        [Required(ErrorMessage="Please select a Patient")]
        public string PatientNameOrMobile { get; set; }

        [Required(ErrorMessage = "Fees can only be added for an existing Patient")]
        public string PatientId { get; set; }

        [Required(ErrorMessage = "Please select a Doctor")]
        public string DoctorId { get; set; }
        public List<SelectListItem> AllDoctors { get; set; }

        public IncomeAddModel()
        {
            AllModes = SelectListHelper.GetIncomeOrExpenseModes();
            AllCategories = SelectListHelper.GetIncomeCategories();
            AllDoctors = SelectListHelper.GetDoctors();
        }

        internal Income ToIncome()
        {
            var income = new Income
            {
                Id = ObjectIdHelper.GenerateNewIdString(),
                Category = Category,
                Date = DateTimeHelper.FromDatePicker(Date),
                Amount = Amount,
                Mode = Mode,
                Notes = Notes,
                CreatorId = APP.Common.UserId,
                PracticeId = APP.Common.PracticeId
            };

            return income;
        }

        internal Fees ToFees()
        {
            Fees fees = new Fees(ToIncome());
            fees.DoctorId = DoctorId;
            fees.PatientId = PatientId;
            return fees;
        }
    }

    public class IncomeEditModel
    {
        public string Id { get; set; }

        [Required]
        public string Category { get; set; }
        public List<SelectListItem> AllCategories { get; set; }

        [Required]
        public string Date { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:#.##}")]
        [Required]
        public decimal Amount { get; set; }

        [Required]
        public string Mode { get; set; }
        public List<SelectListItem> AllModes { get; set; }

        public string Notes { get; set; }

        public IncomeEditModel()
        {
            AllModes = SelectListHelper.GetIncomeOrExpenseModes();
            AllCategories = SelectListHelper.GetIncomeCategories();
            AllCategories.RemoveAll(x => x.Value == "Fees");
        }

        public IncomeEditModel(Income income) : this()
        {
            var timezone = APP.Common.Practice.Timezone;

            Id = income.Id;
            Category = income.Category;
            Date = income.Date.InTimezone(timezone).ToString(DateTimeFormats.DatePicker);
            Amount = income.Amount;
            Mode = income.Mode;
            Notes = income.Notes;
        }

        internal void ToIncome(Income income)
        {
            income.Category = Category;
            income.Date = DateTimeHelper.FromDatePicker(Date);
            income.Amount = Amount;
            income.Mode = Mode;
            income.Notes = Notes;
        }
    }

    public class FeesAddModel : IncomeAddModel
    {
        [Required]
        public string AppointmentId { get; set; }

        public string PatientName { get; set; }
        public string PatientMobile { get; set; }
        public string PatientLocality { get; set; }

        public FeesAddModel() : base()
        {
        }

        public FeesAddModel(Appointment appointment) : this()
        {
            var timezone = APP.Common.Practice.Timezone;
            Date = appointment.Date.InTimezone(timezone).ToString(DateTimeFormats.DatePicker);
            
            AppointmentId = appointment.Id;
            DoctorId = appointment.DoctorId;
            PatientId = appointment.PatientId;
            Category = "Fees";

            var patient = APP.Common.DB.Patients.GetById(PatientId);
            if(patient != null)
            {
                PatientName = patient.Name;
                PatientMobile = patient.Mobile;
                PatientLocality = patient.Locality;
            }
        }

        internal Fees ToFees(Appointment appointment)
        {
            Fees fees = new Fees(ToIncome());
            fees.AppointmentId = appointment.Id;
            fees.DoctorId = appointment.DoctorId;
            fees.PatientId = appointment.PatientId;
            return fees;
        }
    }

    public class FeesEditModel : IncomeEditModel
    {
        public string PatientId { get; set; }
        public string PatientName { get; set; }
        public string PatientMobile { get; set; }
        public string PatientLocality { get; set; }

        [Required(ErrorMessage = "Please select a Doctor")]
        public string DoctorId { get; set; }
        public List<SelectListItem> AllDoctors { get; set; }

        public string AppointmentId { get; set; }

        public FeesEditModel()
        {
            AllModes = SelectListHelper.GetIncomeOrExpenseModes();
            AllCategories = new List<SelectListItem>();
            AllDoctors = SelectListHelper.GetDoctors();
        }

        public FeesEditModel(Fees fees)
            : this()
        {
            var timezone = APP.Common.Practice.Timezone;

            Id = fees.Id;
            Date = fees.Date.InTimezone(timezone).ToString(DateTimeFormats.DatePicker);
            Amount = fees.Amount;
            Mode = fees.Mode;
            Notes = fees.Notes;

            AppointmentId = fees.AppointmentId;
            DoctorId = fees.DoctorId;
            var patient = APP.Common.DB.Patients.GetById(fees.PatientId);
            if(patient != null)
            {
                PatientId = patient.Id;
                PatientName = patient.Name;
                PatientMobile = patient.Mobile;
                PatientLocality = patient.Locality;
            }
        }

        internal void ToFees(Fees fees)
        {
            fees.Date = DateTimeHelper.FromDatePicker(Date);
            fees.Amount = Amount;
            fees.Mode = Mode;
            fees.Notes = Notes;

            //allow changing doctor only if fees isn't linked with an appointment
            if (string.IsNullOrWhiteSpace(fees.AppointmentId))
            {
                fees.DoctorId = DoctorId;
            }
        }
    }




    public class ExpenseAddModel
    {
        [Required]
        public string Category { get; set; }
        public List<SelectListItem> AllCategories { get; set; }

        [Required]
        public string Date { get; set; }
        
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:#.##}")]
        [Required]
        public decimal Amount { get; set; }
        
        [Required]
        public string Mode { get; set; }
        public List<SelectListItem> AllModes { get; set; }

        public string Notes { get; set; }

        public ExpenseAddModel()
        {
            AllModes = SelectListHelper.GetIncomeOrExpenseModes();
            AllCategories = SelectListHelper.GetExpenseCategories();
        }

        internal Expense ToExpense()
        {
            var expense = new Expense
            {
                Id = ObjectIdHelper.GenerateNewIdString(),
                Category = Category,
                Date = DateTimeHelper.FromDatePicker(Date),
                Amount = Amount,
                Mode = Mode,
                Notes = Notes,
                CreatorId = APP.Common.UserId,
                PracticeId = APP.Common.PracticeId
            };

            return expense;
        }
    }

    public class ExpenseEditModel
    {
        public string Id { get; set; }

        [Required]
        public string Category { get; set; }
        public List<SelectListItem> AllCategories { get; set; }

        [Required]
        public string Date { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:#.##}")]
        [Required]
        public decimal Amount { get; set; }

        [Required]
        public string Mode { get; set; }
        public List<SelectListItem> AllModes { get; set; }

        public string Notes { get; set; }

        public ExpenseEditModel()
        {
            AllModes = SelectListHelper.GetIncomeOrExpenseModes();
            AllCategories = SelectListHelper.GetExpenseCategories();
        }

        public ExpenseEditModel(Expense expense) : this()
        {
            var timezone = APP.Common.Practice.Timezone;

            Id = expense.Id;
            Category = expense.Category;
            Date = expense.Date.InTimezone(timezone).ToString(DateTimeFormats.DatePicker);
            Amount = expense.Amount;
            Mode = expense.Mode;
            Notes = expense.Notes;
        }

        internal void ToExpense(Expense expense)
        {
            expense.Category = Category;
            expense.Date = DateTimeHelper.FromDatePicker(Date);
            expense.Amount = Amount;
            expense.Mode = Mode;
            expense.Notes = Notes;
        }
    }




    public class ReportsModel
    {
        public string Start { get; set; }
        public string End { get; set; }
        public List<ReportsSelectItemModel> AllReports { get; set; }

        public ReportsModel()
        {
            AllReports = new List<ReportsSelectItemModel>();
        }

        public ReportsModel(string timezone)
        {
            var now = DateTimeHelper.NowInZone(timezone);
            var start = now.StartOfWeek(DayOfWeek.Monday);

            Start = start.ToString(DateTimeFormats.DateRangePicker);
            End = start.AddDays(6).ToString(DateTimeFormats.DateRangePicker);
        }
    }

    public class ReportsSelectItemModel
    {
        public string Text { get; set; }
        public string Get { get; set; }
        public bool Selected { get; set; }
    }




    public class ReportAllIncomeModel
    {
        public string ChartJson { get; private set; }

        public IncomeListModel List { get; set; }

        public ReportAllIncomeModel()
        {
            List = new IncomeListModel();
        }

        public ReportAllIncomeModel(List<ChartDateAmount> chart, IncomeListModel list, DateTime start, DateTime end) : this()
        {
            ChartJson = new BarChartModel(chart, start, end).ToJson();
            List = list;
        }
    }

    public class IncomeListModel : ListModel
    {
        public List<IncomeModel> Incomes { get; set; }

        public IncomeListModel()
        {
            Incomes = new List<IncomeModel>();
        }
    }

    public class IncomeModel
    {
        public string Id { get; set; }
        public string Category { get; set; }
        public string PatientName { get; set; }
        public string DoctorName { get; set; }
        public string Mode { get; set; }
        public string Date { get; set; }
        public string Amount { get; set; }

        public IncomeModel()
        {
        }

        public IncomeModel(Income income, List<Fees> fees, List<Doctor> doctors, List<Patient> patients)
        {
            var timezone = APP.Common.Practice.Timezone;

            Id = income.Id;
            Category = income.Category;
            Mode = income.Mode;
            Date = income.Date.InTimezone(timezone).ToString(DateTimeFormats._8Mar87);
            Amount = income.Amount.To2Decimals();

            if (Category == "Fees")
            {
                var fee = fees.FirstOrDefault(x => x.Id == income.Id);
                if (fee == null)
                    return;

                var patient = patients.FirstOrDefault(x => x.Id == fee.PatientId);
                PatientName = patient == null ? string.Empty : patient.Name;

                var doctor = doctors.FirstOrDefault(x => x.Id == fee.DoctorId);
                DoctorName = doctor == null ? string.Empty : doctor.Name;
            }
        }
    }




    public class ReportPerDoctorModel
    {
        public string ChartJson { get; private set; }
        public IncomePerDoctorListModel List { get; set;  }

        public ReportPerDoctorModel()
        {
        }

        public ReportPerDoctorModel(List<ChartObjectIdAmount> chart)
        {
            var doctors = APP.Common.DB.Staffs.Get<Doctor>().ToList();
            ChartJson = new PieChartModel(chart, doctors).ToJson();
            List = new IncomePerDoctorListModel(chart, doctors);
        }
    }

    public class IncomePerDoctorListModel
    {
        public List<IncomePerDoctorModel> Doctors { get; set; }

        public IncomePerDoctorListModel()
        {
            Doctors = new List<IncomePerDoctorModel>();
        }

        public IncomePerDoctorListModel(List<ChartObjectIdAmount> chart)
        {   
        }

        public IncomePerDoctorListModel(List<ChartObjectIdAmount> chart, List<Doctor> doctors) : this()
        {
            Doctors.AddRange(chart.Select(x => new IncomePerDoctorModel(x, doctors)).ToList());
        }
    }

    public class IncomePerDoctorModel
    {
        public string DoctorName { get; set; }
        public string Amount { get; set; }
        public string CalendarColor { get; set; }

        public IncomePerDoctorModel(ChartObjectIdAmount item, List<Doctor> doctors)
        {
            var doctor = doctors.FirstOrDefault(x => x.Id == item.Label);
            DoctorName = doctor == null ? string.Empty : doctor.Name;
            CalendarColor = doctor == null ? "#fff" : doctor.CalendarColor;
            Amount = item.Value.To2Decimals();
        }
    }




    public class ReportByCategoryModel
    {
        public string ChartJson { get; private set; }
        public IncomePerCategoryListModel List { get; set; }

        public ReportByCategoryModel()
        {
        }

        public ReportByCategoryModel(List<ChartStringAmount> chart)
        {
            List = new IncomePerCategoryListModel(chart);
            ChartJson = new PieChartModel(List.Categories).ToJson();
        }
    }

    public class IncomePerCategoryListModel
    {
        public List<IncomePerCategoryModel> Categories { get; set; }

        public IncomePerCategoryListModel()
        {
            Categories = new List<IncomePerCategoryModel>();
        }

        public IncomePerCategoryListModel(List<ChartStringAmount> chart) : this()
        {
            var i = 0;
            Categories.AddRange(chart.Select(x => new IncomePerCategoryModel(x, i++)).ToList());
        }
    }

    public class IncomePerCategoryModel
    {
        public string Category { get; set; }
        public decimal Amount { get; set; }
        public string Color { get; set; }

        public IncomePerCategoryModel(ChartStringAmount item, int i)
        {
            Category = item.Label;
            Amount = item.Value;
            Color = i < _count ? _colors[i] : _colors[i - _count];
        }

        static string[] _colors = new string[] { 
            "#8147cb", "#cb47ae", "#ffa800", "#00b0e8","#b9cb47",
            "#479bcb", "#47cb9b", "#cb9647", "#cb4f47", "#47cbcb"
        };

        static readonly int _count = _colors.Count();
    }




    public class ReportAllExpensesModel
    {
        public string ChartJson { get; private set; }

        public ExpensesListModel List { get; set; }

        public ReportAllExpensesModel()
        {
            List = new ExpensesListModel();
        }

        public ReportAllExpensesModel(List<ChartDateAmount> chart, ExpensesListModel list, DateTime start, DateTime end)
            : this()
        {
            ChartJson = new BarChartModel(chart, start, end).ToJson();
            List = list;
        }
    }

    public class ExpensesListModel : ListModel
    {
        public List<ExpenseModel> Expenses { get; set; }

        public ExpensesListModel()
        {
            Expenses = new List<ExpenseModel>();
        }
    }

    public class ExpenseModel
    {
        public string Id { get; set; }
        public string Category { get; set; }
        public string Mode { get; set; }
        public string Date { get; set; }
        public string Amount { get; set; }

        public ExpenseModel()
        {
        }

        public ExpenseModel(Expense expense)
        {
            var timezone = APP.Common.Practice.Timezone;

            Id = expense.Id;
            Category = expense.Category;
            Mode = expense.Mode;
            Date = expense.Date.InTimezone(timezone).ToString(DateTimeFormats._8Mar87);
            Amount = expense.Amount.To2Decimals();
        }
    }




    public class ChartModel
    {
        internal string ToJson()
        {
            return new JavaScriptSerializer().Serialize(this);
        }
    }




    public class BarChartModel : ChartModel
    {
        public List<string> Labels { get; set; }
        public List<decimal> Data { get; set; }

        public BarChartModel()
        {
            Labels = new List<string>();
            Data = new List<decimal>();
        }

        public BarChartModel(List<ChartDateAmount> chart, DateTime start, DateTime end) : this()
        {
            var timezone = APP.Common.Practice.Timezone;

            while (start <= end)
            {
                Labels.Add(start.InTimezone(timezone).ToString(DateTimeFormats._8Mar));

                var record = chart.FirstOrDefault(x => x.Date == start);
                if (record == null)
                    Data.Add(0);
                else
                    Data.Add(record.Value);

                start = start.AddDays(1);
            }
        }

        public BarChartModel(List<ChartObjectIdAmount> chart) : this()
        {
            Labels.AddRange(chart.Select(x => x.Label).ToList());
            Data.AddRange(chart.Select(x => x.Value).ToList());
        }
    }




    public class PieChartModel : ChartModel
    {
        public List<PieSliceModel> Slices { get; set; }

        public PieChartModel()
        {
            Slices = new List<PieSliceModel>();
        }

        public PieChartModel(List<ChartObjectIdAmount> chart, List<Doctor> doctors) : this()
        {
            Slices.AddRange(chart.Select(x => new PieSliceModel(x, doctors)).ToList());
        }

        public PieChartModel(List<IncomePerCategoryModel> list) : this()
        {
            Slices.AddRange(list.Select(x => new PieSliceModel(x)).ToList());
        }
    }

    public class PieSliceModel
    {
        public decimal value { get; set; }
        public string color { get; set; }
        public string label { get; set; }

        public PieSliceModel()
        {
        }

        public PieSliceModel(ChartObjectIdAmount chart, List<Doctor> doctors) : this()
        {
            var doctor = doctors.FirstOrDefault(x => x.Id == chart.Label);
            if (doctor == null)
                return;

            value = chart.Value;
            color = doctor.CalendarColor;
            label = string.Format("{0}{1}", doctor.Name, 
                                            (string.IsNullOrWhiteSpace(doctor.Title) ? 
                                            string.Empty : 
                                            string.Format(" ({0})", doctor.Title)));
        }

        public PieSliceModel(IncomePerCategoryModel category) : this()
        {
            value = category.Amount;
            color = category.Color;
            label = category.Category;
        }
    }
}