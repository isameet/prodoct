﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProdoctWebApp.Models
{
    public class LinkResponseModel
    {
        public string Heading { get; set; }
        public string Message { get; set; }
    }

    public class SetPasswordModel
    {
        [Required(ErrorMessage = "New Password required")]
        [StringLength(30, MinimumLength = 6, ErrorMessage = "6 < Password Length < 30")]
        public string NewPassword { get; set; }

        [Required(ErrorMessage = "Please confirm your new password")]
        [Compare("NewPassword", ErrorMessage = "New Password entries do not match")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }

        public string Email { get; set; }
    }
}