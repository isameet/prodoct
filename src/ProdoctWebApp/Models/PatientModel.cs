﻿using ProdoctCommon.Data;
using ProdoctCommon.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProdoctWebApp.Models
{
    public class PatientModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Locality { get; set; }
        public string Mobile { get; set; }

        public PatientModel()
        {

        }

        public PatientModel(Patient patient)
        {
            Id = patient.Id;
            Name = patient.Name;
            Locality = patient.Locality;
            Mobile = patient.Mobile;
        }
    }

    public class PatientsListModel : ListModel
    {
        public List<PatientModel> Patients { get; set; }

        public PatientsListModel()
        {
            Patients = new List<PatientModel>();
        }
    }

    public class PatientAddModel
    {
        [Required(ErrorMessage = "Name required")]
        public string Name { get; set; }
        public string Locality { get; set; }

        [RegularExpression(RegexFormats.PhoneNumberE123, ErrorMessage = "Mobile # format: +[Country Code][Number]")]
        public string Mobile { get; set; }
        
        [RegularExpression(RegexFormats.EmailCaseInsensitive, ErrorMessage = "Invalid Email Id")]
        public string Email { get; set; }

        public PatientAddModel()
        {
            Mobile = "+91";
        }

        internal Patient ToPatient()
        {
            var patient = new Patient
            {
                Id = ObjectIdHelper.GenerateNewIdString(),
                Mobile = Mobile,
                Email = Email,
                Locality = Locality,
                CreatedOn = DateTime.Now,
                CreatorId = APP.Common.UserId,
                PracticeId = APP.Common.PracticeId
            };

            patient.SetName(Name);

            return patient;
        }
    }

    public class PatientEditModel
    {
        public string Id { get; set; }

        [Required(ErrorMessage = "Name required")]
        public string Name { get; set; }
        public string Locality { get; set; }

        [RegularExpression(RegexFormats.PhoneNumberE123, ErrorMessage = "Mobile # format: +[Country Code][Number]")]
        public string Mobile { get; set; }

        [RegularExpression(RegexFormats.EmailCaseInsensitive, ErrorMessage = "Invalid Email Id")]
        public string Email { get; set; }

        public PatientEditModel()
        {

        }

        public PatientEditModel(Patient patient)
        {
            Id = patient.Id;
            Name = patient.Name;
            Email = patient.Email;
            Mobile = patient.Mobile;
            Locality = patient.Locality;
        }

        internal void ToPatient(Patient patient)
        {
            patient.SetName(Name);
            patient.Mobile = Mobile;
            patient.Email = Email;
            patient.Locality = Locality;
        }
    }
}