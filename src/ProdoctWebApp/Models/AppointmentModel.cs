﻿using ProdoctCommon.Data;
using ProdoctCommon.Extensions;
using ProdoctCommon.Helpers;
using ProdoctWebApp.Helpers;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace ProdoctWebApp.Models
{
    public class AppointmentIndexModel
    {
        public AddAppointmentModel AddModel { get; set; }

        public AppointmentIndexModel()
        {
            AddModel = new AddAppointmentModel();
        }
    }

    public class AddAppointmentModel
    {
        [Required(ErrorMessage="Date is required")]
        public string AppointmentDate { get; set; }

        public string PatientId { get; set; }
        
        [Required(ErrorMessage="Patient's name is required")]
        public string PatientName { get; set; }

        [RegularExpression(RegexFormats.PhoneNumberE123, ErrorMessage="Mobile # format: +[Country Code][Number]")]
        public string PatientMobile { get; set; }
        
        public string PatientLocality { get; set; }

        [Required(ErrorMessage="Please select a Doctor")]
        public string DoctorId { get; set; }
        public List<SelectListItem> AllDoctors { get; set; }
        public string AppointmentNotes { get; set; }

        public AddAppointmentModel() 
        {
            PatientMobile = "+91";
            AllDoctors = new List<SelectListItem>();
            AllDoctors = SelectListHelper.GetDoctors();
        }

        internal Appointment ToAppointment()
        {
            var appointment = new Appointment 
            {
                Id = ObjectIdHelper.GenerateNewIdString(),
                Date = DateTimeHelper.FromDateTimePicker(AppointmentDate),
                DoctorId = DoctorId,
                PatientId = PatientId,
                PracticeId = APP.Common.PracticeId,
                CreatorId = APP.Common.UserId,
                Notes = AppointmentNotes
            };

            return appointment;
        }

        internal Patient ToPatient()
        {
            var patient = new Patient
            {
                Id = ObjectIdHelper.GenerateNewIdString(),
                Mobile = PatientMobile,
                Locality = PatientLocality,
                PracticeId = APP.Common.PracticeId,
                CreatorId = APP.Common.UserId,
                LastAppointment = new AppointmentSummary
                {
                    Date = DateTimeHelper.FromDateTimePicker(AppointmentDate),
                    DoctorId = DoctorId
                }
            };

            patient.SetName(PatientName);

            return patient;
        }
    }

    public class EditAppointmentModel
    {
        public string AppointmentId { get; set; }

        [Required]
        public string AppointmentDate { get; set; }

        public string PatientId { get; set; }

        public string PatientName { get; set; }

        public string PatientMobile { get; set; }

        public string PatientLocality { get; set; }

        [Required]
        public string DoctorId { get; set; }
        public List<SelectListItem> AllDoctors { get; set; }

        public string AppointmentNotes { get; set; }

        public bool IsCanceled { get; set; }

        public EditAppointmentModel()
        {
            AllDoctors = new List<SelectListItem>();
        }

        public EditAppointmentModel(Appointment appointment) : this()
        {
            AppointmentId = appointment.Id;
            AppointmentDate = appointment.Date.InTimezone(APP.Common.Practice.Timezone).ToString(DateTimeFormats.DateTimePicker);
            PatientId = appointment.PatientId;
            DoctorId = appointment.DoctorId;
            AppointmentNotes = appointment.Notes;
            IsCanceled = appointment.IsCanceled();

            AllDoctors = SelectListHelper.GetDoctors();

            var patient = APP.Common.DB.Patients.GetById(appointment.PatientId);
            if(patient != null)
            {
                PatientName = patient.Name;
                PatientMobile = patient.Mobile;
                PatientLocality = patient.Locality;
            }
        }

        public void UpdateAppointment(Appointment appointment)
        {
            appointment.Date = DateTimeHelper.FromDateTimePicker(AppointmentDate);
            appointment.DoctorId = DoctorId;
            appointment.Notes = AppointmentNotes;         
        }
    }
}