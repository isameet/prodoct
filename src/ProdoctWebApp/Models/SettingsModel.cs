﻿using ProdoctCommon.Data;
using ProdoctCommon.Extensions;
using ProdoctCommon.Helpers;
using ProdoctWebApp.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace ProdoctWebApp.Models
{
    public class MyInfoModel
    {
        [Required(ErrorMessage = "Name required")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Email required")]
        [RegularExpression(RegexFormats.EmailCaseInsensitive, ErrorMessage = "Invalid Email Id")]
        public string Email { get; set; }

        [RegularExpression(RegexFormats.PhoneNumberE123, ErrorMessage = "Mobile # format: +[Country Code][Number]")]
        public string Mobile { get; set; }

        public MyInfoModel()
        {
        }

        public MyInfoModel(User user)
        {
            Name = user.Name;
            Email = user.Email;
            Mobile = user.Mobile;
        }
    }

    public class ChangePasswordModel
    {
        [Required(ErrorMessage = "Current Password required")]
        public string CurrentPassword { get; set; }

        [Required(ErrorMessage = "New Password required")]
        [StringLength(30, MinimumLength = 6, ErrorMessage = "6 < Password Length < 30")]
        public string NewPassword { get; set; }

        [Required(ErrorMessage = "Please confirm your new password")]
        [System.ComponentModel.DataAnnotations.Compare("NewPassword", ErrorMessage = "New Password entries do not match")]
        public string ConfirmPassword { get; set; }

        public ChangePasswordModel()
        {

        }
    }

    public class PracticeSettingsModel
    {
        [Required(ErrorMessage = "Practice Name required")]
        public string Name { get; set; }

        [RegularExpression(RegexFormats.EmailCaseInsensitive, ErrorMessage = "Invalid Email Id")]
        public string Email { get; set; }

        public string Website { get; set; }

        public string ContactNumber { get; set; }

        [Required(ErrorMessage = "Timezone required")]
        public string Timezone { get; set; }
        public List<SelectListItem> AllTimezones { get; set; }
        
        public PracticeSettingsModel()
        {
            AllTimezones = new List<SelectListItem>();
        }
        public PracticeSettingsModel(Practice practice)
        {
            Name = practice.Name;
            Email = practice.Email;
            Website = practice.Website;
            ContactNumber = practice.ContactNumber;
            Timezone = practice.Timezone;
            AllTimezones = SelectListHelper.GetTimezones();
        }

        internal void ToPractice(Practice practice)
        {
            practice.Name = Name;
            practice.Email = Email;
            practice.Website = Website;
            practice.ContactNumber = ContactNumber;
            practice.Timezone = Timezone;
        }
    }

    public class CalendarSettingsModel
    {
        public bool ShowCanceledAppointments { get; set; }

        public CalendarSettingsModel()
        {

        }

        public CalendarSettingsModel(CalendarSettings settings)
        {
            ShowCanceledAppointments = settings.ShowCanceledAppointments;
        }

        internal CalendarSettings ToCalendarSettings()
        {
            return new CalendarSettings
            {
                ShowCanceledAppointments = ShowCanceledAppointments
            };
        }
    }

    public class StaffListModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public StaffRole Role { get; set; }
        public string Privileges { get; set; }
        public bool AllowLogin { get; set; }
        public bool HasAccepted { get; set; }
        public string LastLogin { get; set; }
        public StaffListModel()
        {
        }

        public StaffListModel(Staff staff)
        {
            Id = staff.Id;
            Name = staff.Name;
            Title = staff.Title;
            Role = staff.Role;
            Privileges = staff.Privileges.IsAdmin ? "Admin" : "Custom";
            AllowLogin = staff.AllowLogin;
            HasAccepted = staff.HasAccepted;
            LastLogin = staff.GetLastLoginString(APP.Common.Practice.Timezone);
        }
    }

    public class StaffAddModel
    {
        [Required(ErrorMessage = "Name required")]
        public string Name { get; set; }

        public string Title { get; set; }

        [Required(ErrorMessage = "Email required")]
        [RegularExpression(RegexFormats.EmailCaseInsensitive, ErrorMessage = "Invalid Email Id")]
        public string Email { get; set; }

        [RegularExpression(RegexFormats.PhoneNumberE123, ErrorMessage = "Mobile # format: +[Country Code][Number]")]
        public string Mobile { get; set; }

        public bool AllowLogin { get; set; }

        public StaffAddModel()
        {
        }

        internal StaffGetUserResult GetUser()
        {
            var result = new StaffGetUserResult();
            if (string.IsNullOrWhiteSpace(Email))
            {
                result.Message = "Email not specified";
                result.Error = true;
                return result;
            }

            var practiceId = APP.Common.PracticeId;
            var user = APP.Common.DB.Users.GetByEmail(Email);
            if (user == null)
            {
                user = new User(Name, Email, Mobile, practiceId);
                result.IsNew = true;
            }

            user.AddPractice(practiceId);
            
            result.User = user;
            return result;
        }

        internal Staff ToStaff()
        {
            var staff = new Staff
            {
                Id = ObjectIdHelper.GenerateNewIdString(),
                Name = Name,
                Title = Title,
                Mobile = Mobile,
                CreatorId = APP.Common.UserId,
                PracticeId = APP.Common.PracticeId,
                CreatedOn = DateTime.Now
            };

            staff.SetAllowLogin(AllowLogin, APP.Common);
            staff.SetEmail(Email, APP.Common);

            return staff;
        }
    }

    public class StaffEditModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public StaffEditModel()
        {

        }

        public StaffEditModel(Staff staff)
        {
            Id = staff.Id;
            Name = staff.Name;
        }
    }

    public class StaffBasicsModel
    {
        public string Id { get; set; }

        [Required(ErrorMessage = "Name required")]
        public string Name { get; set; }

        public string Title { get; set; }

        [Required(ErrorMessage = "Email required")]
        [RegularExpression(RegexFormats.EmailCaseInsensitive, ErrorMessage = "Invalid Email Id")]
        public string Email { get; set; }

        [RegularExpression(RegexFormats.PhoneNumberE123, ErrorMessage = "Mobile # format: +[Country Code][Number]")]
        public string Mobile { get; set; }

        public bool AllowLogin { get; set; }

        public bool IsSelf { get; set; }

        public StaffBasicsModel()
        {
        }
        public StaffBasicsModel(Staff staff, string loggedInUserId)
        {
            Id = staff.Id;
            Name = staff.Name;
            Title = staff.Title;
            Email = staff.Email;
            Mobile = staff.Mobile;
            AllowLogin = staff.AllowLogin;
            IsSelf = staff.UserId == loggedInUserId;
        }

        internal void ToStaff(Staff staff)
        {
            staff.Name = Name;
            staff.Title = Title;
            staff.Mobile = Mobile;
        }
    }

    public class StaffPrivilegesModel
    {
        public string Id { get; set; }
        public bool IsAdmin { get; set; }
        public bool AppointmentsRead { get; set; }
        public bool AppointmentsWrite { get; set; }
        public bool PatientsRead { get; set; }
        public bool PatientsWrite { get; set; }
        public bool InventoryRead { get; set; }
        public bool InventoryWrite { get; set; }
        public bool FinanceRead { get; set; }
        public bool FinanceWrite { get; set; }
        public bool IsSelf { get; set; }

        public StaffPrivilegesModel()
        {
        }

        public StaffPrivilegesModel(Staff staff, string loggedInUserId)
        {
            Id = staff.Id;
            IsAdmin = staff.Privileges.IsAdmin;
            AppointmentsRead = staff.Privileges.AppointmentsRead;
            AppointmentsWrite = staff.Privileges.AppointmentsWrite;
            PatientsRead = staff.Privileges.PatientsRead;
            PatientsWrite = staff.Privileges.PatientsWrite;
            InventoryRead = staff.Privileges.InventoryRead;
            InventoryWrite = staff.Privileges.InventoryWrite;
            FinanceRead = staff.Privileges.FinanceRead;
            FinanceWrite = staff.Privileges.FinanceWrite;
            IsSelf = staff.UserId == loggedInUserId;
        }

        internal Privileges ToPrivileges()
        {
            return new Privileges
            {
                IsAdmin = IsAdmin,
                AppointmentsRead = AppointmentsRead,
                AppointmentsWrite = AppointmentsWrite,
                PatientsRead = PatientsRead,
                PatientsWrite = PatientsWrite,
                InventoryRead = InventoryRead,
                InventoryWrite = InventoryWrite,
                FinanceRead = FinanceRead,
                FinanceWrite = FinanceWrite
            };
        }
    }

    public class DoctorAddModel : StaffAddModel
    {
        [Required(ErrorMessage = "Please pick a calendar color for the Doctor")]
        [RegularExpression(RegexFormats.HexColor, ErrorMessage = "Invalid calendar color")]
        public string CalendarColor { get; set; }

        public DoctorAddModel()
        {
            CalendarColor = "#479BCB";
        }

        internal Doctor ToDoctor()
        {
            var doctor = new Doctor
            {
                Id = ObjectIdHelper.GenerateNewIdString(),
                Name = Name,
                Title = Title,
                Mobile = Mobile,
                CreatorId = APP.Common.UserId,
                PracticeId = APP.Common.PracticeId,
                CalendarColor = CalendarColor,
                CreatedOn = DateTime.Now
            };

            doctor.SetEmail(Email, APP.Common);
            doctor.SetAllowLogin(AllowLogin, APP.Common);

            return doctor;
        }
    }

    public class DoctorEditModel : StaffEditModel
    {
        public DoctorEditModel()
        {

        }

        public DoctorEditModel(Doctor doctor)
        {
            Id = doctor.Id;
            Name = doctor.Name;
        }
    }

    public class DoctorBasicsModel : StaffBasicsModel
    {
        [Required(ErrorMessage = "Please pick a calendar color for the Doctor")]
        [RegularExpression(RegexFormats.HexColor, ErrorMessage = "Invalid calendar color")]
        public string CalendarColor { get; set; }

        public DoctorBasicsModel()
        {
        }

        public DoctorBasicsModel(Doctor doctor, string loggedInUserId)
        {
            Id = doctor.Id;
            Name = doctor.Name;
            Title = doctor.Title;
            Email = doctor.Email;
            Mobile = doctor.Mobile;
            AllowLogin = doctor.AllowLogin;
            CalendarColor = doctor.CalendarColor;
            IsSelf = doctor.UserId == loggedInUserId;
        }

        internal void ToDoctor(Doctor doctor)
        {
            doctor.Name = Name;
            doctor.Title = Title;
            doctor.Mobile = Mobile;
            doctor.CalendarColor = CalendarColor;
        }
    }

    public class DoctorPrivilegesModel : StaffPrivilegesModel
    {
        public DoctorPrivilegesModel()
        {
        }

        public DoctorPrivilegesModel(Doctor doctor)
        {
            Id = doctor.Id;
            IsAdmin = doctor.Privileges.IsAdmin;
            AppointmentsRead = doctor.Privileges.AppointmentsRead;
            AppointmentsWrite = doctor.Privileges.AppointmentsWrite;
            PatientsRead = doctor.Privileges.PatientsRead;
            PatientsWrite = doctor.Privileges.PatientsWrite;
            InventoryRead = doctor.Privileges.InventoryRead;
            InventoryWrite = doctor.Privileges.InventoryWrite;
            FinanceRead = doctor.Privileges.FinanceRead;
            FinanceWrite = doctor.Privileges.FinanceWrite;
        }
    }
}