﻿using ProdoctWebApp.MVC;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace ProdoctWebApp.Models
{
    [ModelBinder(typeof(CalendarRequestModelBinder))]
    public class CalendarRequestModel
    {
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string DoctorId { get; set; }
    }

    public class CalendarEventModel
    {
        //fullcalendar properties
        public string id { get; set; }
        public string  title { get; set; }
        public string start { get; set; }
        public string end { get; set; }
        public string color { get; set; }
        public string textColor { get; set; }

        //custom properties
        public string description { get; set; }
        public bool isCanceled { get; set; }
    }

    public class CalendarPatientModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }

    public class CalendarDoctorModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Color { get; set; }
    }

    public class CalendarStatsModel
    {
        public List<CalendarDoctorStatsModel> Doctors { get; set; }
    }

    public class CalendarDoctorStatsModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Color { get; set; }
        public int Appointments { get; set; }
    }
}