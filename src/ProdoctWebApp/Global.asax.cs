﻿using ProdoctCommon.Data;
using ProdoctCommon.Helpers;
using ProdoctCommon.Logging;
using ProdoctWebApp.MVC;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace ProdoctWebApp
{
    public class ProdoctMvc : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            RegisterModelBinders();
        }

        protected void Application_BeginRequest()
        {
            APP.Common.SetLogger(new NLogLogger(GetType()));
            APP.Common.DB = new ProdoctDatabase(new DatabaseContext());
        }

        private void RegisterModelBinders()
        {
            ModelBinders.Binders.Add(typeof(string), new TrimModelBinder());
        }
    }
}
