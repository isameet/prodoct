﻿using ProdoctWebApp.MVC;
using System.Web;
using System.Web.Mvc;

namespace ProdoctWebApp
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new BetterAuthorizeAttribute());
        }
    }
}
