﻿using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Optimization;

namespace ProdoctWebApp
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            var destinationJs = "~/bundles/js";
            var desintationCss = "~/bundles/css";

            Plugins(bundles, destinationJs, desintationCss);
            Appointments(bundles, destinationJs, desintationCss);
            Patients(bundles, destinationJs, desintationCss);
            Inventory(bundles, destinationJs, desintationCss);
            Finance(bundles, destinationJs, desintationCss);
            Settings(bundles, destinationJs, desintationCss);
            Login(bundles, destinationJs, desintationCss);
            ForgotPassword(bundles, destinationJs, desintationCss);
            SetPassword(bundles, destinationJs, desintationCss);
            LinkResponse(bundles, destinationJs, desintationCss);

            BundleTable.EnableOptimizations = true;
        }

        private static void Plugins(BundleCollection bundles, string destinationJs, string desintationCss)
        {
            var source = "~/Content/plugins/basic";
            bundles.Add(new ScriptBundle(string.Format("{0}/plugins", destinationJs)).Include(
                                         string.Format("{0}/jquery-{{version}}.js", source),
                                         string.Format("{0}/jquery.validate*", source),
                                         string.Format("{0}/jquery.unobtrusive-ajax*", source),
                                         string.Format("{0}/modernizr-*", source),
                                         string.Format("{0}/bootstrap.js", source),
                                         string.Format("{0}/respond.js", source)));

            bundles.Add(new ScriptBundle(string.Format("{0}/modernizr", destinationJs)).Include(
                                         string.Format("{0}/modernizr-*", source)));

            bundles.Add(new StyleBundle(string.Format("{0}/plugins", desintationCss)).Include(
                                        "~/Content/css/bootstrap.css",
                                        "~/Content/css/app.css"));
        }

        private static void Appointments(BundleCollection bundles, string destinationJs, string desintaionCss)
        {
            bundles.Add(new ScriptBundle(string.Format("{0}/appointments", destinationJs))
                   .Include("~/Content/plugins/calendar/moment.min.js",
                            "~/Content/plugins/calendar/fullcalendar.js",
                            "~/Content/plugins/datetimepicker/bootstrap-datetimepicker.js",
                            "~/Content/plugins/autocomplete/jquery.autocomplete.js",
                            "~/Content/js/common/nav.js",
                            "~/Content/js/common/collapsible.js",
                            "~/Content/js/common/modal.js",
                            "~/Content/js/common/datetimepicker.js",
                            "~/Content/js/common/flash.js",
                            "~/Content/js/common/form.js",
                            "~/Content/js/common/confirm.js",
                            "~/Content/js/common/extensions.js",
                            "~/Content/js/common/helpers.js",
                            "~/Content/js/common/ajax.js",
                            "~/Content/js/autcomplete/patients.js",
                            "~/Content/js/appointments/appointments.js",
                            "~/Content/js/appointments/calendar.js",
                            "~/Content/js/appointments/mediator.js",
                            "~/Content/js/appointments/view.js"));

            bundles.Add(new StyleBundle(string.Format("{0}/appointments", desintaionCss))
                   .Include("~/Content/plugins/calendar/fullcalendar.css",
                            "~/Content/plugins/datetimepicker/bootstrap-datetimepicker.css",
                            "~/Content/plugins/autocomplete/jquery.autocomplete.css",
                            "~/Content/css/appointments/appointments.css"));
        }

        private static void Patients(BundleCollection bundles, string destinationJs, string desintaionCss)
        {
            bundles.Add(new ScriptBundle(string.Format("{0}/patients", destinationJs))
                   .Include("~/Content/js/common/nav.js",
                            "~/Content/js/common/collapsible.js",
                            "~/Content/js/common/modal.js",
                            "~/Content/js/common/flash.js",
                            "~/Content/js/common/form.js",
                            "~/Content/js/common/confirm.js",
                            "~/Content/js/common/extensions.js",
                            "~/Content/js/common/helpers.js",
                            "~/Content/js/common/ajax.js",
                            "~/Content/js/patients/patients.js",
                            "~/Content/js/patients/mediator.js",
                            "~/Content/js/patients/view.js"));

            bundles.Add(new StyleBundle(string.Format("{0}/patients", desintaionCss))
                   .Include("~/Content/css/patients/patients.css"));
        }

        private static void Inventory(BundleCollection bundles, string destinationJs, string desintaionCss)
        {
            bundles.Add(new ScriptBundle(string.Format("{0}/inventory", destinationJs))
                   .Include("~/Content/plugins/datepicker/bootstrap-datepicker.js",
                            "~/Content/js/common/nav.js",
                            "~/Content/js/common/modal.js",
                            "~/Content/js/common/flash.js",
                            "~/Content/js/common/form.js",
                            "~/Content/js/common/confirm.js",
                            "~/Content/js/common/extensions.js",
                            "~/Content/js/common/helpers.js",
                            "~/Content/js/common/ajax.js",
                            "~/Content/js/common/datepicker.js",
                            "~/Content/js/inventory/inventory.js",
                            "~/Content/js/inventory/mediator.js",
                            "~/Content/js/inventory/view.js"));

            bundles.Add(new StyleBundle(string.Format("{0}/inventory", desintaionCss))
                   .Include("~/Content/plugins/datepicker/datepicker.css",
                            "~/Content/css/inventory/inventory.css"));
        }

        private static void Finance(BundleCollection bundles, string destinationJs, string desintaionCss)
        {
            bundles.Add(new ScriptBundle(string.Format("{0}/finance", destinationJs))
                   .Include("~/Content/plugins/datepicker/bootstrap-datepicker.js",
                            "~/Content/plugins/chartjs/Chart.js",
                            "~/Content/plugins/autocomplete/jquery.autocomplete.js",
                            "~/Content/plugins/history/jquery.history.js",
                            "~/Content/js/common/nav.js",
                            "~/Content/js/common/collapsible.js",
                            "~/Content/js/common/modal.js",
                            "~/Content/js/common/flash.js",
                            "~/Content/js/common/form.js",
                            "~/Content/js/common/confirm.js",
                            "~/Content/js/common/extensions.js",
                            "~/Content/js/common/helpers.js",
                            "~/Content/js/common/ajax.js",
                            "~/Content/js/common/datepicker.js",
                            "~/Content/js/common/daterangepicker.js",
                            "~/Content/js/autcomplete/patients.js",
                            "~/Content/js/finance/charts.js",
                            "~/Content/js/finance/finance.js",
                            "~/Content/js/finance/mediator.js",
                            "~/Content/js/finance/view.js"));

            bundles.Add(new StyleBundle(string.Format("{0}/finance", desintaionCss))
                   .Include("~/Content/plugins/datepicker/datepicker.css",
                            "~/Content/plugins/autocomplete/jquery.autocomplete.css",
                            "~/Content/css/finance/finance.css"));
        }

        private static void Settings(BundleCollection bundles, string destinationJs, string desintaionCss)
        {
            bundles.Add(new ScriptBundle(string.Format("{0}/settings", destinationJs))
                   .Include("~/Content/plugins/colorpicker/js/bootstrap-colorpicker.js",
                            "~/Content/js/common/nav.js",
                            "~/Content/js/common/flash.js",
                            "~/Content/js/common/form.js",
                            "~/Content/js/common/confirm.js",
                            "~/Content/js/common/extensions.js",
                            "~/Content/js/common/tabs.js",
                            "~/Content/js/common/ajax.js",
                            "~/Content/js/common/modal.js",
                            "~/Content/js/common/helpers.js",
                            "~/Content/js/settings/settings.js",
                            "~/Content/js/settings/mediator.js",
                            "~/Content/js/settings/view.js"));
    
            bundles.Add(new StyleBundle(string.Format("{0}/settings", desintaionCss))
                   .Include("~/Content/plugins/colorpicker/css/bootstrap-colorpicker.css",
                            "~/Content/css/settings/settings.css"));
        }

        private static void Login(BundleCollection bundles, string destinationJs, string desintaionCss)
        {
            bundles.Add(new ScriptBundle(string.Format("{0}/login", destinationJs))
                   .Include("~/Content/js/account/login/login.js",
                            "~/Content/js/account/login/view.js",
                            "~/Content/js/account/login/mediator.js"));

            bundles.Add(new StyleBundle(string.Format("{0}/login", desintaionCss))
                   .Include("~/Content/css/account/login.css"));
        }

        private static void ForgotPassword(BundleCollection bundles, string destinationJs, string desintaionCss)
        {
            bundles.Add(new ScriptBundle(string.Format("{0}/forgotpassword", destinationJs))
                   .Include("~/Content/js/common/flash.js",
                            "~/Content/js/common/form.js",
                            "~/Content/js/common/ajax.js",
                            "~/Content/js/account/forgotpassword/forgotpassword.js",
                            "~/Content/js/account/forgotpassword/view.js",
                            "~/Content/js/account/forgotpassword/mediator.js"));

            bundles.Add(new StyleBundle(string.Format("{0}/forgotpassword", desintaionCss))
                   .Include("~/Content/css/common/flash.css",
                            "~/Content/css/common/form.css",
                            "~/Content/css/account/forgotpassword.css"));
        }

        private static void SetPassword(BundleCollection bundles, string destinationJs, string desintaionCss)
        {
            bundles.Add(new ScriptBundle(string.Format("{0}/setpassword", destinationJs))
                   .Include("~/Content/js/links/setpassword/setpassword.js",
                            "~/Content/js/links/setpassword/mediator.js",
                            "~/Content/js/links/setpassword/view.js"));

            bundles.Add(new StyleBundle(string.Format("{0}/setpassword", desintaionCss))
                   .Include("~/Content/css/links/links.css",
                            "~/Content/css/links/setpassword.css"));
        }

        private static void LinkResponse(BundleCollection bundles, string destinationJs, string desintaionCss)
        {
            //bundles.Add(new ScriptBundle(string.Format("{0}/linkresponse", destinationJs))
            //       .Include("~/Content/js/links/..."));

            bundles.Add(new StyleBundle(string.Format("{0}/linkresponse", desintaionCss))
                   .Include("~/Content/css/links/links.css"));
        }
    }




    //following to be used if problem noticed in ordering of bundled files --------------------

    internal class AsIsBundleOrderer : IBundleOrderer
    {
        public virtual IEnumerable<BundleFile> OrderFiles(BundleContext context, IEnumerable<BundleFile> files)
        {
            return files;
        }
    }

    internal static class BundleExtensions
    {
        public static Bundle ForceOrdered(this Bundle sb)
        {
            sb.Orderer = new AsIsBundleOrderer();
            return sb;
        }
    }
}
