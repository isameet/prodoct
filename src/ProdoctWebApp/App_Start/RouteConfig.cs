﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ProdoctWebApp
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(RouteName.Login, "login", new { controller = "Account", action = "Login" });
            routes.MapRoute(RouteName.Logout, "logout", new { controller = "Account", action = "Logout" });
            routes.MapRoute(RouteName.ForgotPassword, "forgotpassword", new { controller = "Account", action = "ForgotPassword" });
            routes.MapRoute(RouteName.Appointments, "appointments", new { controller = "Appointments", action = "Index" });
            routes.MapRoute(RouteName.Patients, "patients", new { controller = "Patients", action = "Index" });
            routes.MapRoute(RouteName.Inventory, "inventory", new { controller = "Inventory", action = "Index" });
            routes.MapRoute(RouteName.Finance, "finance", new { controller = "Finance", action = "Index" });
            routes.MapRoute(RouteName.Settings, "settings", new { controller = "Settings", action = "Index" });
            routes.MapRoute(
                name: RouteName.Link,
                url: "l/{email}/{code}",
                defaults: new { controller = "Links", action = "Process", code = UrlParameter.Optional, email = UrlParameter.Optional }
            );
            routes.MapRoute(RouteName.NotFound, "not-found", new { controller = "Links", action = "NotFound" });
            routes.MapRoute(
                name: "IssueMedicine",
                url: "medicine/{med}/{app}",
                defaults: new { controller = "Inventory", action = "IssueMedicine", app = UrlParameter.Optional, med = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "AddFees",
                url: "fees/{app}",
                defaults: new { controller = "Income", action = "AddFees", app = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Appointments", action = "Index", id = UrlParameter.Optional }
            );
        }
    }

    public static class RouteName
    {
        public const string Login = "Login";
        public const string Logout = "Logout";
        public const string ForgotPassword = "ForgotPassword";
        public const string Appointments = "Appointments";
        public const string Patients = "Patients";
        public const string Inventory = "Inventory";
        public const string Finance = "Finance";
        public const string Settings = "Settings";
        public const string Link = "Link";
        public const string NotFound = "NotFound";
    }
}
