﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ProdoctWebApp.Startup))]
namespace ProdoctWebApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
