﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProdoctWebApp.Controllers
{
    public class FinanceController : ProdoctBaseController
    {
        public FinanceController()
        {
            Helpers.General.Sleep();
        }

        #region READ

        public ActionResult Index()
        {
            if (!CanView())
                return RedirectToAllowedRoute();

            return View();
        }

        #endregion




        private bool CanView()
        {
            return Privileges.FinanceRead;
        }

        private bool CanWrite()
        {
            return Privileges.FinanceWrite;
        }
    }
}