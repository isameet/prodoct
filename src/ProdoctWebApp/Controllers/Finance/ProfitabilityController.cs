﻿using ProdoctWebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProdoctWebApp.Controllers
{
    public class ProfitabilityController : ProdoctBaseController
    {
        public ProfitabilityController()
        {
            Helpers.General.Sleep();
        }

        #region READ

        public ActionResult Reports()
        {
            if (!CanView())
                return PartialViewModalAccessDenied();

            return PartialView(FinanceModel.GetViewName("Profitability/_Reports"));
        }

        #endregion




        private bool CanView()
        {
            return Privileges.FinanceRead;
        }

        private bool CanWrite()
        {
            return Privileges.FinanceWrite;
        }
    }
}