﻿using ProdoctCommon.Data;
using ProdoctCommon.Dto;
using ProdoctCommon.Extensions;
using ProdoctCommon.Helpers;
using ProdoctWebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProdoctWebApp.Controllers
{
    public class ExpensesController : ProdoctBaseController
    {
        public ExpensesController()
        {
            Helpers.General.Sleep();
        }

        #region READ

        public ActionResult Reports()
        {
            if (!CanView())
                return PartialViewModalAccessDenied();

            var model = new ReportsModel(Practice.Timezone);
            model.AllReports = new List<ReportsSelectItemModel>
            {
                new ReportsSelectItemModel { Text = "All Expenses", Get = "/Expenses/All", Selected = true },
                new ReportsSelectItemModel { Text = "Expenses by Category", Get = "/Expenses/ByCategory" }
            };

            return PartialView(FinanceModel.GetViewName("Expenses/_Reports"), model);
        }




        public ActionResult All(ReportsModel settings)
        {
            DateTime start, end;
            var message = string.Empty;
            if (!CheckPreconditionsForReport(settings, out start, out end, out message))
                return PartialViewError(message);

            var model = new ReportAllExpensesModel(GetAllExpensesChart(start, end),
                                                   GetAllExpensesList(start, end),
                                                   start,
                                                   end);

            return PartialView(FinanceModel.GetViewName("Expenses/_ReportAllExpenses"), model);
        }

        private List<ChartDateAmount> GetAllExpensesChart(DateTime start, DateTime end)
        {
            return DB.Expenses.GetAllExpensesChart(start, end);
        }

        private ExpensesListModel GetAllExpensesList(DateTime start, DateTime end)
        {
            var settings = new ListSettingsReport { Start = start, End = end };
            return GetAllExpensesList(settings);
        }

        private ExpensesListModel GetAllExpensesList(ListSettingsReport settings)
        {
            var count = 0;
            var expenses = DB.Expenses.GetList(settings, out count).Select(x => new ExpenseModel(x)).ToList();

            return new ExpensesListModel
            {
                Expenses = expenses,
                MoreExist = !(expenses.Count < settings.Take),
                Skip = settings.Skip + expenses.Count
            };
        }




        public ActionResult List(ListSettingsReport settings)
        {
            var model = GetAllExpensesList(settings);
            var html = RenderPartialViewToString(FinanceModel.GetViewName("Expenses/_List"), model);
            return Json(new
            {
                HasError = false,
                Message = string.Empty,
                Html = html
            });
        }




        public ActionResult ByCategory(ReportsModel settings)
        {
            DateTime start, end;
            var message = string.Empty;
            if (!CheckPreconditionsForReport(settings, out start, out end, out message))
                return PartialViewError(message);

            var model = new ReportByCategoryModel(DB.Expenses.GetByCategoryChart(start, end));
            return PartialView(FinanceModel.GetViewName("Expenses/_ReportByCategory"), model);
        }




        private bool CheckPreconditionsForReport(ReportsModel model, out DateTime start, out DateTime end, out string message)
        {
            start = DateTimeHelper.FromDateRangePicker(model.Start).ToUtc();
            end = DateTimeHelper.FromDateRangePicker(model.End).ToUtc();
            message = string.Empty;

            if (start > end)
            {
                message = "Invalid date range";
                return false;
            }

            if (end.Subtract(start).TotalDays > 31)
            {
                message = "Maximum date range - 31 days";
                return false;
            }

            return true;
        }


        #endregion

        #region CREATE

        public ActionResult Add()
        {
            if (!CanWrite())
                return PartialViewModalAccessDenied();

            return PartialView(FinanceModel.GetViewName("Expenses/_Add"), new ExpenseAddModel());
        }

        [HttpPost]
        public ActionResult Add(ExpenseAddModel model)
        {
            if (!CanWrite())
                return JsonResultAccessDenied();

            if (!ModelState.IsValid)
                return JsonResultInvalidData();

            var expense = model.ToExpense();
            var result = DB.Expenses.Add(expense).ToStandardResult("Expense added");
            return JsonStandardResult(result);
        }

        #endregion

        #region UPDATE

        public ActionResult Edit(string id)
        {
            if (!CanWrite())
                return PartialViewModalAccessDenied();

            var expense = DB.Expenses.GetById(id);
            if (expense == null)
                return PartialViewModalNotFound();

            var model = new ExpenseEditModel(expense);
            return PartialView(FinanceModel.GetViewName("Expenses/_Edit"), model);
        }

        [HttpPost]
        public ActionResult Edit(ExpenseEditModel model)
        {
            if (!CanWrite())
                return JsonResultAccessDenied();

            if (!ModelState.IsValid)
                return JsonResultInvalidData();

            var message = string.Empty;
            if (!CheckExpenseEditConditions(model, out message))
                return JsonResultInvalidData(message);

            var expense = DB.Expenses.GetById(model.Id);
            if (expense == null)
                return JsonResultInvalidData("Record not found!");

            model.ToExpense(expense);
            var result = DB.Expenses.Update(expense).ToStandardResult("Updated");
            return JsonStandardResult(result);
        }

        private bool CheckExpenseEditConditions(ExpenseEditModel model, out string message)
        {
            message = string.Empty;
            return true;
        }

        #endregion

        #region DELETE

        public JsonResult Delete(string id)
        {
            if (!CanWrite())
                return JsonResultAccessDenied();

            var expense = DB.Expenses.GetById(id);
            if (expense == null)
                return JsonResultInvalidData("Record not found!");

            var message = string.Empty;
            if (!CheckPreconditionsForDeletingExpense(expense, out message))
                return JsonResultInvalidData(message);

            var result = DB.Expenses.Remove(expense).ToStandardResult("Deleted");
            return JsonStandardResult(result);
        }

        private bool CheckPreconditionsForDeletingExpense(Expense expense, out string message)
        {
            message = string.Empty;
            return true;
        }

        #endregion

        private bool CanView()
        {
            return Privileges.FinanceRead;
        }

        private bool CanWrite()
        {
            return Privileges.FinanceWrite;
        }
    }
}