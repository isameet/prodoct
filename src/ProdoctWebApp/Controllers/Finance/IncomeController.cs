﻿using ProdoctCommon.Data;
using ProdoctCommon.Dto;
using ProdoctCommon.Extensions;
using ProdoctCommon.Helpers;
using ProdoctWebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace ProdoctWebApp.Controllers
{
    public class IncomeController : ProdoctBaseController
    {
        public IncomeController()
        {
            Helpers.General.Sleep();
        }

        #region READ

        public ActionResult Reports()
        {
            if (!CanView())
                return PartialViewModalAccessDenied();

            var model = new ReportsModel(Practice.Timezone);
            model.AllReports = new List<ReportsSelectItemModel>
            {
                new ReportsSelectItemModel { Text = "All Income", Get = "/Income/All", Selected = true },
                new ReportsSelectItemModel { Text = "Income per Doctor", Get = "/Income/PerDoctor" },
                new ReportsSelectItemModel { Text = "Income by Category", Get = "/Income/ByCategory" }
            };

            return PartialView(FinanceModel.GetViewName("Income/_Reports"), model);
        }




        public ActionResult All(ReportsModel settings)
        {
            DateTime start, end;
            var message = string.Empty;
            if (!CheckPreconditionsForReport(settings, out start, out end, out message))
                return PartialViewError(message);

            var model = new ReportAllIncomeModel(GetAllIncomeChart(start, end), 
                                                 GetAllIncomeList(start, end), 
                                                 start, 
                                                 end);
            
            return PartialView(FinanceModel.GetViewName("Income/_ReportAllIncome"), model);
        }

        private List<ChartDateAmount> GetAllIncomeChart(DateTime start, DateTime end)
        {
            return DB.Incomes.GetAllIncomeChart(start, end);
        }

        private IncomeListModel GetAllIncomeList(DateTime start, DateTime end)
        {
            var settings = new ListSettingsReport { Start = start, End = end };
            return GetAllIncomeList(settings);
        }

        private IncomeListModel GetAllIncomeList(ListSettingsReport settings)
        {
            var count = 0;
            var incomes = DB.Incomes.GetList(settings, out count);
            var doctors = new List<Doctor>();
            var patients = new List<Patient>();
            if (incomes.Fees != null && incomes.Fees.Count > 0)
            {
                var doctorIds = incomes.Fees.Select(x => x.DoctorId).ToList();
                doctors = DB.Staffs.Get<Doctor>().Where(x => doctorIds.Contains(x.Id)).ToList();

                var patientIds = incomes.Fees.Select(x => x.PatientId).ToList();
                patients = DB.Patients.Get(new string[] { "Name" }).Where(x => patientIds.Contains(x.Id)).ToList();
            }
            var list = incomes.Incomes.Select(x => new IncomeModel(x, incomes.Fees, doctors, patients)).ToList();

            return new IncomeListModel
            {
                Incomes = list,
                MoreExist = !(list.Count < settings.Take),
                Skip = settings.Skip + list.Count
            };
        }




        public ActionResult List(ListSettingsReport settings)
        {
            var model = GetAllIncomeList(settings);
            var html = RenderPartialViewToString(FinanceModel.GetViewName("Income/_List"), model);
            return Json(new
            {
                HasError = false,
                Message = string.Empty,
                Html = html
            });
        }




        public ActionResult PerDoctor(ReportsModel settings)
        {
            DateTime start, end;
            var message = string.Empty;
            if (!CheckPreconditionsForReport(settings, out start, out end, out message))
                return PartialViewError(message);

            var model = new ReportPerDoctorModel(DB.Incomes.GetPerDoctorChart(start, end));
            return PartialView(FinanceModel.GetViewName("Income/_ReportPerDoctor"), model);
        }




        public ActionResult ByCategory(ReportsModel settings)
        {
            DateTime start, end;
            var message = string.Empty;
            if (!CheckPreconditionsForReport(settings, out start, out end, out message))
                return PartialViewError(message);

            var model = new ReportByCategoryModel(DB.Incomes.GetByCategoryChart(start, end));
            return PartialView(FinanceModel.GetViewName("Income/_ReportByCategory"), model);
        }




        private bool CheckPreconditionsForReport(ReportsModel model, out DateTime start, out DateTime end, out string message)
        {
            start = DateTimeHelper.FromDateRangePicker(model.Start).ToUtc();
            end = DateTimeHelper.FromDateRangePicker(model.End).ToUtc();
            message = string.Empty;

            if (start > end)
            {
                message = "Invalid date range";
                return false;
            }

            if(end.Subtract(start).TotalDays > 31)
            {
                message = "Maximum date range - 31 days";
                return false;
            }

            return true;
        }

        #endregion

        #region CREATE

        public ActionResult Add()
        {
            if (!CanWrite())
                return PartialViewModalAccessDenied();

            return PartialView(FinanceModel.GetViewName("Income/_Add"), new IncomeAddModel());
        }

        [HttpPost]
        public ActionResult Add(IncomeAddModel model)
        {
            if (!CanWrite())
                return JsonResultAccessDenied();

            if(string.IsNullOrWhiteSpace(model.Category))
                return JsonResultInvalidData();

            if (model.Category.ToLower() == "fees")
                return AddFees(model);

            return AddIncome(model);
        }

        private ActionResult AddIncome(IncomeAddModel model)
        {
            ModelState.Remove("PatientNameOrMobile");
            ModelState.Remove("PatientId");
            ModelState.Remove("DoctorId");

            if (!ModelState.IsValid)
                return JsonResultInvalidData();

            var income = model.ToIncome();
            var result = DB.Incomes.Add(income).ToStandardResult("Income added");
            return JsonStandardResult(result);
        }

        private ActionResult AddFees(IncomeAddModel model)
        {
            ModelState.Remove("PatientNameOrMobile");

            if (!ModelState.IsValid)
                return JsonResultInvalidData();

            if (!ModelState.IsValid)
                return JsonResultInvalidData();

            var fees = model.ToFees();
            var result = DB.Incomes.Add(fees).ToStandardResult("Fees added");
            return JsonStandardResult(result);
        }

        #endregion

        #region UPDATE

        public ActionResult Edit(string id)
        {
            if (!CanWrite())
                return PartialViewModalAccessDenied();

            var income = DB.Incomes.GetById(id);
            if (income == null)
                return PartialViewModalNotFound();

            var model = new IncomeEditModel(income);
            return PartialView(FinanceModel.GetViewName("Income/_Edit"), model);
        }

        [HttpPost]
        public ActionResult Edit(IncomeEditModel model)
        {
            if (!CanWrite())
                return JsonResultAccessDenied();

            if (!ModelState.IsValid)
                return JsonResultInvalidData();

            var message = string.Empty;
            if (!CheckIncomeEditConditions(model, out message))
                return JsonResultInvalidData(message);

            var income = DB.Incomes.GetById(model.Id);
            if (income == null)
                return JsonResultInvalidData("Record not found!");

            model.ToIncome(income);
            var result = DB.Incomes.Update(income).ToStandardResult("Updated");
            return JsonStandardResult(result);
        }

        private bool CheckIncomeEditConditions(IncomeEditModel model, out string message)
        {
            message = string.Empty;
            return true;
        }




        public ActionResult EditFees(string id)
        {
            if (!CanWrite())
                return PartialViewModalAccessDenied();

            var fees = DB.Incomes.GetById<Fees>(id);
            if (fees == null)
                return PartialViewModalNotFound();

            var model = new FeesEditModel(fees);
            return PartialView(FinanceModel.GetViewName("Income/_EditFees"), model);
        }

        [HttpPost]
        public ActionResult EditFees(FeesEditModel model)
        {
            if (!CanWrite())
                return JsonResultAccessDenied();

            ModelState.Remove("Category");
            if (!ModelState.IsValid)
                return JsonResultInvalidData();

            var message = string.Empty;
            if (!CheckIncomeEditConditions(model, out message))
                return JsonResultInvalidData(message);

            var fees = DB.Incomes.GetById<Fees>(model.Id);
            if (fees == null)
                return JsonResultInvalidData("Record not found!");

            model.ToFees(fees);
            var result = DB.Incomes.Update(fees).ToStandardResult("Updated");
            return JsonStandardResult(result);
        }

        #endregion

        #region DELETE

        public JsonResult Delete(string id)
        {
            if (!CanWrite())
                return JsonResultAccessDenied();

            var income = DB.Incomes.GetById(id);
            if (income == null)
                return JsonResultInvalidData("Record not found!");

            var message = string.Empty;
            if (!CheckPreconditionsForDeletingIncome(income, out message))
                return JsonResultInvalidData(message);

            var result = DB.Incomes.Remove(income).ToStandardResult("Deleted");
            return JsonStandardResult(result);
        }

        private bool CheckPreconditionsForDeletingIncome(Income income, out string message)
        {
            message = string.Empty;
            return true;
        }

        #endregion




        #region ADD FEES

        public ActionResult AddFees(string app)
        {
            if (!CanWriteAppointments)
                return ViewAccessDenied();

            var appointment = DB.Appointments.GetById(app);
            if (appointment == null)
                return ViewNotFound();

            return View(FinanceModel.GetViewName("Income/AddFees"), new FeesAddModel(appointment));
        }

        [HttpPost]
        public ActionResult AddFees(FeesAddModel model)
        {
            if (!CanWriteAppointments)
                return JsonResultAccessDenied();

            ModelState.Remove("PatientNameOrMobile");
            if (!ModelState.IsValid)
                return JsonResultInvalidData();

            var appointment = DB.Appointments.GetById(model.AppointmentId);
            if (appointment == null)
                return JsonResultInvalidData("Appointment not found!");

            var fees = model.ToFees(appointment);
            var result = DB.Incomes.Add(fees).ToStandardResult("Fees added");
            return JsonStandardResult(result);
        }

        #endregion




        private bool CanView()
        {
            return Privileges.FinanceRead;
        }

        private bool CanWrite()
        {
            return Privileges.FinanceWrite;
        }
    }
}