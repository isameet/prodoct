﻿using ProdoctCommon.Data;
using ProdoctCommon.Extensions;
using ProdoctCommon.Helpers;
using ProdoctWebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace ProdoctWebApp.Controllers
{
    public class AppointmentsController : ProdoctBaseController
    {
        public AppointmentsController()
        {
            Helpers.General.Sleep();
        }

        #region READ

        public ActionResult Index()
        {
            if (!Privileges.AppointmentsRead)
                return RedirectToAllowedRoute();

            var model = new AppointmentIndexModel();
            return View(model);
        }

        public JsonResult GetCalendar(CalendarRequestModel model)
        {
            var staff = LoggedInStaff;
            if (staff == null)
                return Json(new
                {
                    Events = string.Empty,
                    Stats = string.Empty
                });

            var appointments = DB.Appointments.Get(model.Start, model.End, staff.CalendarSettings).ToList();
            var patients = new List<CalendarPatientModel>();
            var doctors = new List<CalendarDoctorModel>();
            GetPatientsAndDoctors(appointments, out patients, out doctors);

            var stats = GetCalendarStats(appointments, doctors);

            var appointmentsQuery = string.IsNullOrWhiteSpace(model.DoctorId) ?
                                    appointments : appointments.Where(x => x.DoctorId == model.DoctorId);

            var events = appointmentsQuery.Select(x => GetCalendarEventModel(x, patients, doctors)).ToList();

            return Json(new
            {
                Events = events,
                Stats = stats
            });
        }

        private CalendarStatsModel GetCalendarStats(List<Appointment> appointments, List<CalendarDoctorModel> doctors)
        {
            var doctorStats = GetCalendarDoctorStats(appointments, doctors);

            return new CalendarStatsModel
            {
                Doctors = doctorStats
            };
        }

        private List<CalendarDoctorStatsModel> GetCalendarDoctorStats(List<Appointment> appointments, List<CalendarDoctorModel> doctors)
        {
            var doctorStats = appointments.GroupBy(x => x.DoctorId)
                                          .Select(group => GetCalendarDoctorStatsModel(group, doctors))
                                          .Where(x => x != null)
                                          .OrderByDescending(x => x.Appointments)
                                          .ToList();

            doctorStats.Insert(0, new CalendarDoctorStatsModel
            {
                Id = string.Empty,
                Name = "All Doctors",
                Color = "#BCC8CF",
                Appointments = appointments.Count
            });

            var doctorIds = doctors.Select(x => x.Id).Distinct().ToList();
            var remainingDoctorsStats = DB.Staffs.Get<Doctor>()
                                          .Where(x => !doctorIds.Contains(x.Id))
                                          .OrderBy(x => x.Name)
                                          .Select(x => new CalendarDoctorStatsModel
                                          {
                                              Id = x.Id,
                                              Name = x.Name,
                                              Color = x.CalendarColor,
                                              Appointments = 0
                                          }).ToList();

            doctorStats.AddRange(remainingDoctorsStats);
            return doctorStats;
        }

        private CalendarDoctorStatsModel GetCalendarDoctorStatsModel(IGrouping<string, Appointment> group, List<CalendarDoctorModel> doctors)
        {
            var doctor = doctors.FirstOrDefault(x => x.Id == group.Key);
            if (doctor == null)
                return null;

            var model = new CalendarDoctorStatsModel
            {
                Id = group.Key,
                Name = doctor.Name,
                Color = doctor.Color,
                Appointments = group.Count()
            };

            return model;
        }

        private CalendarEventModel GetCalendarEventModel(Appointment appointment, List<CalendarPatientModel> patients, List<CalendarDoctorModel> doctors)
        {
            var doctor = doctors.FirstOrDefault(y => y.Id == appointment.DoctorId);
            var start = appointment.Date.InTimezone(Practice.Timezone);

            return new CalendarEventModel
            {
                id = appointment.Id,
                title = patients.FirstOrDefault(y => y.Id == appointment.PatientId).Name,
                description = string.Format("Apppointment with {0}", doctor.Name),
                color = doctor.Color,
                start = start.ToString(DateTimeFormats.ISO8601),
                isCanceled = appointment.IsCanceled()
            };
        }

        private void GetPatientsAndDoctors(List<Appointment> appointments, out List<CalendarPatientModel> patients, out List<CalendarDoctorModel> doctors)
        {
            var patientIds = appointments.Select(x => x.PatientId).Distinct().ToList();
            var doctorIds = appointments.Select(x => x.DoctorId).Distinct().ToList();

            patients = DB.Patients.Get()
                         .Where(x => patientIds.Contains(x.Id))
                         .Select(x => new CalendarPatientModel
                         {
                             Id = x.Id,
                             Name = x.Name
                         }).ToList();

            doctors = DB.Staffs.Get<Doctor>()
                        .Where(x => doctorIds.Contains(x.Id))
                        .Select(x => new CalendarDoctorModel
                        {
                            Id = x.Id,
                            Name = x.Name,
                            Color = x.CalendarColor
                        }).ToList();
        }

        #endregion

        #region CREATE

        [HttpPost]
        public JsonResult Add(AddAppointmentModel model)
        {
            if (!Privileges.AppointmentsWrite)
                return JsonResultAccessDenied();

            #region validate parameters

            if (!string.IsNullOrWhiteSpace(model.PatientId))
            {
                ModelState.Remove("PatientName");
            }

            if(!ModelState.IsValid)
            {
                return Json(new
                {
                    HasError = true,
                    Message = "Invalid data"
                });
            }

            #endregion

            var appointment = model.ToAppointment();
            
            if (string.IsNullOrWhiteSpace(model.PatientId))
            {
                var addPatientResult = AddPatient(model);
                if (addPatientResult.HasError)
                    return JsonResultInvalidData(string.Format("Error adding Patient - {0}", addPatientResult.Message));

                appointment.PatientId = addPatientResult.Patient.Id;
            }
            
            var result = DB.Appointments.Add(appointment).ToStandardResult("Appointment created");
            return JsonStandardResult(result);
        }

        private dynamic AddPatient(AddAppointmentModel model)
        {
            var patient = model.ToPatient();
            var result = DB.Patients.Add(patient);

            var error = result.HasError();
            var message = error ? result.LastErrorMessage : string.Empty;

            return new
            {
                HasError = error,
                Message = message,
                Patient = patient
            };
        }

        #endregion

        #region UPDATE

        public ActionResult Edit(string id)
        {
            if (!Privileges.AppointmentsWrite)
                return PartialViewModalAccessDenied();

            var appointment = DB.Appointments.GetById(id);
            if(appointment == null)
                return PartialViewModalNotFound();

            var model = new EditAppointmentModel(appointment);
            return PartialView("_Edit", model);
        }

        [HttpPost]
        public JsonResult Edit(EditAppointmentModel model)
        {
            if (!Privileges.AppointmentsWrite)
                return JsonResultAccessDenied();

            if (!ModelState.IsValid)
                return JsonResultInvalidData();

            var appointment = DB.Appointments.GetById(model.AppointmentId);
            if (appointment == null)
                return JsonResultInvalidData("Appointment not found!");

            model.UpdateAppointment(appointment);
            var result = DB.Appointments.Update(appointment).ToStandardResult("Updated");
            return JsonStandardResult(result);
        }

        #endregion

        #region DELETE

        public JsonResult Cancel(string id)
        {
            return SetCanceledTo(id, true);
        }

        public JsonResult Confirm(string id)
        {
            return SetCanceledTo(id, false);
        }

        public JsonResult Delete(string id)
        {
            if (!Privileges.AppointmentsWrite)
                return JsonResultAccessDenied();

            var appointment = DB.Appointments.GetById(id);
            if (appointment == null)
                return JsonResultInvalidData("Appointment not found!");

            var result = DB.Appointments.Remove(appointment).ToStandardResult("Deleted");
            return JsonStandardResult(result);
        }

        private JsonResult SetCanceledTo(string id, bool isCanceled)
        {
            if (!Privileges.AppointmentsWrite)
                return JsonResultAccessDenied();

            var appointment = DB.Appointments.GetById(id);
            if (appointment == null)
                return JsonResultInvalidData("Appointment not found!");

            appointment.IsCanceled(isCanceled);
            var result = DB.Appointments.Update(appointment).ToStandardResult(isCanceled ? "Canceled" : "Confirmed");
            return JsonStandardResult(result);
        }

        #endregion
    }
}