﻿using ProdoctCommon.Data;
using ProdoctCommon.Helpers;
using ProdoctWebApp.Models;
using ProdoctWebApp.MVC;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace ProdoctWebApp.Controllers
{
    [AllowAnonymous]
    public class LinksController : ProdoctBaseController
    {
        //Public -------------------------------------------------------
        public ActionResult Process(string email, string code)
        {
            if (!ValidateInputs(email, code))
                return InvalidLinkView();

            var link = DB.Links.GetUnConsumedByEmailAndCode(email, code);
            if (link == null)
                return InvalidLinkView();

            return ProcessLink(link);
        }

        public ActionResult NotFound()
        {
            return InvalidLinkView();
        }

        [HttpPost]
        public ActionResult SetPassword(SetPasswordModel model)
        {
            if (!ModelState.IsValid)
                return View();

            var link = DB.Links.GetUnConsumedByEmailAndCode(model.Email, model.Code);
            if (link == null)
                return InvalidLinkView();

            var user = DB.Users.GetByEmail(model.Email);
            if(user == null)
            {
                link.Consume();
                DB.Links.Update(link);
                return InvalidLinkView();
            }

            user.SetPassword(model.NewPassword);
            DB.Users.Update(user);

            if(link.Type == LinkType.ChangePassword)
            {
                link.Consume();
                DB.Links.Update(link);
                return LinkResponseView(new LinkResponseModel
                {
                    Heading = "Password changed",
                    Message = string.Format("You can now <a href='{0}'>login using your new credentials</a>.", Url.RouteUrl(RouteName.Login))
                });
            }

            return ProcessLink(link);
        }


        //UserEmailVerify ----------------------------------------------
        private ActionResult UserEmailVerify(Link link)
        {
            var user = DB.Users.GetById(link.UserId);
            if (UserEmailVerifyLinkErroneous(user, ref link))
                return InvalidLinkView();

            if (string.IsNullOrWhiteSpace(user.PasswordHash))
                return GetSetPasswordView(user, link);

            user.Verify();
            DB.Users.Update(user);

            link.Consume();
            DB.Links.Update(link);

            return LinkResponseView(new LinkResponseModel
            {
                Heading = "Verified!",
                Message = "Thank you for verifying your email Id. Hope you enjoy using prodoc."
            });
        }

        private bool UserEmailVerifyLinkErroneous(User user, ref Link link)
        {
            if (user == null)
                link.Result = new StandardResult(true, "User not found");
            else if (user.Email != link.Email)
                link.Result = new StandardResult(true, "User's email has changed");
            else if (user.IsVerified)
                link.Result = new StandardResult(true, "User already verified");

            if (link.Result.Error)
            {
                link.Consume();
                DB.Links.Update(link);
                return true;
            }

            return false;
        }


        //Staff Invitation ---------------------------------------------
        private ActionResult StaffInvitation(Link link)
        {
            var staff = DB.Staffs.GetById(link.StaffId);
            var user = staff == null ? null : DB.Users.GetById(link.UserId);

            if (StaffInvitationLinkErroneous(user, staff, ref link))
                return InvalidLinkView();

            if (string.IsNullOrWhiteSpace(user.PasswordHash))
                return GetSetPasswordView(user, link);

            if (!user.IsVerified)
            {
                user.Verify();
                DB.Users.Update(user);
            }

            staff.AcceptInvitation();
            DB.Staffs.Update(staff);

            link.Consume();
            DB.Links.Update(link);

            return LinkResponseView(new LinkResponseModel
            {
                Heading = "Invitation Accepted!",
                Message = "Thank you for accepting the invitation. Hope you enjoy using prodoc."
            });
        }

        private bool StaffInvitationLinkErroneous(User user, Staff staff, ref Link link)
        {
            if (user == null || staff == null)
                link.Result = new StandardResult(true, "User / staff not found");
            else if (user.Email != link.Email || staff.Email != link.Email)
                link.Result = new StandardResult(true, "User or staff's email has changed");
            else if (staff.HasAccepted)
                link.Result = new StandardResult(true, "Staff has already accepted invitation");

            if (link.Result.Error)
            {
                link.Consume();
                DB.Links.Update(link);
                return true;
            }

            return false;
        }


        //Change Password ----------------------------------------------
        private ActionResult ChangePassword(Link link)
        {
            var user = DB.Users.GetById(link.UserId);

            if (ChangePasswordLinkErroneous(user, ref link))
                return InvalidLinkView();

            return GetSetPasswordView(user, link);
        }

        private bool ChangePasswordLinkErroneous(User user, ref Link link)
        {
            if(user == null)
                link.Result = new StandardResult(true, "User not found");
            else if(user.Email != link.Email)
                link.Result = new StandardResult(true, "User's email has changed");

            if (link.Result.Error)
            {
                link.Consume();
                DB.Links.Update(link);
                return true;
            }

            return false;
        }

        private ActionResult GetSetPasswordView(User user, Link link)
        {
            var model = new SetPasswordModel
            {
                Email = user.Email,
                Code = link.Code
            };

            return View("SetPassword", model);
        }


        //Helpers -------------------------------------------------------
        private ActionResult ProcessLink(Link link)
        {
            switch (link.Type)
            {
                case LinkType.ChangePassword:
                    return ChangePassword(link);

                case LinkType.StaffInvitation:
                    return StaffInvitation(link);

                case LinkType.UserEmailVerify:
                    return UserEmailVerify(link);

                default:
                    return InvalidLinkView();
            }
        }

        private bool ValidateInputs(string email, string code)
        {
            if (string.IsNullOrWhiteSpace(email) || string.IsNullOrWhiteSpace(code))
                return false;

            if (!Regex.IsMatch(email, RegexFormats.EmailCaseInsensitive) || 
                !Regex.IsMatch(code, RegexFormats.Code16))
                return false;

            return true;
        }

        private ActionResult InvalidLinkView()
        {
            return LinkResponseView(new LinkResponseModel
            {
                Heading = "Not Found",
                Message = "If you need help, please copy the link from your browser's address bar and send it to <a href='mailto:support@prodoc.in'>support@prodoc.in</a>"
            });
        }

        private ActionResult LinkResponseView(LinkResponseModel model)
        {
            return View("LinkResponse", model);
        }
    }
}