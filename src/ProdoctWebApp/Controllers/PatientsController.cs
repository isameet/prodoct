﻿using ProdoctCommon.Data;
using ProdoctCommon.Dto;
using ProdoctCommon.Extensions;
using ProdoctCommon.Helpers;
using ProdoctWebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace ProdoctWebApp.Controllers
{
    public class PatientsController : ProdoctBaseController
    {
        public PatientsController()
        {
            Helpers.General.Sleep();
        }

        #region READ

        public ActionResult Index()
        {
            if (!Privileges.PatientsRead)
                return RedirectToAllowedRoute();

            return View();
        }

        public JsonResult List(ListSettingsDefault settings)
        {
            var count = 0;
            var patients = DB.Patients.GetList(settings, out count)
                          .Select(x => new PatientModel(x))
                          .ToList();

            var model = new PatientsListModel
            {
                Patients = patients,
                MoreExist = !(patients.Count < settings.Take),
                Skip = settings.Skip + patients.Count
            };

            var html = RenderPartialViewToString("_List", model);

            return Json(new
            {
                HasError = false,
                Message = string.Empty,
                Html = html,
                Count = count
            });
        }

        public JsonResult Name(string query)
        {
            if (string.IsNullOrWhiteSpace(query))
                return Json(new
                {
                    suggestions = new string[] { }
                });

            var patients = DB.Patients.SearchByName(query, 20);

            return Json(new
            {
                Patients = GetSearchResults(patients)
            });
        }

        public JsonResult Mobile(string query)
        {
            if (string.IsNullOrWhiteSpace(query))
                return Json(new
                {
                    suggestions = new string[] { }
                });

            var patients = DB.Patients.SearchByMobile(query, 20);

            return Json(new
            {
                Patients = GetSearchResults(patients)
            });
        }

        public JsonResult NameOrMobile(string query)
        {
            if (string.IsNullOrWhiteSpace(query))
                return Json(new
                {
                    suggestions = new string[] { }
                });

            var patients = DB.Patients.SearchByNameOrMobile(query, 20);

            return Json(new
            {
                Patients = GetSearchResults(patients)
            });
        }

        private dynamic GetSearchResults(IEnumerable<Patient> patients)
        {
            return patients.Select(x => new
            {
                Id = x.Id,
                Name = x.Name,
                Mobile = x.Mobile,
                Locality = x.Locality
            }).ToList();
        }

        #endregion

        #region ADD

        public ActionResult Add()
        {
            if (!CanWrite())
                return PartialViewModalAccessDenied();

            return PartialView("_Add", new PatientAddModel());
        }

        [HttpPost]
        public ActionResult Add(PatientAddModel model)
        {
            if (!CanWrite())
                return JsonResultAccessDenied();

            if (!ModelState.IsValid)
                return JsonResultInvalidData();

            var message = string.Empty;
            if (!CheckPatientAddConditions(model, out message))
                return JsonResultInvalidData(message);

            var patient = model.ToPatient();
            var result = DB.Patients.Add(patient).ToStandardResult("Patient added");

            return JsonStandardResult(result);
        }

        private bool CheckPatientAddConditions(PatientAddModel model, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrWhiteSpace(model.Mobile))
                return true;

            var nameUpper = model.Name.ToUpper();
            var duplicate = DB.Patients.Get().FirstOrDefault(x => x.Name.ToUpper() == nameUpper &&
                                                                  x.Mobile == model.Mobile);

            if (duplicate != null)
            {
                message = "Patient with same name and mobile number already exists";
                return false;
            }

            return true;
        }

        #endregion

        #region UPDATE

        public ActionResult Edit(string id)
        {
            if (!CanWrite())
                return PartialViewModalAccessDenied();

            var patient = DB.Patients.GetById(id);
            if (patient == null)
                return PartialViewModalNotFound();

            var model = new PatientEditModel(patient);
            return PartialView("_Edit", model);
        }

        [HttpPost]
        public ActionResult Edit(PatientEditModel model)
        {
            if (!CanWrite())
                return JsonResultAccessDenied();

            if (!ModelState.IsValid)
                return JsonResultInvalidData();

            var message = string.Empty;
            if (!CheckPatientEditConditions(model, out message))
                return JsonResultInvalidData(message);

            var patient = DB.Patients.GetById(model.Id);
            if (patient == null)
                return JsonResultInvalidData("Patient not found!");

            model.ToPatient(patient);
            var result = DB.Patients.Update(patient).ToStandardResult("Updated");
            return JsonStandardResult(result);
        }

        private bool CheckPatientEditConditions(PatientEditModel model, out string message)
        {
            message = string.Empty;

            if (string.IsNullOrWhiteSpace(model.Mobile))
                return true;

            var nameUpper = model.Name.ToUpper();
            var duplicate = DB.Patients.Get().FirstOrDefault(x => x.Name.ToUpper() == nameUpper &&
                                                                  x.Mobile == model.Mobile &&
                                                                  x.Id != model.Id);

            if (duplicate != null)
            {
                message = "Patient with same name and mobile number already exists";
                return false;
            }

            return true;
        }

        #endregion

        #region DELETE

        public JsonResult Delete(string id)
        {
            if (!CanWrite())
                return JsonResultAccessDenied();

            var patient = DB.Patients.GetById(id);
            if (patient == null)
                return JsonResultInvalidData("Patient not found!");

            var message = string.Empty;
            if (!CheckPreconditionsForDeletingPatient(patient, out message))
                return JsonResultInvalidData(message);

            var result = DB.Patients.Remove(patient).ToStandardResult("Deleted");
            return JsonStandardResult(result);
        }

        private bool CheckPreconditionsForDeletingPatient(Patient patient, out string message)
        {
            message = string.Empty;

            var anyAppointment = DB.Appointments.Get().FirstOrDefault(x => x.PatientId == patient.Id);
            if (anyAppointment != null)
            {
                message = "Sorry, cannot delete a Patient having Appointments";
                return false;
            }

            return true;
        }

        #endregion

        private bool CanWrite()
        {
            return Privileges.PatientsWrite;
        }
    }
}