﻿using ProdoctCommon.Data;
using ProdoctCommon.Helpers;
using ProdoctCommon.Logging;
using ProdoctWebApp.MVC;
using System;
using System.IO;
using System.Security.Claims;
using System.Text;
using System.Web.Mvc;

namespace ProdoctWebApp.Controllers
{
    public abstract class ProdoctBaseController : Controller
    {
        protected AppContext App { 
            get 
            {
                if (_app == null)
                    _app = APP.Common;

                return _app; 
            } 
        }
        AppContext _app;

        protected ProdoctDatabase DB { get { return App.DB; } }
        protected ILogger Log { get { return App.Log; } }
        protected Staff LoggedInStaff { get { return App.Staff; } }
        protected User LoggedInUser { get { return App.User; } }
        protected Practice Practice { get { return App.Practice; } }
        


        public Privileges Privileges
        {
            get
            {
                var staff = App.Staff;
                if (staff == null)
                    return Privileges.GetEmptyPrivileges();

                return staff.Privileges;
            }
        }
        public bool IsAdmin { get { return Privileges.IsAdmin; } }
        public bool CanWriteAppointments { get { return Privileges.AppointmentsWrite && Privileges.AppointmentsRead; } }



        protected ProdoctBaseController()
        {
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            if (LoggedInUser != null)
                PopulateViewBag();
        }

        private void PopulateViewBag()
        {
            var isVerified = LoggedInUser.IsVerified;
            var noPractice = Practice == null;
            var allowLogin = LoggedInStaff != null && LoggedInStaff.AllowLogin;
            var hasAccepted = allowLogin && LoggedInStaff.HasAccepted;

            ViewBag.Privileges = Privileges;
            ViewBag.IsVerified = isVerified;
            ViewBag.NoPractice = noPractice;
            ViewBag.AllowLogin = allowLogin;
            ViewBag.HasAccepted = hasAccepted;
            ViewBag.LogoTitle = noPractice ? "prodoc" : Practice.Name;

            ViewBag.SettingsOnly = !isVerified || noPractice || !allowLogin || !hasAccepted;
        }




        protected ActionResult RedirectToAllowedRoute()
        {
            return RedirectToRoute(GetAllowedRoute());
        }

        protected string GetAllowedRoute(Privileges privileges = null)
        {
            privileges = privileges ?? Privileges;

            if (privileges.AppointmentsRead)
                return RouteName.Appointments;

            if (privileges.PatientsRead)
                return RouteName.Patients;

            if (privileges.InventoryRead)
                return RouteName.Inventory;

            if (privileges.FinanceRead)
                return RouteName.Finance;

            return RouteName.Settings;
        }




        protected string RenderPartialViewToString(ControllerBase controller, string viewName, object model)
        {
            var originalModel = controller.ViewData.Model;
            controller.ViewData.Model = model;
            try
            {
                using (var sw = new StringWriter())
                {
                    var viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                    var viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData,
                                                      controller.TempData, sw);
                    viewResult.View.Render(viewContext, sw);

                    return sw.GetStringBuilder().ToString();
                }
            }
            catch (Exception ex)
            {
                Log.Error("RenderPartialViewToString", ex);
                throw;
            }
            finally
            {
                controller.ViewData.Model = originalModel;
            }
        }

        public string RenderPartialViewToString(string viewName, object model)
        {
            var controller = ControllerContext.Controller;

            return RenderPartialViewToString(controller, viewName, model);
        }




        protected override JsonResult Json(object data, string contentType, Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            Response.AddHeader("Cache-Control", "no cache, no store");
            return base.Json(data, contentType, contentEncoding, JsonRequestBehavior.AllowGet);
        }

        protected JsonResult JsonResultInvalidData(string message = "Invalid data")
        {
            return JsonResultMessage(true, message);
        }

        protected JsonResult JsonResultAccessDenied(string message = "Access denied")
        {
            return JsonResultMessage(true, message);
        }

        protected JsonResult JsonStandardResult(StandardResult result)
        {
            return JsonResultMessage(result.Error, result.Message);
        }

        protected JsonResult JsonResultMessage(bool error, string message)
        {
            return Json(new
            {
                HasError = error,
                Message = message
            });
        }

        protected JsonResult JsonResultWithRedirectUrl(string url)
        {
            return Json(new
            {
                HasError = false,
                Message = string.Empty,
                RedirectUrl = url
            });
        }

        protected JsonResult JsonResultWithNotFoundRedirectUrl()
        {
            return JsonResultWithRedirectUrl(Url.RouteUrl(RouteName.NotFound));
        }

        protected PartialViewResult PartialViewNotFound()
        {
            return PartialView("_NotFound");
        }

        protected PartialViewResult PartialViewModalNotFound()
        {
            return PartialView("_NotFoundModal");
        }

        protected PartialViewResult PartialViewAccessDenied()
        {
            return PartialView("_AccessDenied");
        }

        protected PartialViewResult PartialViewModalAccessDenied()
        {
            return PartialView("_AccessDeniedModal");
        }

        protected PartialViewResult PartialViewError(string message = null)
        {
            return PartialView("_Error", message);
        }

        protected ViewResult ViewMessage(string message = null)
        {
            if (string.IsNullOrWhiteSpace(message))
                message = "An error occured";

            return View("Message", message);
        }

        protected ViewResult ViewNotFound(string message = null)
        {
            if (string.IsNullOrWhiteSpace(message))
                message = "Not found!";

            return ViewMessage(message);
        }

        protected ViewResult ViewAccessDenied(string message = null)
        {
            if (string.IsNullOrWhiteSpace(message))
                message = "Access denied!";

            return ViewMessage(message);
        }
    }
}