﻿using ProdoctCommon.Data;
using ProdoctCommon.Helpers;
using ProdoctWebApp.Helpers;
using ProdoctWebApp.Models;
using ProdoctWebApp.MVC;
using System;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace ProdoctWebApp.Controllers
{
    [AllowAnonymous]
    public class AccountController : ProdoctBaseController
    {
        #region Login / Logout

        [AnonymousOnly]
        public ActionResult Login(string returnUrl)
        {
            var model = new LoginModel
            {
                ReturnUrl = returnUrl
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            if (!ModelState.IsValid)
                return View();

            User user = null;

            if (CheckCredentials(model, out user))
            {
                Staff staff = null;
                if (!string.IsNullOrWhiteSpace(user.DefaultPracticeId))
                {
                    staff = DB.Staffs.Get().FirstOrDefault(x => x.PracticeId == user.DefaultPracticeId && x.UserId == user.Id);
                }

                if (staff == null)
                    staff = new Staff();

                var identity = new ClaimsIdentity(new[] { new Claim(ClaimTypes.Name, user.Id) }, AuthNames.ApplicationCookie);
                Request.GetOwinContext().Authentication.SignIn(identity);

                if (staff != null)
                {
                    staff.LastLogin = DateTime.Now;
                    DB.Staffs.Update(staff);
                }

                return Redirect(GetRedirectUrl(model.ReturnUrl, staff.Privileges));
            }

            ModelState.AddModelError("", "Invalid email or password");
            return View();
        }

        private bool CheckCredentials(LoginModel model, out User user)
        {
            user = DB.Users.GetByEmail(model.Email);
            if (user == null)
                return false;

            if (!AuthenticationHelper.VerifyHashedPassword(user.PasswordHash, model.Password))
            {
                user = null;
                return false;
            }

            return true;
        }

        [AllowAnonymous]
        public ActionResult LogOut()
        {
            Request.GetOwinContext().Authentication.SignOut(AuthNames.ApplicationCookie);
            return RedirectToAction("Login");
        }

        private string GetRedirectUrl(string returnUrl, Privileges privileges)
        {
            if (string.IsNullOrWhiteSpace(returnUrl) || !Url.IsLocalUrl(returnUrl))
            {
                return Url.RouteUrl(GetAllowedRoute(privileges));
            }

            return returnUrl;
        }

        #endregion

        #region change password

        [AnonymousOnly]
        public ActionResult ForgotPassword()
        {
            return View(new ForgotPasswordModel());
        }

        [HttpPost]
        public ActionResult ForgotPassword(ForgotPasswordModel model)
        {
            if (!ModelState.IsValid)
                return JsonResultInvalidData();

            var user = DB.Users.GetByEmail(model.Email);
            if (user == null)
                return Json(new { HasError = false, Message = string.Empty });

            LinkHelper.ScheduleChangePassword(user, Url);

            return Json(new { HasEror = false, Message = string.Empty });
        }

        #endregion
    }
}