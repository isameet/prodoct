﻿using ProdoctCommon.Data;
using ProdoctCommon.Extensions;
using ProdoctCommon.Helpers;
using ProdoctWebApp.Helpers;
using ProdoctWebApp.Models;
using ProdoctWebApp.MVC;
using System.Linq;
using System.Web.Mvc;

namespace ProdoctWebApp.Controllers
{
    public class SettingsController : ProdoctBaseController
    {
        public SettingsController()
        {
            Helpers.General.Sleep();
        }

        [AllowUnauthorized]
        public ActionResult Index()
        {
            return View();
        }

        #region MY PROFILE

        #region READ

        [AllowUnauthorized]
        public ActionResult MyProfile()
        {
            return PartialView("_Profile");
        }

        [AllowUnauthorized]
        public ActionResult MyInfo()
        {
            var user = LoggedInUser;
            var model = new MyInfoModel(user);

            return PartialView("_ProfileInfo", model);
        }

        [AllowUnauthorized]
        public ActionResult ChangePassword()
        {
            var model = new ChangePasswordModel();
            return PartialView("_ProfilePassword", model);
        }

        #endregion

        #region UPDATE

        [HttpPost]
        [AllowUnauthorized]
        public JsonResult MyInfo(MyInfoModel model)
        {
            if (!ModelState.IsValid)
                return JsonResultInvalidData();

            var user = LoggedInUser;
            var emailResult = user.SetEmail(model.Email, APP.Common);
            if (emailResult.Error)
                return JsonResultInvalidData(emailResult.Message);

            user.Name = model.Name;
            user.Mobile = model.Mobile;

            var result = DB.Users.Update(user).ToStandardResult("Updated");
            
            if (!result.Error)
            {
                foreach(var staff in emailResult.StaffsAffected)
                    DB.Staffs.Update(staff);
            }

            var redirectUrl = string.Empty;
            if (emailResult.Changed)
            {
                LinkHelper.ScheduleUserEmailVerify(user, Url);
                redirectUrl = Url.RouteUrl(RouteName.Settings);
            }

            return Json(new
            {
                HasError = result.Error,
                Message = result.Message,
                RedirecUrl = redirectUrl
            });
        }

        [HttpPost]
        [AllowUnauthorized]
        public JsonResult ChangePassword(ChangePasswordModel model)
        {
            if (!ModelState.IsValid)
                return JsonResultInvalidData();

            var user = LoggedInUser;
            if (!AuthenticationHelper.VerifyHashedPassword(user.PasswordHash, model.CurrentPassword))
                return JsonResultInvalidData("Incorrect Current Password");

            user.SetPassword(model.NewPassword);
            var result = DB.Users.Update(user).ToStandardResult("Password changed");
            return JsonStandardResult(result);
        }

        #endregion

        #endregion

        #region PRACTICE PROFILE

        #region READ

        public ActionResult PracticeProfile()
        {
            if (!IsAdmin)
                return PartialViewAccessDenied();

            var model = new PracticeSettingsModel(Practice);
            return PartialView("_Practice", model);
        }

        #endregion

        #region UPDATE

        [HttpPost]
        public JsonResult PracticeProfile(PracticeSettingsModel model)
        {
            if (!IsAdmin)
                return JsonResultAccessDenied();

            if (!ModelState.IsValid)
                return JsonResultInvalidData();

            var practice = Practice;
            model.ToPractice(practice);

            var result = DB.Practices.Update(practice).ToStandardResult("Updated");
            return JsonStandardResult(result);
        }

        #endregion

        #endregion

        #region PRACTICE STAFF

        #region READ

        public ActionResult Staff()
        {
            if (!IsAdmin)
                return PartialViewAccessDenied();

            var model = DB.Staffs.Get(new string[] { "Name", "Role", "Privileges.IsAdmin", "Title", "HasAccepted", "AllowLogin", "LastLogin" })
                          .AsQueryable()
                          .OrderBy(x => x.Role)
                          .Select(x => new StaffListModel(x))
                          .ToList();

            return PartialView("_Staff", model);
        }

        #endregion

        #region CREATE

        public ActionResult StaffAdd()
        {
            if (!IsAdmin)
                return PartialViewModalAccessDenied();

            return PartialView("_StaffAdd", new StaffAddModel());
        }

        [HttpPost]
        public JsonResult StaffAdd(StaffAddModel model)
        {
            if (!IsAdmin)
                return JsonResultAccessDenied();

            if (!ModelState.IsValid)
                return JsonResultInvalidData();

            var message = string.Empty;
            if (!CheckStaffAddConditions(model, out message))
                return JsonResultInvalidData(message);

            var userResult = model.GetUser();
            if (userResult.Error)
                return JsonResultInvalidData(string.Format("User not created. {0}", userResult.Message));
            else
                ProcessUserResult(userResult);

            var staff = model.ToStaff();
            var result = DB.Staffs.Add(staff).ToStandardResult("Staff added");
            var staffId = string.Empty;

            if (!result.Error)
            {
                if (staff.AllowLogin)
                {
                    staffId = staff.Id;
                    result.Message = string.Empty;
                    LinkHelper.ScheduleStaffInvitation(staff, Url);
                }
            }

            return Json(new
            {
                HasError = result.Error,
                Message = result.Message,
                Id = staffId
            });
        }

        private void ProcessUserResult(StaffGetUserResult userResult)
        {
            if (userResult.IsNew)
                DB.Users.Add(userResult.User);
            else
                DB.Users.Update(userResult.User);
        }

        private bool CheckStaffAddConditions(StaffAddModel model, out string message)
        {
            message = string.Empty;
            var duplicate = DB.Staffs.GetByEmail(model.Email);
            if(duplicate != null)
            {
                message = "Another staff member uses same Email Id";
                return false;
            }

            return true;
        }

        public ActionResult DoctorAdd()
        {
            if (!IsAdmin)
                return PartialViewModalAccessDenied();

            return PartialView("_DoctorAdd", new DoctorAddModel());
        }

        [HttpPost]
        public JsonResult DoctorAdd(DoctorAddModel model)
        {
            if (!IsAdmin)
                return JsonResultAccessDenied();

            if (!ModelState.IsValid)
                return JsonResultInvalidData();

            var message = string.Empty;
            if (!CheckDoctorAddConditions(model, out message))
                return JsonResultInvalidData(message);

            var userResult = model.GetUser();
            if (userResult.Error)
                return JsonResultInvalidData(string.Format("User not created. {0}", userResult.Message));
            else
                ProcessUserResult(userResult);

            var doctor = model.ToDoctor();
            var result = DB.Staffs.Add(doctor).ToStandardResult("Doctor added");
            var doctorId = string.Empty;

            if (!result.Error)
            {
                if (doctor.AllowLogin)
                {
                    doctorId = doctor.Id;
                    result.Message = string.Empty;
                    LinkHelper.ScheduleStaffInvitation(doctor, Url);
                }
            }

            return Json(new
            {
                HasError = result.Error,
                Message = result.Message,
                Id = doctorId
            });
        }

        private bool CheckDoctorAddConditions(DoctorAddModel model, out string message)
        {
            message = string.Empty;
            var duplicate = DB.Staffs.GetByEmail(model.Email);
            if (duplicate != null)
            {
                message = "Another staff member uses same Email Id";
                return false;
            }

            return true;
        }

        #endregion

        #region UPDATE

        public ActionResult StaffEdit(string id)
        {
            if (!IsAdmin)
                return PartialViewModalAccessDenied();

            var staff = DB.Staffs.GetById(id);
            if (staff == null)
                return PartialViewModalNotFound();

            var model = new StaffEditModel(staff);
            return PartialView("_StaffEdit", model);
        }

        public ActionResult StaffBasics(string id)
        {
            if (!IsAdmin)
                return PartialViewAccessDenied();

            var staff = DB.Staffs.GetById(id);
            if (staff == null)
                return PartialViewNotFound();

            var model = new StaffBasicsModel(staff, APP.Common.UserId);
            return PartialView("_StaffEditBasics", model);
        }

        public ActionResult StaffPrivileges(string id)
        {
            if (!IsAdmin)
                return PartialViewAccessDenied();

            var staff = DB.Staffs.GetById(id);
            if (staff == null)
                return PartialViewNotFound();

            var model = new StaffPrivilegesModel(staff, APP.Common.UserId);
            return PartialView("_StaffEditPrivileges", model);
        }

        [HttpPost]
        public JsonResult StaffBasics(StaffBasicsModel model)
        {
            if (!IsAdmin)
                return JsonResultAccessDenied();

            if (!ModelState.IsValid)
                return JsonResultInvalidData();

            var staff = DB.Staffs.GetById(model.Id);
            if (staff == null)
                return JsonResultInvalidData("Staff not found!");

            model.ToStaff(staff);
            var allowLoginResult = staff.SetAllowLogin(model.AllowLogin, APP.Common);
            if(allowLoginResult.Error)
                return JsonResultInvalidData(allowLoginResult.Message);

            var emailResult = staff.SetEmail(model.Email, APP.Common);
            if (emailResult.Error)
                return JsonResultInvalidData(emailResult.Message);

            var result = DB.Staffs.Update(staff).ToStandardResult("Updated");

            if (!result.Error)
            {
                DB.Users.Process(emailResult);
                ProcessLinkReval(emailResult, allowLoginResult, staff);
            }

            return JsonStandardResult(result);
        }

        [HttpPost]
        public JsonResult StaffPrivileges(StaffPrivilegesModel model)
        {
            if (!IsAdmin)
                return JsonResultAccessDenied();

            if (!ModelState.IsValid)
                return JsonResultInvalidData();

            var staff = DB.Staffs.GetById(model.Id);
            if (staff == null)
                return JsonResultInvalidData("Staff not found!");

            var message = string.Empty;
            if (!CheckPreconditionsForEditingPrivileges(staff, model, out message))
                return JsonResultInvalidData(message);

            if (model.IsAdmin)
                staff.SetAdminPrivileges();
            else
                staff.Privileges = model.ToPrivileges();

            var result = DB.Staffs.Update(staff).ToStandardResult("Updated");
            return JsonStandardResult(result);
        }

        public ActionResult DoctorEdit(string id)
        {
            if (!IsAdmin)
                return PartialViewModalAccessDenied();

            var doctor = DB.Staffs.GetById<Doctor>(id);
            if (doctor == null)
                return PartialViewModalNotFound();

            var model = new DoctorEditModel(doctor);
            return PartialView("_DoctorEdit", model);
        }

        public ActionResult DoctorBasics(string id)
        {
            if (!IsAdmin)
                return PartialViewAccessDenied();

            var doctor = DB.Staffs.GetById<Doctor>(id);
            if (doctor == null)
                return PartialViewNotFound();

            var model = new DoctorBasicsModel(doctor, APP.Common.UserId);
            return PartialView("_DoctorEditBasics", model);
        }

        public ActionResult DoctorPrivileges(string id)
        {
            if (!IsAdmin)
                return PartialViewAccessDenied();

            var doctor = DB.Staffs.GetById<Doctor>(id);
            if (doctor == null)
                return PartialViewModalNotFound();

            var model = new DoctorPrivilegesModel(doctor);
            return PartialView("_DoctorEditPrivileges", model);
        }

        [HttpPost]
        public JsonResult DoctorBasics(DoctorBasicsModel model)
        {
            if (!IsAdmin)
                return JsonResultAccessDenied();

            if (!ModelState.IsValid)
                return JsonResultInvalidData();

            var doctor = DB.Staffs.GetById<Doctor>(model.Id);
            if (doctor == null)
                return JsonResultInvalidData("Doctor not found!");

            model.ToDoctor(doctor);
            
            var allowLoginResult = doctor.SetAllowLogin(model.AllowLogin, APP.Common);
            if (allowLoginResult.Error)
                return JsonResultInvalidData(allowLoginResult.Message);

            var emailResult = doctor.SetEmail(model.Email, APP.Common);
            if (emailResult.Error)
                return JsonResultInvalidData(emailResult.Message);

            var result = DB.Staffs.Update(doctor).ToStandardResult("Updated");
            
            if (!result.Error)
            {
                DB.Users.Process(emailResult);
                ProcessLinkReval(emailResult, allowLoginResult, doctor);
            }

            return JsonStandardResult(result);
        }

        [HttpPost]
        public JsonResult DoctorPrivileges(DoctorPrivilegesModel model)
        {
            if (!IsAdmin)
                return JsonResultAccessDenied();

            if (!ModelState.IsValid)
                return JsonResultInvalidData();

            var doctor = DB.Staffs.GetById<Doctor>(model.Id);
            if (doctor == null)
                return JsonResultInvalidData("Doctor not found!");

            var message = string.Empty;
            if (!CheckPreconditionsForEditingPrivileges(doctor, model, out message))
                return JsonResultInvalidData(message);

            if (model.IsAdmin)
                doctor.SetAdminPrivileges();
            else
                doctor.Privileges = model.ToPrivileges();

            var result = DB.Staffs.Update(doctor).ToStandardResult("Updated");
            return JsonStandardResult(result);
        }

        private bool CheckPreconditionsForEditingPrivileges(Staff staff, StaffPrivilegesModel model, out string message)
        {
            message = string.Empty;

            //if changing from admin to non-admin, ensure there is at least one active admin
            if (staff.Privileges.IsAdmin && model.IsAdmin == false)
            {
                var anotherAdmin = DB.Staffs.GetOneActiveAdminOtherThan(staff.Id);
                if (anotherAdmin == null)
                {
                    message = "At least one active admin required";
                    return false;
                }
            }

            return true;
        }

        private void ProcessLinkReval(SetStaffEmailResult emailResult, SetAllowLoginResult allowLoginResult, Staff staff)
        {
            if (ShouldLinkBeSent(emailResult, allowLoginResult, staff))
            {
                LinkHelper.ScheduleStaffInvitation(staff, Url);
            }
            else if (allowLoginResult.LinkShouldBeDeleted)
            {
                LinkHelper.CancelStaffInvitations(staff);
            }
        }

        private bool ShouldLinkBeSent(SetStaffEmailResult emailResult, SetAllowLoginResult allowLoginResult, Staff staff)
        {
            return (emailResult.Changed && staff.AllowLogin) || allowLoginResult.LinkShouldBeSent;
        }

        #endregion

        #region DELETE

        public JsonResult StaffDelete(string id)
        {
            if (!IsAdmin)
                return JsonResultAccessDenied();

            var staff = DB.Staffs.GetById(id);
            if (staff == null)
                return JsonResultInvalidData("Staff not found!");

            var message = string.Empty;
            if (!CheckPreconditionsForDeletingStaff(staff, out message))
                return JsonResultInvalidData(message);

            var result = DB.Staffs.Remove(staff).ToStandardResult("Deleted");

            if (!result.Error)
            {
                RemoveUserPractice(staff);
                LinkHelper.CancelStaffInvitations(staff);
            }

            return JsonStandardResult(result);
        }

        public JsonResult DoctorDelete(string id)
        {
            if (!IsAdmin)
                return JsonResultAccessDenied();

            var doctor = DB.Staffs.GetById<Doctor>(id);
            if (doctor == null)
                return JsonResultInvalidData("Doctor not found!");

            var message = string.Empty;
            if (!CheckPreconditionsForDeletingDoctor(doctor, out message))
                return JsonResultInvalidData(message);

            var result = DB.Staffs.Remove(doctor).ToStandardResult("Deleted");

            if (!result.Error)
            {
                RemoveUserPractice(doctor);
                LinkHelper.CancelStaffInvitations(doctor);
            }

            return JsonStandardResult(result);
        }

        private bool CheckPreconditionsForDeletingStaff(Staff staff, out string message)
        {
            message = string.Empty;

            //can't delete self
            if(staff.UserId == APP.Common.UserId)
            {
                message = "Cannot delete your own account";
                return false;
            }

            return true;
        }

        private bool CheckPreconditionsForDeletingDoctor(Doctor doctor, out string message)
        {
            message = string.Empty;

            if (!CheckPreconditionsForDeletingStaff(doctor, out message))
                return false;

            var anyAppointment = DB.Appointments.Get().FirstOrDefault(x => x.DoctorId == doctor.Id);
            if(anyAppointment != null)
            {
                message = "Cannot delete a Doctor having Appointments. You could disable login instead.";
                return false;
            }

            return true;
        }

        private void RemoveUserPractice(Staff staff)
        {
            var user = DB.Users.GetById(staff.UserId);
            if (user != null)
            {
                user.RemovePractice(staff.PracticeId);
                DB.Users.Update(user);
            }
        }

        #endregion

        #endregion

        #region CALENDAR SETTINGS

        #region READ

        public ActionResult Calendar()
        {
            if (!Privileges.AppointmentsRead)
                return PartialViewAccessDenied();

            var staff = LoggedInStaff;
            if (staff == null)
                return PartialViewNotFound();

            var model = new CalendarSettingsModel(staff.CalendarSettings);
            return PartialView("_Calendar", model);
        }

        #endregion

        #region UPDATE

        [HttpPost]
        public JsonResult Calendar(CalendarSettingsModel model)
        {
            if (!Privileges.AppointmentsRead)
                return JsonResultAccessDenied();

            if (!ModelState.IsValid)
                return JsonResultInvalidData();

            var staff = LoggedInStaff;
            if (staff == null)
                return JsonResultInvalidData("Account not found!");

            staff.CalendarSettings = model.ToCalendarSettings();
            var result = DB.Staffs.Update(staff).ToStandardResult("Updated");
            return JsonStandardResult(result);
        }

        #endregion

        #endregion
    }
}