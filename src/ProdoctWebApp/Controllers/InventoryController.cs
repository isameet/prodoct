﻿using ProdoctCommon.Data;
using ProdoctCommon.Dto;
using ProdoctCommon.Extensions;
using ProdoctCommon.Helpers;
using ProdoctWebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProdoctWebApp.Controllers
{
    public class InventoryController : ProdoctBaseController
    {
        public InventoryController()
        {
            Helpers.General.Sleep();
        }

        #region READ ITEM

        public ActionResult Index()
        {
            if (!CanRead())
                return RedirectToAllowedRoute();

            var model = new InventoryIndexModel
            {
                ItemList = GetAllItemsList()
            };
            
            return View(model);
        }

        public ActionResult ListItems(ListSettingsItems settings)
        {
            var model = GetAllItemsList(settings);
            var html = RenderPartialViewToString("_ListItem", model);
            return Json(new
            {
                HasError = false,
                Message = string.Empty,
                Html = html
            });
        }

        private ItemListModel GetAllItemsList(ListSettingsItems settings = null)
        {
            settings = settings ?? new ListSettingsItems();
            var count = 0;
            var list = DB.Inventory.GetList(settings, out count)
                                   .Select(x => new ItemModel(x))
                                   .ToList();

            return new ItemListModel
            {
                Items = list,
                MoreExist = !(list.Count < settings.Take),
                Skip = settings.Skip + list.Count
            };
        }

        #endregion

        #region CREATE ITEM

        public ActionResult AddItem()
        {
            if (!CanWrite())
                return PartialViewModalAccessDenied();

            return PartialView("_AddItem");
        }

        [HttpPost]
        public ActionResult AddItem(ItemAddModel model)
        {
            if (!CanWrite())
                return JsonResultAccessDenied();

            if (!ModelState.IsValid)
                return JsonResultInvalidData();

            var message = string.Empty;
            if (!CheckItemAddConditions(model, out message))
                return JsonResultInvalidData(message);

            var item = model.ToItem();
            var result = DB.Inventory.Add(item).ToStandardResult("Item added");

            return JsonStandardResult(result);
        }

        private bool CheckItemAddConditions(ItemAddModel model, out string message)
        {
            message = string.Empty;
            
            var duplicate = DB.Inventory.GetByName(model.Name);
            if(duplicate != null)
            {
                message = "Another item uses same name";
                return false;
            }

            duplicate = DB.Inventory.GetByCode(model.Code);
            if(duplicate != null)
            {
                message = "Another item uses same code";
                return false;
            }

            return true;
        }

        #endregion

        #region UPDATE ITEM

        public ActionResult Edit(string id)
        {
            if (!CanWrite())
                return PartialViewModalAccessDenied();

            var item = DB.Inventory.GetById(id);
            if (item == null)
                return PartialViewModalNotFound();

            var model = new ItemEditContainerModel(item);
            return PartialView("_EditItem", model);
        }

        public ActionResult EditBasics(string id)
        {
            if (!CanWrite())
                return PartialViewAccessDenied();

            var item = DB.Inventory.GetById(id);
            if (item == null)
                return PartialViewNotFound();

            var model = new ItemEditModel(item);
            return PartialView("_EditItemBasics", model);
        }

        [HttpPost]
        public ActionResult EditBasics(ItemEditModel model)
        {
            if (!CanWrite())
                return JsonResultAccessDenied();

            if (!ModelState.IsValid)
                return JsonResultInvalidData();

            var message = string.Empty;
            if (!CheckItemEditConditions(model, out message))
                return JsonResultInvalidData(message);

            var item = DB.Inventory.GetById(model.Id);
            if (item == null)
                return JsonResultInvalidData("Item not found!");

            model.ToItem(item);
            var result = DB.Inventory.Update(item).ToStandardResult("Updated");
            if(result.Error)
                JsonStandardResult(result);

            return Json(new
            {
                HasError = result.Error,
                Message = result.Message,
                Item = new
                {
                    Name = item.Name,
                    Code = item.Code
                }
            });
        }

        private bool CheckItemEditConditions(ItemEditModel model, out string message)
        {
            message = string.Empty;

            var duplicate = DB.Inventory.GetByName(model.Name);
            if(duplicate != null && duplicate.Id != model.Id)
            {
                message = "Another item uses same name";
                return false;
            }

            duplicate = DB.Inventory.GetByCode(model.Code);
            if (duplicate != null && duplicate.Id != model.Id)
            {
                message = "Another item uses same code";
                return false;
            }

            return true;
        }

        #endregion

        #region DELETE ITEM

        public JsonResult DeleteItem(string id)
        {
            if (!CanWrite())
                return JsonResultAccessDenied();

            var item = DB.Inventory.GetById(id);
            if (item == null)
                return JsonResultInvalidData("Item not found!");

            var message = string.Empty;
            if (!CheckPreconditionsForDeletingItem(item, out message))
                return JsonResultInvalidData(message);

            var result = DB.Inventory.Remove(item).ToStandardResult("Deleted");
            if(!result.Error)
            {
                DB.InventoryHistory.RemoveByItem(item);
            }

            return JsonStandardResult(result);
        }

        private bool CheckPreconditionsForDeletingItem(Item item, out string message)
        {
            message = string.Empty;

            var consumed = DB.InventoryHistory.Get().FirstOrDefault(x => x.ItemId == item.Id &&
                                                                         x.Type == ItemHistory.TransactionType.Consumed);

            if(consumed != null)
            {
                message = "Sorry, an item that's been consumed can't be deleted";
                return false;
            }

            return true;
        }

        #endregion




        #region READ STOCK

        public ActionResult GetStock(string id)
        {
            var settings = new ListSettingsStock { ItemId = id };
            var model = GetStockList(settings);
            return PartialView("_ListStockContainer", model);
        }

        public ActionResult ListStock(ListSettingsStock settings)
        {
            var model = GetStockList(settings);
            var html = RenderPartialViewToString("_ListStock", model);
            return Json(new
            {
                HasError = false,
                Message = string.Empty,
                Html = html
            });
        }

        private StockListModel GetStockList(ListSettingsStock settings = null)
        {
            settings = settings ?? new ListSettingsStock();
            var count = 0;
            var item = DB.Inventory.GetById(settings.ItemId);
            var list = new List<StockModel>();

            if (item != null)
            {
                list = DB.Inventory.GetStockList(item, settings, out count)
                                   .Select(x => new StockModel(x))
                                   .ToList();
            }

            return new StockListModel
            {
                StockingUnit = item.StockingUnit,
                Batches = list,
                MoreExist = !(list.Count < settings.Take),
                Skip = settings.Skip + list.Count
            };
        }

        #endregion

        #region CREATE STOCK

        public ActionResult AddStock(string id)
        {
            if (!CanWrite())
                return PartialViewAccessDenied();

            var item = DB.Inventory.GetById(id);
            if (item == null)
                return PartialViewNotFound();

            var model = new StockAddModel(item);
            return PartialView("_AddStock", model);
        }

        [HttpPost]
        public ActionResult AddStock(StockAddModel model)
        {
            if (!CanWrite())
                return JsonResultAccessDenied();

            if (!ModelState.IsValid)
                return JsonResultInvalidData();

            var item = DB.Inventory.GetById(model.ItemId);
            if (item == null)
                return JsonResultInvalidData("Item not found!");

            var message = string.Empty;
            if (!CheckStockAddConditions(model, item, out message))
                return JsonResultInvalidData(message);

            var batch = model.ToBatch();
            item.Stock.AddBatch(batch);
            var result = DB.Inventory.Update(item).ToStandardResult("Stock added");
            if (!result.Error)
            {
                DB.InventoryHistory.Add(model.ToItemHistory(item, batch));
            }

            return JsonStandardResult(result);
        }

        private bool CheckStockAddConditions(StockAddModel model, Item item, out string message)
        {
            message = string.Empty;

            var nameUpper = model.BatchName.ToUpper();
            var duplicate = item.Stock.Batches.FirstOrDefault(x => x.Name.ToUpper() == nameUpper);
            if (duplicate != null)
            {
                message = "Another batch in this item uses same name";
                return false;
            }

            return true;
        }

        #endregion

        #region CONSUME STOCK

        public ActionResult ConsumeStock(string id)
        {
            if (!CanWrite())
                return PartialViewAccessDenied();

            var item = DB.Inventory.GetById(id);
            if (item == null)
                return PartialViewNotFound();

            var model = new StockConsumeModel(item);
            return PartialView("_ConsumeStock", model);
        }

        [HttpPost]
        public ActionResult ConsumeStock(StockConsumeModel model)
        {
            if (!CanWrite())
                return JsonResultAccessDenied();

            if (!ModelState.IsValid)
                return JsonResultInvalidData();

            Item item = null;
            ItemBatch batch = null;
            var message = string.Empty;

            if (!CheckStockConsumeConditions(model, out item, out batch, out message))
                return JsonResultInvalidData(message);

            item.Stock.Consume(batch, model.Quantity.Value);
            var result = DB.Inventory.Update(item).ToStandardResult("Stock consumed");
            if (!result.Error)
            {
                DB.InventoryHistory.Add(model.ToItemHistory(item, batch));
            }

            return JsonStandardResult(result);
        }

        private bool CheckStockConsumeConditions(StockConsumeModel model, out Item item, out ItemBatch batch, out string message)
        {
            message = string.Empty;
            item = null;
            batch = null;

            item = DB.Inventory.GetById(model.ItemId);
            if (item == null)
            {
                message = "Item not found";
                return false;
            }

            batch = item.Stock.Batches.FirstOrDefault(x => x.Id == model.BatchId);
            if (batch == null)
            {
                message = "Batch not found";
                return false;
            }
            
            var nowInZone = DateTimeHelper.NowInZone(Practice.Timezone);
            if(batch.Expiry < nowInZone)
            {
                message = "Sorry, batch has expired";
                return false;
            }

            if(batch.Available < model.Quantity)
            {
                message = string.Format("Sorry, only {0} {1} available", batch.Available, item.StockingUnit);
                return false;
            }

            return true;
        }

        #endregion

        #region DELETE STOCK

        public JsonResult DeleteBatch(string itemId, string batchId)
        {
            if (!CanWrite())
                return JsonResultAccessDenied();

            Item item = null;
            ItemBatch batch = null;
            var message = string.Empty;
            if (!CheckPreconditionsForDeletingBatch(itemId, batchId, out item, out batch, out message))
                return JsonResultInvalidData(message);

            item.Stock.RemoveBatch(batch);
            var result = DB.Inventory.Update(item).ToStandardResult("Deleted");
            if (!result.Error)
            {
                DB.InventoryHistory.RemoveByBatch(batch);
            }

            return JsonStandardResult(result);
        }

        private bool CheckPreconditionsForDeletingBatch(string itemId, string batchId, out Item item, out ItemBatch batch, out string message)
        {
            message = string.Empty;
            item = null;
            batch = null;

            item = DB.Inventory.GetById(itemId);
            if (item == null)
            {
                message = "Item not found!";
                return false;
            }

            batch = item.Stock.Batches.FirstOrDefault(x => x.Id == batchId);
            if(batch == null)
            {
                message = "Batch not found!";
                return false;
            }

            var consumed = DB.InventoryHistory.Get()
                             .FirstOrDefault(x => x.BatchId == batchId &&
                                                  x.Type == ItemHistory.TransactionType.Consumed);

            if (consumed != null)
            {
                message = "Sorry, a batch that's been consumed can't be deleted";
                return false;
            }

            return true;
        }

        #endregion



        #region READ HISTORY

        public ActionResult GetHistory(string id)
        {
            var settings = new ListSettingsItemHistory { ItemId = id };
            var model = GetHistoryList(settings);
            return PartialView("_ListHistoryContainer", model);
        }

        public ActionResult ListHistory(ListSettingsItemHistory settings)
        {
            var model = GetHistoryList(settings);
            var html = RenderPartialViewToString("_ListHistory", model);
            return Json(new
            {
                HasError = false,
                Message = string.Empty,
                Html = html
            });
        }

        private ItemHistoryListModel GetHistoryList(ListSettingsItemHistory settings = null)
        {
            settings = settings ?? new ListSettingsItemHistory();
            var count = 0;
            var item = DB.Inventory.GetById(settings.ItemId);
            var list = new List<ItemHistoryModel>();

            if (item != null)
            {
                var history = DB.InventoryHistory.GetList(settings, out count).ToList();
                var patientIds = history.Select(x => x.PatientId).Distinct().ToList();
                var doctorIds = history.Select(x => x.DoctorId).Distinct().ToList();
                var doctors = DB.Staffs.Get<Doctor>().Where(x => doctorIds.Contains(x.Id)).ToList();
                var patients = DB.Patients.Get(new string[] { "Name" }).Where(x => patientIds.Contains(x.Id)).ToList();

                list = history.Select(x => new ItemHistoryModel(x, item, doctors, patients))
                              .ToList();
            }

            return new ItemHistoryListModel
            {
                StockingUnit = item.StockingUnit,
                Transactions = list,
                MoreExist = !(list.Count < settings.Take),
                Skip = settings.Skip + list.Count
            };
        }

        #endregion

        #region REVERSE HISTORY

        public JsonResult Reverse(string id)
        {
            if (!CanWrite())
                return JsonResultAccessDenied();

            var history = DB.InventoryHistory.GetById(id);
            if (history == null)
                return JsonResultInvalidData("Record not found!");

            Item item = null;
            ItemBatch batch = null;
            var message = string.Empty;
            if (!CheckPreconditionsForReversingHistory(history, out item, out batch, out message))
                return JsonResultInvalidData(message);

            if (ReverseHistory(history, item, batch, out message))
            {
                var result = DB.InventoryHistory.Remove(history).ToStandardResult("Reversed");
                return Json(new
                {
                    HasError = result.Error,
                    Message = result.Message,
                    HistoryId = history.Id
                });
            }

            return JsonResultInvalidData(message);
        }

        private bool ReverseHistory(ItemHistory history, Item item, ItemBatch batch, out string message)
        {
            message = string.Empty;
            if (history.Type == ItemHistory.TransactionType.Added)
            {
                item.Stock.RemoveBatch(batch);
            }
            else if(history.Type == ItemHistory.TransactionType.Consumed)
            {
                item.Stock.Available += history.Quantity;
                batch.Available += history.Quantity;
            }

            return !DB.Inventory.Update(item).ToStandardResult().Error;
        }

        private bool CheckPreconditionsForReversingHistory(ItemHistory history, out Item item, out ItemBatch batch, out string message)
        {
            message = string.Empty;
            item = null;
            batch = null;

            item = DB.Inventory.GetById(history.ItemId);
            if (item == null)
            {
                message = "Item not found!";
                return false;
            }

            batch = item.Stock.Batches.FirstOrDefault(x => x.Id == history.BatchId);
            if (batch == null)
            {
                message = "Batch not found!";
                return false;
            }

            if (batch.HasExpired)
            {
                message = "Sorry, cannot reverse history for an expired batch";
                return false;
            }

            //if trying to reverse an added transaction...
            //... only allow if there's no consumption record
            if (history.Type == ItemHistory.TransactionType.Added)
            {
                var consumed = DB.InventoryHistory.Get()
                                 .FirstOrDefault(x => x.BatchId == history.BatchId &&
                                                      x.Type == ItemHistory.TransactionType.Consumed);

                if (consumed != null)
                {
                    message = "Sorry, cannot reverse because this batch has been consumed";
                    return false;
                }
            }

            return true;
        }

        #endregion



        #region MEDICINES

        public JsonResult GetMedicines()
        {
            var html = RenderPartialViewToString("_GetMedicines", new GetMedicinesModel());
            return Json(new
            {
                HasError = false,
                Message = string.Empty,
                Html = html
            });
        }

        public ActionResult IssueMedicine(string med, string app)
        {
            if (!CanWriteAppointments)
                return ViewAccessDenied();

            var medicine = DB.Inventory.GetById(med);
            if (medicine == null)
                return ViewNotFound("Medicine not found!");

            var appointment = DB.Appointments.GetById(app);
            if (appointment == null)
                return ViewNotFound("Appointment not found!");

            return View(new IssueMedicineModel(medicine, appointment));
        }

        [HttpPost]
        public ActionResult IssueMedicine(IssueMedicineModel model)
        {
            if (!CanWrite())
                return JsonResultAccessDenied();

            model.Type = "Sales";
            ModelState.Remove("Type");
            if (!ModelState.IsValid)
                return JsonResultInvalidData();

            Item item = null;
            ItemBatch batch = null;
            Appointment appointment = null;
            var message = string.Empty;

            if (!CheckStockConsumeConditions(model, out item, out batch, out message) ||
                !CheckIssueMedicineConditions(model, out appointment, out message))
                return JsonResultInvalidData(message);

            item.Stock.Consume(batch, model.Quantity.Value);
            var result = DB.Inventory.Update(item).ToStandardResult("Medicine Issued");
            if (!result.Error)
            {
                DB.InventoryHistory.Add(model.ToItemHistory(item, batch, appointment));
            }

            return JsonStandardResult(result);
        }

        private bool CheckIssueMedicineConditions(IssueMedicineModel model, out Appointment appointment, out string message)
        {
            message = string.Empty;
            appointment = DB.Appointments.GetById(model.AppointmentId);
            if(appointment == null)
            {
                message = "Appointment not found!";
                return false;
            }

            return true;
        }

        #endregion




        private bool CanRead()
        {
            return Privileges.InventoryRead;
        }

        private bool CanWrite()
        {
            return CanRead() && Privileges.InventoryWrite;
        }
    }
}