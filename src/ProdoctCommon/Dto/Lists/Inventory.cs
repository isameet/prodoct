﻿
namespace ProdoctCommon.Dto
{
    public class ListSettingsItems : ListSettingsDefault
    {
        public string StockLevel { get; set; }

        public ListSettingsItems()
        {
        }
    }

    public class ListSettingsStock : ListSettingsDefault
    {
        public string ItemId { get; set; }

        public ListSettingsStock()
        {
        }
    }

    public class ListSettingsItemHistory : ListSettingsDefault
    {
        public string ItemId { get; set; }

        public ListSettingsItemHistory()
        {
        }
    }
}
