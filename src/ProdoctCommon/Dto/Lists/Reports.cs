﻿
using System;

namespace ProdoctCommon.Dto
{
    public class ListSettingsReport : ListSettingsDefault
    {
        public DateTime Start { get; set; }
        public DateTime End { get; set; }

        public ListSettingsReport()
        {
        }
    }
}
