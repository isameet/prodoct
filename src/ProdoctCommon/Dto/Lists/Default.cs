﻿using System;

namespace ProdoctCommon.Dto
{
    public class ListSettingsDefault
    {
        public string SortBy { get; set; }
        public string SearchTerm { get; set; }
        public int Skip { get; set; }
        public int Take { get; set; }

        public ListSettingsDefault()
        {
            Take = 20;
        }
    }
}
