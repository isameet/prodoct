﻿using NodaTime;
using System;

namespace ProdoctCommon.Extensions
{
    public static class DateTimeExtensions
    {
        public static DateTime ToUtc(this DateTime dateTime)
        {
            return dateTime.ToUniversalTime();

            //return new DateTime(dateTime.Ticks, DateTimeKind.Utc);
        }

        public static DateTime InTimezone(this DateTime date, string timezone)
        {
            if (date == DateTime.MinValue)
            {
                return new DateTime(date.Ticks, DateTimeKind.Unspecified);
            }

            if (date == DateTime.MaxValue)
            {
                return new DateTime(date.Ticks, DateTimeKind.Unspecified);
            }

            if (string.IsNullOrWhiteSpace(timezone))
            {
                timezone = "UTC";
            }

            if (DateTimeZoneProviders.Tzdb.Ids.Contains(timezone) == false)
            {
                timezone = "UTC";
            }

            if (date.Kind == DateTimeKind.Unspecified)
            {
                date = new DateTime(date.Ticks, DateTimeKind.Utc);
            }

            var tzInfo = DateTimeZoneProviders.Tzdb[timezone];
            return Instant.FromDateTimeUtc(date)
                          .InZone(tzInfo)
                          .LocalDateTime
                          .ToDateTimeUnspecified();
        }

        public static DateTime StartOfWeek(this DateTime date, DayOfWeek startOfWeek)
        {
            int diff = date.DayOfWeek - startOfWeek;
            if (diff < 0)
            {
                diff += 7;
            }

            return date.AddDays(-1 * diff).Date;
        }
    }
}
