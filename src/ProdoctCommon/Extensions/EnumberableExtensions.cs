﻿using System;
using System.Collections.Generic;

namespace ProdoctCommon.Extensions
{
    public static class EnumberableExtensions
    {
        public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
        {
            foreach (var element in source)
            {
                action(element);
            }
        }
    }
}
