﻿using System;

namespace ProdoctCommon.Extensions
{
    public static class DecimalExtensions
    {
        public static string To2Decimals(this decimal value)
        {
            return string.Format("{0:#.##}", value);
        }
    }
}
