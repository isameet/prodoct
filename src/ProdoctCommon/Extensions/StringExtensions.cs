﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProdoctCommon.Extensions
{
    public static class StringExtensions
    {
        public static bool EqualsInsensitive(this string str, string other)
        {
            return string.Equals(str, other, StringComparison.OrdinalIgnoreCase);
        }
    }   
}
