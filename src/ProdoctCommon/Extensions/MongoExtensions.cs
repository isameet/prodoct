﻿using MongoDB.Bson;
using MongoDB.Bson.IO;
using MongoDB.Driver;
using ProdoctCommon.Helpers;

namespace ProdoctCommon.Extensions
{
    public static class MongoExtensions
    {
        public static bool HasExecutedSuccessfully(this WriteConcernResult result)
        {
            return result.HasLastErrorMessage == false;
        }

        public static bool HasError(this WriteConcernResult result)
        {
            return result.HasLastErrorMessage;
        }

        public static bool HasUpdatedOneDocument(this WriteConcernResult result)
        {
            return result.HasLastErrorMessage == false && result.DocumentsAffected == 1 && result.UpdatedExisting;
        }

        public static bool HasInsertedOneDocument(this WriteConcernResult result)
        {
            return result.HasLastErrorMessage == false && result.UpdatedExisting == false;
        }

        public static string ToLogFormat(this WriteConcernResult result)
        {
            var data = new
            {
                Ok = result.Ok,
                HasLastErrorMessage = result.HasLastErrorMessage,
                LastErrorMessage = result.LastErrorMessage,
                ErrorMessage = result.ErrorMessage,
                DocumentsAffected = result.DocumentsAffected
            };
            return data.ToJsonIndented();
        }

        public static string ToJsonIndented<TNominalType>(this TNominalType obj)
        {
            return obj.ToJson(new JsonWriterSettings { Indent = true });
        }

        public static StandardResult ToStandardResult(this WriteConcernResult result, string successMessage = "Successful")
        {
            var error = result.HasError();
            return new StandardResult
            {
                Error = error,
                Message = error ? result.LastErrorMessage : successMessage
            };
        }
    }
}
