﻿using MongoDB.Bson;
using MongoDB.Driver;
using ProdoctCommon.Data;
using ProdoctCommon.Extensions;
using ProdoctCommon.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProdoctCommon.DataMigrations
{
    public abstract class MongoMigration
    {
        protected string MongoServerUrl;
        protected string DbName;
        protected MongoServer Server;
        protected MongoDatabase Database;
        protected ILogger Logger;
        protected abstract string VersionToApplyOnTopOf { get; }
        private string _version;
        internal static string _CollectionName = "SchemaVersions";

        protected MongoMigration(string mongoServerUrl, string dbName)
        {
            MongoServerUrl = mongoServerUrl;
            DbName = dbName;
            Logger = new NLogLogger(GetType());

            InitializeMongoDatabaseInstance();
        }

        public virtual void Execute()
        {
            InitializeMongoDatabaseInstance();

            Console.WriteLine("Checking migration {0}", GetVersion());
            var existingVersions = GetExistingVersions();

            if (existingVersions.Count > 0)
            {
                if (existingVersions.Contains(GetVersion()))
                {
                    Console.WriteLine("\tAlready migrated to version {0}", GetVersion());
                    return;
                }

                var latestVersion = existingVersions.Last();
                if (latestVersion.EqualsInsensitive(VersionToApplyOnTopOf) == false)
                {
                    Console.WriteLine("\tCannot migrate. Expected to apply migration on version {0}. " +
                                      "Intead current version is {1}",
                                      VersionToApplyOnTopOf,
                                      latestVersion);

                    var message = string.Format("Current db version ({0}) is not compatible with {1}", latestVersion, this);
                    throw new InvalidOperationException(message);
                }
            }

            Console.WriteLine("\tMigrating...");
            Migrate();

            UpdateSchemaVersion();
            Console.WriteLine("\tMigrated.");
        }

        public string GetVersion()
        {
            if (string.IsNullOrWhiteSpace(_version))
            {
                _version = GetType().Name.Replace("Migration_", "");
            }
            return _version;
        }

        protected abstract void Migrate();

        protected virtual void UpdateSchemaVersion()
        {
            GetCollection().Insert(new BsonDocument() {{"Version", GetVersion()}});
        }

        protected virtual void InitializeMongoDatabaseInstance()
        {
            if (string.IsNullOrWhiteSpace(MongoServerUrl))
            {
                MongoServerUrl = ProdoctDatabase._MongoServerUrl;
            }
            if (string.IsNullOrWhiteSpace(DbName))
            {
                DbName = ProdoctDatabase._MongoDatabase;
            }

            var client = new MongoClient(MongoServerUrl);
            Server = client.GetServer();
            Database = Server.GetDatabase(DbName);
        }

        private List<string> GetExistingVersions()
        {
            return GetCollection()
                .FindAll()
                .ToList()
                .Select(x => x.GetValue("Version").AsString)
                .OrderBy(x => x)
                .ToList();
        }

        private MongoCollection<BsonDocument> GetCollection()
        {
            return Database.GetCollection(_CollectionName);
        }

        protected virtual void IterateOver<T>(IQueryable<T> source, Action<List<T>> action)
        {
            const int take = 5000;
            var skip = 0;
            while (true)
            {
                var items = source.Skip(skip).Take(take).ToList();
                if (items.Count == 0)
                {
                    break;
                }

                action(items);
                skip += take;
            }
        }

        public override string ToString()
        {
            return string.Format("Migration -> From {0} to {1}", VersionToApplyOnTopOf, GetVersion());
        }
    }
}