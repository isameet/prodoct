﻿using MongoDB.Bson;
using MongoDB.Driver;
using ProdoctCommon.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProdoctCommon.DataMigrations
{
    public class MigrationExecutor
    {
        private const string _thresholdToDetectOldMigrations = "2013_04_19_2359";
        private readonly string _mongoServerUrl;
        private readonly string _dbName;
        private MongoServer _server;
        private MongoDatabase _database;

        public MigrationExecutor()
        {
            InitializeMongoDatabaseInstance();
        }
        
        public MigrationExecutor(string mongoServerUrl, string dbName)
        {
            _mongoServerUrl = mongoServerUrl;
            _dbName = dbName;
            
            InitializeMongoDatabaseInstance();
        }

        public void ExecuteAll()
        {
            DeleteOldMigrationInfo();

            var migrations = typeof(MongoMigration).Assembly
                                                    .GetExportedTypes()
                                                    .Where(x => x.BaseType == typeof(MongoMigration))
                                                    .OrderBy(x => x.Name)
                                                    .ToList();
            migrations.ForEach(x =>
            {
                var migration = Activator.CreateInstance(x,
                                                         _mongoServerUrl,
                                                         _dbName) as MongoMigration;
                if (migration != null)
                {
                    migration.Execute();
                }
            });
        }

        private void DeleteOldMigrationInfo()
        {
            var existingVersions = GetExistingVersions();
            if (existingVersions.Any(x => string.Compare(x,
                                                         _thresholdToDetectOldMigrations,
                                                         StringComparison.Ordinal) < 0))
            {
                GetCollection().RemoveAll();
                GetCollection().Insert(new BsonDocument() { { "Version", _thresholdToDetectOldMigrations } });
            }
        }

        private List<string> GetExistingVersions()
        {
            return GetCollection()
                .FindAll()
                .ToList()
                .Select(x => x.GetValue("Version").AsString)
                .OrderBy(x => x)
                .ToList();
        }

        private MongoCollection<BsonDocument> GetCollection()
        {
            return _database.GetCollection(MongoMigration._CollectionName);
        }

        private void InitializeMongoDatabaseInstance()
        {
            var serverUrl = string.IsNullOrWhiteSpace(_mongoServerUrl) ?
                            ProdoctDatabase._MongoServerUrl :
                            _mongoServerUrl;

            var dbName = string.IsNullOrWhiteSpace(_dbName) ?
                         ProdoctDatabase._MongoDatabase :
                         _dbName;

            _server = new MongoClient(serverUrl).GetServer();
            _database = _server.GetDatabase(dbName);
        }
    }
}
