﻿using ProdoctCommon.Data;
using ProdoctCommon.Helpers;
using System;
using System.Linq;

namespace ProdoctCommon.DataMigrations.Migrations
{
    public class Migration_2015_02_20_1415 : MongoMigration
    {
        protected override string VersionToApplyOnTopOf
        {
            get { return "2015_01_01_0000"; }
        }

        public Migration_2015_02_20_1415(string mongoServerUrl, string dbName) : base(mongoServerUrl, dbName)
        {
        }

        protected override void Migrate()
        {
            IntroduceFinanceModule();
        }

        private void IntroduceFinanceModule()
        {
            Console.WriteLine("------------------------------------------------");
            Console.WriteLine("Dummy Migration");
            Console.WriteLine("------------------------------------------------");

            var db = new ProdoctDatabase(new DatabaseContext());
            var practices = db.Practices.Get().ToList();
            var i = 1;

            foreach(var practice in practices)
            {
                db.SetContext(new DatabaseContext { PracticeId = practice.Id });
            }
        }
    }
}
