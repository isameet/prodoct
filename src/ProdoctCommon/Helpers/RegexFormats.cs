﻿namespace ProdoctCommon.Helpers
{
    public static class RegexFormats
    {
        public const string EmailCaseInsensitive = "[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?";

        //reference - later half of the code sample in http://msdn.microsoft.com/en-us/library/01escwtf.aspx
        public const string StricterEmailCaseInsensitive = @"^(?("")(""[^""]+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]*\.)+[a-zA-Z0-9]{2,17}))$";

        public const string MobileNumberIndian = @"^([\+\s]?91[\-\s]?)?[789]\d{9}$";
        public const string PhoneNumberE123 = @"^\+(?:[0-9]\s?){6,14}[0-9]$";
        public const string Time12Hr = @"^(0?[1-9]|1[0-2]):([0-5][0-9]):([0-5][0-9]) (([a|A]|[p|P])[m|M])$";

        public const string HexColor = @"^#(?:[0-9a-fA-F]{3}){1,2}$";


        public const string Code16 = @"^[0-9a-zA-Z]{4}\-[0-9a-zA-Z]{4}\-[0-9a-zA-Z]{4}\-[0-9a-zA-Z]{4}$";
    }
}
