﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using ProdoctCommon.Data;
using ProdoctCommon.Logging;
using System;
using System.Collections.Generic;

namespace ProdoctCommon.Helpers
{
    public class AppContext
    {
        public ProdoctDatabase DB { get; set; }
        public string PracticeId { get; set; }
        public string UserId { get; set; }
        public string StaffId { get; set; }
        
        

        
        public ILogger Log
        {
            get
            {
                if (_log == null)
                    _log = new NLogLogger(GetType());

                return _log;
            }
        }
        public Practice Practice 
        {  
            get
            {
                if(_practice == null)
                    _practice = DB.Practices.GetById(PracticeId);

                return _practice;
            }
        }
        public User User 
        {
            get
            {
                if (_user == null)
                    _user = DB.Users.GetById(UserId);

                return _user;
            }

            set
            {
                _user = value;
                UserId = value == null ? null : _user.Id;
            }
        }
        public Staff Staff 
        {
            get
            {
                if (_staff == null)
                    _staff = DB.Staffs.GetById(StaffId);

                if (_staff == null)
                {
                    _staff = DB.Staffs.GetByUserId(UserId);
                    StaffId = _staff == null ? null : _staff.Id;
                }

                return _staff;
            }
        }

        
        
        
        public AppContext()
        {
        }
        public void SetLogger(ILogger logger)
        {
            _log = logger;
        }




        public bool IsUnauthorized()
        {
            if (User == null || Staff == null)
                return true;

            return !User.IsVerified ||
                   string.IsNullOrWhiteSpace(PracticeId) ||
                   !Staff.AllowLogin ||
                   !Staff.HasAccepted;
        }



        Practice _practice;
        User _user;
        Staff _staff;
        ILogger _log;
    }

    public class DatabaseContext
    {
        public string PracticeId { get; set; }

        public DatabaseContext()
        {

        }
    }

    public class SelectListItemDto
    {
        public string Text { get; set; }
        public string Value { get; set; }

        public SelectListItemDto()
        {

        }
    }

    public class SetStaffEmailResult
    {
        public bool Changed { get; set; }
        public bool Error { get; set; }
        public string Message { get; set; }
        public User OldUser { get; set; }
        public User ChangedUser { get; set; }
        public bool NewUserCreated { get; set; }
    }

    public class SetAllowLoginResult
    {
        public bool Changed { get; set; }
        public bool LinkShouldBeSent { get; set; }
        public bool LinkShouldBeDeleted { get; set; }
        public bool Error { get; set; }
        public string Message { get; set; }
    }

    public class SetUserEmailResult
    {
        public bool Changed { get; set; }
        public bool Error { get; set; }
        public string Message { get; set; }
        public List<Staff> StaffsAffected { get; set; }

        public SetUserEmailResult()
        {
            StaffsAffected = new List<Staff>();
        }
    }

    public class StandardResult
    {
        public bool Error { get; set; }
        public string Message { get; set; }

        public StandardResult()
        {
        }

        public StandardResult(bool error, string message)
        {
            Error = error;
            Message = message;
        }
    }

    public class StaffGetUserResult : StandardResult
    {
        public User User { get; set; }
        public bool IsNew { get; set; }
    }

    public class ChartDateAmount
    {
        public DateTime Date { get; set; }
        public decimal Value { get; set; }
    }

    public class ChartObjectIdAmount
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string Label { get; set; }
        public decimal Value { get; set; }
    }

    public class ChartStringAmount
    {
        public string Label { get; set; }
        public decimal Value { get; set; }
    }
}
