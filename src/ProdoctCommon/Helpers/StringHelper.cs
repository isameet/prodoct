﻿
namespace ProdoctCommon.Helpers
{
    public static class StringHelper
    {
        public static bool ContainsDigitsOnly(string str)
        {
            foreach (char c in str)
            {
                if (c < '0' || c > '9')
                    return false;
            }

            return true;
        }

        public static string FirstLetterToUpper(string str)
        {
            if (string.IsNullOrWhiteSpace(str))
                return null;

            if (str.Length > 1)
                return char.ToUpper(str[0]) + str.Substring(1);

            return str.ToUpper();
        }
    }
}
