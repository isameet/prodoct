﻿using NodaTime;
using NodaTime.TimeZones;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace ProdoctCommon.Helpers
{
    public static class DateTimeHelper
    {
        public static Func<DateTime> UtcNow = () => DateTime.UtcNow;

        public static DateTime FromDateTimePicker(string dateString)
        {
            DateTime date = DateTime.MinValue;
            var didItSucceed = TryParse(dateString, DateTimeFormats.DateTimePicker, out date);
            return date;
        }

        public static DateTime FromDatePicker(string dateString)
        {
            DateTime date = DateTime.MinValue;
            var didItSucceed = TryParse(dateString, DateTimeFormats.DatePicker, out date);
            return date;
        }

        public static DateTime FromDateRangePicker(string dateString)
        {
            DateTime date = DateTime.MinValue;
            var didItSucceed = TryParse(dateString, DateTimeFormats.DateRangePicker, out date);
            return date;
        }

        public static DateTime FromCalendarRequestModel(string dateString)
        {
            DateTime date = DateTime.MinValue;
            var didItSucceed = TryParse(dateString, DateTimeFormats.CalendarRequestModel, out date);
            return date;
        }

        public static List<SelectListItemDto> GetTimezones()
        {
            var tzdb = DateTimeZoneProviders.Tzdb;
            var now = SystemClock.Instance.Now;
            var locations = TzdbDateTimeZoneSource.Default.ZoneLocations;
            var values = locations
                .Select(x =>
                {
                    var zoneId = x.ZoneId;
                    var offset = tzdb[zoneId].GetZoneInterval(now).StandardOffset.ToTimeSpan();
                    string displayText;
                    if (offset < TimeSpan.Zero)
                    {
                        displayText = string.Format("UTC-{0:00}:{1:00} - {3} ({2})", Math.Abs(offset.Hours),
                                                    Math.Abs(offset.Minutes),
                                                    zoneId, x.CountryName);
                    }
                    else
                    {
                        displayText = string.Format("UTC+{0:00}:{1:00} - {3} ({2})", offset.Hours,
                                                    offset.Minutes, zoneId, x.CountryName);
                    }

                    return new { Value = zoneId, Text = displayText, Offset = offset, Country = x.CountryName };
                })
                .OrderBy(x => x.Offset).ThenBy(x => x.Country)
                .Select(x => new SelectListItemDto { Value = x.Value, Text = x.Text })
                .ToList();

            return values;
        }

        public static DateTime NowInZone(string timezone)
        {
            return InLocal(UtcNow(), timezone);
        }

        public static DateTime InLocal(DateTime utcDate, string timezone)
        {
            if (utcDate == DateTime.MinValue)
            {
                return new DateTime(utcDate.Ticks, DateTimeKind.Unspecified);
            }

            if (utcDate == DateTime.MaxValue)
            {
                return new DateTime(utcDate.Ticks, DateTimeKind.Unspecified);
            }

            if (string.IsNullOrWhiteSpace(timezone))
            {
                timezone = "UTC";
            }

            if (DateTimeZoneProviders.Tzdb.Ids.Contains(timezone) == false)
            {
                timezone = "UTC";
            }

            if (utcDate.Kind == DateTimeKind.Unspecified)
            {
                utcDate = new DateTime(utcDate.Ticks, DateTimeKind.Utc);
            }

            var tzInfo = DateTimeZoneProviders.Tzdb[timezone];
            return Instant.FromDateTimeUtc(utcDate)
                          .InZone(tzInfo)
                          .LocalDateTime
                          .ToDateTimeUnspecified();
        }

        private static bool TryParse(string dateString, string format, out DateTime date)
        {
            return DateTime.TryParseExact(dateString,
                                   format,
                                   CultureInfo.InvariantCulture,
                                   DateTimeStyles.None,
                                   out date);
        }
    }

    public static class DateTimeFormats
    {
        public const string DateTimePicker = "d MMM, yyyy - h:mm tt";
        public const string DateRangePicker = "d MMM, yy";
        public const string DatePicker = "d MMMM, yyyy";
        public const string ISO8601 = "yyyy-MM-ddTHH:mm:ssZ";
        public const string CalendarRequestModel = "yyyy-MM-dd";
        public const string _8Mar87 = "d MMM, yy";
        public const string _8Mar = "d MMM";
        public const string _March1987 = "MMMM, yyyy";
        public const string _Mar87 = "MMM, yy";
    }
}
