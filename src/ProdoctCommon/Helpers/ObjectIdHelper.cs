﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProdoctCommon.Helpers
{
    public static class ObjectIdHelper
    {
        public static ObjectId GenerateNewId()
        {
            return ObjectId.GenerateNewId();
        }

        public static string GenerateNewIdString()
        {
            return GenerateNewId().ToString();
        }

        public static bool IsObjectId(string id)
        {
            ObjectId objId;
            return ObjectId.TryParse(id, out objId);
        }
    }
}
