﻿using Mandrill;
using ProdoctCommon.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProdoctCommon.Email
{
    public class Mandrill
    {
        public Mandrill(ILogger logger)
        {
            _log = logger;
        }

        public EmailSenderResult SendChangePassword(string recipient, Dictionary<string, string> globals)
        {
            var recipients = new List<EmailAddress> { new EmailAddress(recipient) };
            return SendMandrillEmail(recipients, Template.ChangePassword, globals).FirstOrDefault();
        }
        public EmailSenderResult SendUserEmailVerify(string recipient, Dictionary<string, string> globals)
        {
            var recipients = new List<EmailAddress> { new EmailAddress(recipient) };
            return SendMandrillEmail(recipients, Template.UserEmailVerify, globals).FirstOrDefault();
        }

        public EmailSenderResult SendStaffInvitation(string recipient, Dictionary<string, string> globals)
        {
            var recipients = new List<EmailAddress> { new EmailAddress(recipient) };
            return SendMandrillEmail(recipients, Template.StaffInvitation, globals).FirstOrDefault();
        }

        private List<EmailSenderResult> SendMandrillEmail(List<EmailAddress> recipients, string template, Dictionary<string, string> globals)
        {
            MandrillApi mandrill = new MandrillApi(_apiKey);
            var email = new EmailMessage
            {
                to = recipients,
                from_email = "no_reply@prodoc.in",
                from_name = "prodoc",
            };

            foreach (var global in globals)
            {
                email.AddGlobalVariable(global.Key, global.Value);
            }

            var results = new List<EmailSenderResult>();

            try
            {
                var responses = mandrill.SendMessage(email, template, null);
                foreach(var response in responses)
                {
                    results.Add(ToEmailSenderResult(response));
                }
            }
            catch (Exception ex)
            {
                _log.Error("Error sending email through Mandrill", ex);
            }

            return results;
        }

        private EmailSenderResult ToEmailSenderResult(EmailResult x)
        {
            return new EmailSenderResult
            {
                Error = x.Status == EmailResultStatus.Invalid || x.Status == EmailResultStatus.Rejected,
                Message = ToMessage(x)
            };
        }

        private string ToMessage(EmailResult x)
        {
            return string.Format("Id: {0}\nStatus: {1}\nReject Reason (if any): {2}", 
                                 x.Id, x.Status, x.RejectReason);
        }

        private static class Template
        {
            public static readonly string StaffInvitation = "staff-invitation";
            public static readonly string UserEmailVerify = "user-email-verify";
            public static readonly string ChangePassword = "change-password";
        }

        private string _apiKey = "VbzbcFB61EnYv-CyvYvu7A";
        private ILogger _log;
    }
}
