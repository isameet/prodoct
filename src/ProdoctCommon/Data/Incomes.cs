﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ProdoctCommon.Dto;
using ProdoctCommon.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProdoctCommon.Data
{
    public class Incomes : PracticeEntityDataAccess<Income>
    {
        public Incomes(MongoDatabase database)
            : base(database, "Incomes")
        {
        }

        #region list

        public ListIncomeFeesDto GetList(ListSettingsReport settings, out int count)
        {
            if (settings == null)
                settings = new ListSettingsReport();

            var sortBy = GetSortBy(settings.SortBy);
            var query = GetQuery(settings);
            var incomes = GetCursor(query, sortBy, null, out count, settings.Skip, settings.Take).ToList();
            var feesIds = incomes.Where(x => x.Category == "Fees").Select(x => x.Id).ToList();
            List<Fees> fees = new List<Fees>();
            if (feesIds != null && feesIds.Count > 0)
                fees = Get<Fees>().Where(x => feesIds.Contains(x.Id)).ToList();

            return new ListIncomeFeesDto
            {
                Incomes = incomes,
                Fees = fees
            };
        }

        private IMongoQuery GetQuery(ListSettingsReport settings)
        {
            var wheres = GetStandardWheres();

            wheres.AddRange(new List<IMongoQuery>
                    {
                        Query<Income>.Where(x => x.Date >= settings.Start),
                        Query<Income>.Where(x => x.Date <= settings.End)
                    });

            var searchTerm = settings.SearchTerm;
            if (!string.IsNullOrWhiteSpace(searchTerm))
            {
                // ...
            }

            var query = Query.And(wheres);
            return query;
        }

        private IMongoSortBy GetSortBy(string sort)
        {
            switch (sort)
            {
                default:
                    return SortBy.Descending("Date");
            }
        }

        #endregion

        #region charts

        public List<ChartDateAmount> GetAllIncomeChart(DateTime start, DateTime end)
        {
            var match = new BsonDocument 
            {{ "$match", new BsonDocument 
                {   
                    { "PracticeId", new BsonDocument {{ "$eq", ObjectId.Parse(Context.PracticeId) }} },
                    { "Date", new BsonDocument { {"$gte", start}, {"$lte", end} } }
                }
            }};

            var group = new BsonDocument("$group",
                new BsonDocument
                {
                    { "_id", new BsonDocument {{ "Date", "$Date" }} },
                    { "income", new BsonDocument {{"$sum", "$Amount" }} }
                });

            var sort = new BsonDocument("$sort", new BsonDocument("_id", 1));
            var project = new BsonDocument  {{ "$project", 
                                                new BsonDocument 
                                                    { 
                                                        {"_id", 0}, 
                                                        {"Date","$_id.Date"},
                                                        {"Value", "$income"}, 
                                                    }
                                            }};

            var args = new AggregateArgs
            {
                Pipeline = new[] { match, group, sort, project }
            };

            var result = GetCollection().Aggregate(args)
                                        .Select(x => BsonSerializer.Deserialize<ChartDateAmount>(x))
                                        .ToList();

            return result;
        }

        public List<ChartObjectIdAmount> GetPerDoctorChart(DateTime start, DateTime end)
        {
            var match = new BsonDocument 
            {{ "$match", new BsonDocument 
                {   
                    { "PracticeId", new BsonDocument {{ "$eq", ObjectId.Parse(Context.PracticeId) }} },
                    { "Category", new BsonDocument {{ "$eq", "Fees" }} },
                    { "Date", new BsonDocument { {"$gte", start}, {"$lte", end} } }
                }
            }};

            var group = new BsonDocument("$group",
                new BsonDocument
                {
                    { "_id", new BsonDocument {{ "DoctorId", "$DoctorId" }} },
                    { "income", new BsonDocument {{"$sum", "$Amount" }} }
                });

            var sort = new BsonDocument("$sort", new BsonDocument("_id", 1));
            var project = new BsonDocument  {{ "$project", 
                                                new BsonDocument 
                                                    { 
                                                        {"_id", 0}, 
                                                        {"Label","$_id.DoctorId"},
                                                        {"Value", "$income"}, 
                                                    }
                                            }};

            var args = new AggregateArgs
            {
                Pipeline = new[] { match, group, sort, project }
            };

            var result = GetCollection().Aggregate(args)
                                        .Select(x => BsonSerializer.Deserialize<ChartObjectIdAmount>(x))
                                        .ToList();

            return result;
        }

        public List<ChartStringAmount> GetByCategoryChart(DateTime start, DateTime end)
        {
            var match = new BsonDocument 
            {{ "$match", new BsonDocument 
                {   
                    { "PracticeId", new BsonDocument {{ "$eq", ObjectId.Parse(Context.PracticeId) }} },
                    { "Date", new BsonDocument { {"$gte", start}, {"$lte", end} } }
                }
            }};

            var group = new BsonDocument("$group",
                new BsonDocument
                {
                    { "_id", new BsonDocument {{ "Category", "$Category" }} },
                    { "income", new BsonDocument {{"$sum", "$Amount" }} }
                });

            var sort = new BsonDocument("$sort", new BsonDocument("_id", 1));
            var project = new BsonDocument  {{ "$project", 
                                                new BsonDocument 
                                                    { 
                                                        {"_id", 0}, 
                                                        {"Label","$_id.Category"},
                                                        {"Value", "$income"}, 
                                                    }
                                            }};

            var args = new AggregateArgs
            {
                Pipeline = new[] { match, group, sort, project }
            };

            var result = GetCollection().Aggregate(args)
                                        .Select(x => BsonSerializer.Deserialize<ChartStringAmount>(x))
                                        .ToList();

            return result;
        }

        #endregion
    }

    public class ListIncomeFeesDto
    {
        public List<Income> Incomes { get; set; }
        public List<Fees> Fees { get; set; }
        public ListIncomeFeesDto()
        {
            Incomes = new List<Income>();
            Fees = new List<Fees>();
        }
    }
}
