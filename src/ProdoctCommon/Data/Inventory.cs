﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ProdoctCommon.Dto;
using ProdoctCommon.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProdoctCommon.Data
{
    public class Inventory : PracticeEntityDataAccess<Item>
    {
        public Inventory(MongoDatabase database)
            : base(database, "Inventory")
        {
            
        }

        public Item GetByName(string name)
        {
            if(string.IsNullOrWhiteSpace(name))
                return null;
            
            var nameUpper = name.Trim().ToUpper();
            return Get().FirstOrDefault(x => x.Name.ToUpper() == nameUpper);
        }

        public Item GetByCode(string code)
        {
            if (string.IsNullOrWhiteSpace(code))
                return null;

            var codeUpper = code.Trim().ToUpper();
            return Get().FirstOrDefault(x => x.Code.ToUpper() == codeUpper);
        }

        public IQueryable<Item> GetAvailableMedicines()
        {
            return Get().Where(x => x.Type == Item.ItemType.Drug && x.Stock.Available > 0).OrderBy(x => x.Name);
        }

        #region list

        public IQueryable<Item> GetList(ListSettingsItems settings, out int count)
        {
            var sortBy = GetSortBy(settings.SortBy);
            var query = GetQuery(settings);
            return GetCursor(query, sortBy, null, out count, settings.Skip, settings.Take).AsQueryable();
        }

        private IMongoQuery GetQuery(ListSettingsItems settings)
        {
            var wheres = GetStandardWheres();
            var searchTerm = settings.SearchTerm;

            settings.StockLevel = string.IsNullOrWhiteSpace(settings.StockLevel) ? 
                                  string.Empty : 
                                  settings.StockLevel.ToLower();

            switch (settings.StockLevel)
            {
                case "low":
                    wheres.Add(Query.Where("this.ReorderLevel > this.Stock.Available"));
                    break;
                case "expired":
                    wheres.Add(Query<Item>.Where(x => x.Stock.Expired > 0));
                    break;
                default:
                    break;
            }
            
            if (!string.IsNullOrWhiteSpace(searchTerm))
            {
                var searchTermUpper = searchTerm.Trim().ToUpper();
                var subWheres = new List<IMongoQuery>
                {
                    Query<Item>.Where(x => x.Name.ToUpper().Contains(searchTermUpper)),
                    Query<Item>.Where(x => x.Code.ToUpper().Contains(searchTermUpper))
                };

                wheres.Add(Query.Or(subWheres));
            }

            var query = Query.And(wheres);
            return query;
        }

        private IMongoSortBy GetSortBy(string sort)
        {
            switch (sort)
            {
                default:
                    return SortBy.Ascending("Name");
            }
        }

        #endregion

        #region stock list

        public IEnumerable<ItemBatch> GetStockList(Item item, ListSettingsStock settings, out int count)
        {
            var batches = item.Stock.Batches;
            count = batches.Count;

            var list = batches.OrderByDescending(x => x.Available).Skip(settings.Skip).Take(settings.Take);
            return list;
        }

        #endregion

        public IQueryable<Item> GetExpiringItems()
        {
            var now = DateTime.Now;
            return Get().Where(x => x.Stock.Batches.Any(y => y.Expiry < now && !y.HasExpired));
        }

        public List<ItemBatch> GetExpiringBatches(Item item)
        {
            var now = DateTime.Now;
            return item.Stock.Batches.Where(x => x.Expiry < now && !x.HasExpired).ToList();
        }
    }
}
