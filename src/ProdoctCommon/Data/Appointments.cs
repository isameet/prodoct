﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProdoctCommon.Data
{
    public class Appointments : PracticeEntityDataAccess<Appointment>
    {
        public Appointments(MongoDatabase database)
            : base(database, "Appointments")
        {

        }

        public IQueryable<Appointment> Get(DateTime start, DateTime end, CalendarSettings settings)
        {
            var wheres = GetStandardWheres();
            wheres.Add(Query<Appointment>.GTE(x => x.Date, start));
            wheres.Add(Query<Appointment>.LT(x => x.Date, end));

            if (!settings.ShowCanceledAppointments)
                wheres.Add(Query<Appointment>.EQ(x => x.Status, AppointmentStatus.Confirmed));

            var query = Query.And(wheres);
            return GetCursor(query, SortBy.Null, null).AsQueryable();
        }
    }
}
