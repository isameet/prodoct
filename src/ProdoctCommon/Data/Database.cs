﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;
using ProdoctCommon.Helpers;
using System.Linq;

namespace ProdoctCommon.Data
{
    public class ProdoctDatabase
    {
        public DatabaseContext Context { get; private set; }
        public Appointments Appointments { get; private set; }
        public Patients Patients { get; private set; }
        public Users Users { get; private set; }
        public Practices Practices { get; private set; }
        public Staffs Staffs { get; private set; }
        public Links Links { get; private set; }
        public Incomes Incomes { get; private set; }
        public Expenses Expenses { get; private set; }
        public Inventory Inventory { get; private set; }
        public InventoryHistory InventoryHistory { get; private set; }

        private ProdoctDatabase()
        {
            var client = new MongoClient(_MongoServerUrl);
            var server = client.GetServer();
            _database = server.GetDatabase(_MongoDatabase);
        }

        public ProdoctDatabase(DatabaseContext context) : this()
        {
            Appointments = new Appointments(_database);
            Patients = new Patients(_database);
            Users = new Users(_database);
            Practices = new Practices(_database);
            Staffs = new Staffs(_database);
            Links = new Links(_database);
            Incomes = new Incomes(_database);
            Expenses = new Expenses(_database);
            Inventory = new Inventory(_database);
            InventoryHistory = new InventoryHistory(_database);

            SetContext(context);
        }

        public void SetContext(DatabaseContext context)
        {
            Context = context;
            Appointments.SetContext(context);
            Patients.SetContext(context);
            Users.SetContext(context);
            Practices.SetContext(context);
            Staffs.SetContext(context);
            Links.SetContext(context);
            Incomes.SetContext(context);
            Expenses.SetContext(context);
            Inventory.SetContext(context);
            InventoryHistory.SetContext(context);
        }

        private MongoDatabase _database;
        internal static readonly string _MongoServerUrl = "mongodb://localhost";
        internal static readonly string _MongoDatabase = "ProdoctDb";
    }
}
