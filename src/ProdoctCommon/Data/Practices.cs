﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProdoctCommon.Data
{
    public class Practices : EntityDataAccess<Practice>
    {
        public Practices(MongoDatabase database)
            : base(database, "Practices")
        {

        }

    }
}
