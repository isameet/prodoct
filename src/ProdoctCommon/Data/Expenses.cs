﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ProdoctCommon.Dto;
using ProdoctCommon.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProdoctCommon.Data
{
    public class Expenses : PracticeEntityDataAccess<Expense>
    {
        public Expenses(MongoDatabase database)
            : base(database, "Expenses")
        {
        }

        #region list

        public IQueryable<Expense> GetList(ListSettingsReport settings, out int count)
        {
            if (settings == null)
                settings = new ListSettingsReport();

            var sortBy = GetSortBy(settings.SortBy);
            var query = GetQuery(settings);
            return GetCursor(query, sortBy, null, out count, settings.Skip, settings.Take).AsQueryable();
        }

        private IMongoQuery GetQuery(ListSettingsReport settings)
        {
            var wheres = GetStandardWheres();

            wheres.AddRange(new List<IMongoQuery>
                    {
                        Query<Income>.Where(x => x.Date >= settings.Start),
                        Query<Income>.Where(x => x.Date <= settings.End)
                    });

            var searchTerm = settings.SearchTerm;
            if (!string.IsNullOrWhiteSpace(searchTerm))
            {
                // ...
            }

            var query = Query.And(wheres);
            return query;
        }

        private IMongoSortBy GetSortBy(string sort)
        {
            switch (sort)
            {
                default:
                    return SortBy.Descending("Date");
            }
        }

        #endregion

        #region charts

        public List<ChartDateAmount> GetAllExpensesChart(DateTime start, DateTime end)
        {
            var match = new BsonDocument 
            {{ "$match", new BsonDocument 
                {   
                    { "PracticeId", new BsonDocument {{ "$eq", ObjectId.Parse(Context.PracticeId) }} },
                    { "Date", new BsonDocument { {"$gte", start}, {"$lte", end} } }
                }
            }};

            var group = new BsonDocument("$group",
                new BsonDocument
                {
                    { "_id", new BsonDocument {{ "Date", "$Date" }} },
                    { "expenses", new BsonDocument {{"$sum", "$Amount" }} }
                });

            var sort = new BsonDocument("$sort", new BsonDocument("_id", 1));
            var project = new BsonDocument  {{ "$project", 
                                                new BsonDocument 
                                                    { 
                                                        {"_id", 0}, 
                                                        {"Date","$_id.Date"},
                                                        {"Value", "$expenses"}, 
                                                    }
                                            }};

            var args = new AggregateArgs
            {
                Pipeline = new[] { match, group, sort, project }
            };

            var result = GetCollection().Aggregate(args)
                                        .Select(x => BsonSerializer.Deserialize<ChartDateAmount>(x))
                                        .ToList();

            return result;
        }

        public List<ChartStringAmount> GetByCategoryChart(DateTime start, DateTime end)
        {
            var match = new BsonDocument 
            {{ "$match", new BsonDocument 
                {   
                    { "PracticeId", new BsonDocument {{ "$eq", ObjectId.Parse(Context.PracticeId) }} },
                    { "Date", new BsonDocument { {"$gte", start}, {"$lte", end} } }
                }
            }};

            var group = new BsonDocument("$group",
                new BsonDocument
                {
                    { "_id", new BsonDocument {{ "Category", "$Category" }} },
                    { "expenses", new BsonDocument {{"$sum", "$Amount" }} }
                });

            var sort = new BsonDocument("$sort", new BsonDocument("_id", 1));
            var project = new BsonDocument  {{ "$project", 
                                                new BsonDocument 
                                                    { 
                                                        {"_id", 0}, 
                                                        {"Label","$_id.Category"},
                                                        {"Value", "$expenses"}, 
                                                    }
                                            }};

            var args = new AggregateArgs
            {
                Pipeline = new[] { match, group, sort, project }
            };

            var result = GetCollection().Aggregate(args)
                                        .Select(x => BsonSerializer.Deserialize<ChartStringAmount>(x))
                                        .ToList();

            return result;
        }

        #endregion
    }
}
