﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProdoctCommon.Data
{
    public class Staffs : PracticeEntityDataAccess<Staff>
    {
        public Staffs(MongoDatabase database)
            : base(database, "Staffs")
        {
        }

        public Staff GetByEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                return null;

            email = email.ToLower();
            return Get().FirstOrDefault(x => x.Email.ToLower() == email);
        }

        public Staff GetByUserId(string userId)
        {
            if (string.IsNullOrWhiteSpace(userId))
                return null;

            return Get().FirstOrDefault(x => x.UserId == userId);
        }

        public Staff GetOneActiveAdminOtherThan(string id)
        {
            return Get().FirstOrDefault(x => x.Privileges.IsAdmin && 
                                             x.HasAccepted && 
                                             x.AllowLogin && 
                                             x.Id != id);
        }

        internal List<Staff> GetAllByUserId(string userId)
        {
            return GetCursor(Query<Staff>.EQ(x => x.UserId, userId), SortBy.Null, null).ToList();
        }
    }
}
