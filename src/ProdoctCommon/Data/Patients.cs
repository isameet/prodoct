﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ProdoctCommon.Dto;
using ProdoctCommon.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProdoctCommon.Data
{
    public class Patients : PracticeEntityDataAccess<Patient>
    {
        public Patients(MongoDatabase database)
            : base(database, "Patients")
        {
            
        }

        public IEnumerable<Patient> SearchByNameOrMobile(string query, int limit = 0)
        {
            if (TypingMobileNumber(query))
                return SearchByMobile(query, limit);

            return SearchByName(query, limit);
        }

        public IEnumerable<Patient> SearchByName(string name, int limit = 0)
        {
            var nameUpper = string.IsNullOrWhiteSpace(name) ? string.Empty : name.Trim().ToUpper();
            var wheres = GetStandardWheres();
            wheres.Add(Query<Patient>.Where(x => x.Name.ToUpper().Contains(nameUpper)));

            return Search(Query.And(wheres), limit);
        }

        public IEnumerable<Patient> SearchByMobile(string mobile, int limit = 0)
        {
            var wheres = GetStandardWheres();
            wheres.Add(Query<Patient>.Where(x => x.Mobile.Contains(mobile)));
            return Search(Query.And(wheres), limit);
        }

        private IEnumerable<Patient> Search(IMongoQuery query, int limit)
        {
            return GetCursor(query, 
                             SortBy.Ascending("Name"), 
                             new string[] { "Name", "Mobile", "Locality" }, 
                             skip: 0, take: limit);
        }

        public IQueryable<Patient> GetList(ListSettingsDefault settings, out int count)
        {
            var pastAppointments = false;
            var sortBy = GetSortBy(settings.SortBy, out pastAppointments);
            var query = GetQuery(settings, pastAppointments);
            return GetCursor(query, sortBy, null, out count, settings.Skip, settings.Take).AsQueryable();
        }

        private IMongoQuery GetQuery(ListSettingsDefault settings, bool pastAppointmentsOnly)
        {
            var wheres = GetStandardWheres();
            if (pastAppointmentsOnly)
            {
                var now = DateTime.Now;
                wheres.Add(Query<Patient>.LT(x => x.LastAppointment.Date, now));
            }

            var searchTerm = settings.SearchTerm;
            if (!string.IsNullOrWhiteSpace(searchTerm))
            {
                if (TypingMobileNumber(searchTerm))
                {
                    wheres.Add(Query<Patient>.Where(x => x.Mobile.Contains(searchTerm)));
                }
                else
                {
                    var searchTermUpper = searchTerm.Trim().ToUpper();
                    var subWheres = new List<IMongoQuery>
                    {
                        Query<Patient>.Where(x => x.Name.ToUpper().Contains(searchTermUpper)),
                        Query<Patient>.Where(x => x.Locality.ToUpper().Contains(searchTermUpper))
                    };

                    wheres.Add(Query.Or(subWheres));
                }
            }

            var query = Query.And(wheres);
            return query;
        }

        private bool TypingMobileNumber(string searchTerm)
        {
            if (string.IsNullOrWhiteSpace(searchTerm))
                return false;

            searchTerm = searchTerm.Replace("+", string.Empty);
            if (StringHelper.ContainsDigitsOnly(searchTerm))
                return true;

            return false;
        }

        private IMongoSortBy GetSortBy(string sort, out bool pastAppointments)
        {
            pastAppointments = false;

            switch (sort)
            {
                case "LastAppointment.Date":
                    pastAppointments = true;
                    return SortBy.Descending(sort);

                case "CreatedOn":
                    return SortBy.Descending(sort);

                default:
                    return SortBy.Ascending("Name");
            }
        }
    }
}
