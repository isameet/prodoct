﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ProdoctCommon.Dto;
using ProdoctCommon.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProdoctCommon.Data
{
    public class InventoryHistory : PracticeEntityDataAccess<ItemHistory>
    {
        public InventoryHistory(MongoDatabase database)
            : base(database, "InventoryHistory")
        {
        }

        #region list

        public IQueryable<ItemHistory> GetList(ListSettingsItemHistory settings, out int count)
        {
            var sortBy = GetSortBy(settings.SortBy);
            var query = GetQuery(settings);
            return GetCursor(query, sortBy, null, out count, settings.Skip, settings.Take).AsQueryable();
        }

        private IMongoQuery GetQuery(ListSettingsItemHistory settings)
        {
            var wheres = GetStandardWheres();
            wheres.Add(Query<ItemHistory>.EQ(x => x.ItemId, settings.ItemId));

            //var searchTerm = settings.SearchTerm;
            //if (!string.IsNullOrWhiteSpace(searchTerm))
            //{
            //    var searchTermUpper = searchTerm.Trim().ToUpper();
            //    var subWheres = new List<IMongoQuery>
            //    {
            //        Query<ItemHistory>.Where(x => x.Name.ToUpper().Contains(searchTermUpper))
            //    };

            //    wheres.Add(Query.Or(subWheres));
            //}

            var query = Query.And(wheres);
            return query;
        }

        private IMongoSortBy GetSortBy(string sort)
        {
            switch (sort)
            {
                default:
                    return SortBy.Descending("Date");
            }
        }

        #endregion

        public WriteConcernResult RemoveByItem(Item item)
        {
            return GetCollection().Remove(Query<ItemHistory>.EQ(x => x.ItemId, item.Id));
        }

        public WriteConcernResult RemoveByBatch(ItemBatch batch)
        {
            return GetCollection().Remove(Query<ItemHistory>.EQ(x => x.BatchId, batch.Id));
        }
    }
}
