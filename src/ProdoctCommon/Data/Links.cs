﻿using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProdoctCommon.Data
{
    public class Links : PracticeEntityDataAccess<Link>
    {
        public Links(MongoDatabase database)
            : base(database, "Links")
        {

        }

        public Link GetUnConsumedByEmailAndCode(string email, string code)
        {
            return GetCollection().AsQueryable()
                                  .FirstOrDefault(x => x.Email == email && 
                                                       x.Code == code && 
                                                       x.IsConsumed == false);
        }
    }
}
