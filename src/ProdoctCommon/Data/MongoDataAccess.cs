﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;
using ProdoctCommon.Helpers;
using System.Collections.Generic;
using System.Linq;

namespace ProdoctCommon.Data
{
    public abstract class PracticeEntityDataAccess<T> : EntityDataAccess<T> where T : PracticeEntity
    {
        public PracticeEntityDataAccess(MongoDatabase database, string collectionName)
            : base(database, collectionName)
        {
        }

        #region READ

        public override IQueryable<T> Get()
        {
            var query = base.Get();
            if (!string.IsNullOrWhiteSpace(Context.PracticeId))
                query = query.Where(x => x.PracticeId == Context.PracticeId);

            return query;
        }
        public override MongoCursor<T> Get(string[] fields)
        {
            var cursor = GetCursor(GetStandardQuery(), SortBy.Null, fields);
            return cursor;
        }
        
        public override IQueryable<TOut> Get<TOut>()
        {
            return Get().OfType<TOut>();
        }
        public override MongoCursor<T> Get<TOut>(string[] fields)
        {
            var wheres = GetStandardWheres();
            return Get<TOut>(fields, wheres);
        }

        #endregion

        #region QUERY BUILDERS

        public virtual IMongoQuery GetStandardQuery()
        {
            var wheres = GetStandardWheres();
            if (wheres.Count == 0)
                return Query.Null;

            var query = wheres.Count == 1 ? wheres[0] : Query.And(GetStandardWheres());
            return query;
        }
        public virtual List<IMongoQuery> GetStandardWheres()
        {
            var query = new List<IMongoQuery> { };

            if (!string.IsNullOrWhiteSpace(Context.PracticeId))
                query.Add(Query<PracticeEntity>.EQ(x => x.PracticeId, Context.PracticeId));

            if (query.Count == 0)
                query.Add(Query.Null);

            return query;
        }

        #endregion
    }

    public abstract class EntityDataAccess<T> where T : Entity
    {
        protected DatabaseContext Context { get; private set; }

        public EntityDataAccess(MongoDatabase database, string collectionName)
        {
            _database = database;
            _collectionName = collectionName;
        }

        public void SetContext(DatabaseContext context)
        {
            Context = context;
        }

        #region READ

        public virtual IQueryable<T> Get()
        {
            return GetCollection().AsQueryable<T>();
        }
        public virtual MongoCursor<T> Get(string[] fields)
        {
            var cursor = GetCursor(null, SortBy.Null, fields);
            return cursor;
        }
        

        public virtual IQueryable<TOut> Get<TOut>() where TOut : T
        {
            return Get().OfType<TOut>();
        }

        public virtual MongoCursor<T> Get<TOut>(string[] fields) where TOut : T
        {
            return Get<TOut>(fields, null);
        }

        public MongoCursor<T> Get<TOut>(string[] fields, List<IMongoQuery> wheres) where TOut: T
        {
            var attributes = typeof(TOut).GetCustomAttributes(typeof(BsonDiscriminatorAttribute), false);
            var attr = attributes.FirstOrDefault() as BsonDiscriminatorAttribute;
            if (attr != null)
            {
                if (wheres == null)
                    wheres = new List<IMongoQuery>();

                wheres.Add(Query.EQ("_t", attr.Discriminator));
            }

            var cursor = GetCursor(Query.And(wheres), SortBy.Null, fields);
            return cursor;
        }


        public virtual T GetById(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
                return null;

            return Get().FirstOrDefault(x => x.Id == id);
        }

        public virtual TOut GetById<TOut>(string id) where TOut : T
        {
            if (string.IsNullOrWhiteSpace(id))
                return null;

            return Get<TOut>().FirstOrDefault(x => x.Id == id);
        }


        protected MongoCollection<T> GetCollection()
        {
            return _database.GetCollection<T>(_collectionName);
        }
        protected MongoCursor<T> GetCursor(IMongoQuery query, IMongoSortBy sort, string[] fields, int skip = 0, int take = 0)
        {
            var collection = GetCollection();
            var cursor = query == null ? collection.FindAll() : collection.Find(query);

            if (fields != null && fields.Length > 0)
                cursor.SetFields(fields);

            if (sort != SortBy.Null)
                cursor.SetSortOrder(sort);

            if (skip > 0)
                cursor = cursor.SetSkip(skip);

            if (take > 0)
                cursor = cursor.SetLimit(take);

            return cursor;
        }
        protected MongoCursor<T> GetCursor(IMongoQuery query, IMongoSortBy sort, string[] fields, out int count, int skip = 0, int take = 0)
        {
            var cursor = GetCursor(query, sort, fields, skip, take);
            count = (int)cursor.Count();
            return cursor;
        }

        #endregion

        #region CREATE

        public virtual WriteConcernResult Add(T entity)
        {
            return GetCollection().Insert(entity);
        }

        public virtual dynamic Add(IList<T> entities)
        {
            if (entities.Count == 0)
                return new List<WriteConcernResult>();

            return GetCollection().InsertBatch(entities);
        }

        #endregion

        #region DELETE

        public virtual WriteConcernResult Remove(T entity)
        {
            return Remove(entity.Id);
        }

        public virtual WriteConcernResult Remove(string id)
        {
            return GetCollection().Remove(Query<T>.EQ(x => x.Id, id));
        }

        #endregion

        #region UPDATE

        public virtual WriteConcernResult Update(T entity)
        {
            return GetCollection().Update(Query<T>.Where(x => x.Id == entity.Id),
                                          Update<T>.Replace(entity));
        }

        #endregion

        private MongoDatabase _database;
        private string _collectionName;
    }
}
