﻿using MongoDB.Driver;
using ProdoctCommon.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProdoctCommon.Data
{
    public class Users : EntityDataAccess<User>
    {
        public Users(MongoDatabase database)
            : base(database, "Users")
        {

        }

        public User GetByEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                return null;

            email = email.ToLower();
            return Get().FirstOrDefault(x => x.Email.ToLower() == email);
        }

        public void Process(SetStaffEmailResult result)
        {
            if (!result.Changed)
                return;

            if (result.OldUser != null)
            {
                Update(result.OldUser);
            }

            if (result.ChangedUser != null)
            {
                if (result.NewUserCreated)
                    Add(result.ChangedUser);
                else
                    Update(result.ChangedUser);
            }
        }
    }
}
