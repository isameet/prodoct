﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ProdoctCommon.Data
{
    /// <summary>
    /// Inherited properties include: Id
    /// </summary>
    public abstract class Entity
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
    }

    /// <summary>
    /// Inherited properties include: Id, PracticeId, CreatorId
    /// </summary>
    public abstract class PracticeEntity : Entity
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string PracticeId { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        public string CreatorId { get; set; }
    }
}
