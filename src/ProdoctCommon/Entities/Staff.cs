﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using ProdoctCommon.Extensions;
using ProdoctCommon.Helpers;
using System;

namespace ProdoctCommon.Data
{
    [BsonIgnoreExtraElements]
    [BsonDiscriminator("Staff", RootClass = true, Required = true)]
    [BsonKnownTypes(typeof(Doctor))]
    public class Staff : PracticeEntity
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string UserId { get; private set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string Mobile { get; set; }
        public string Email { get; private set; }
        public StaffRole Role { get; set; }
        public bool AllowLogin { get; private set; }
        public bool HasAccepted { get; private set; }
        public DateTime CreatedOn { get; set; }
        public DateTime AcceptedOn { get; private set; }
        public DateTime LastLogin { get; set; }
        public Privileges Privileges { get; set; }
        public CalendarSettings CalendarSettings { get; set; }
        public Staff()
        {
            Email = string.Empty;
            Privileges = Privileges.GetRegularPrivileges();
            CalendarSettings = CalendarSettings.GetDefault();
            Role = StaffRole.Staff;
        }

        public string GetLastLoginString(string timezone)
        {
            if (LastLogin.Year < 2000)
                return "Never";

            var lastLoginInZone = LastLogin.InTimezone(timezone);
            var nowInZone = DateTimeHelper.NowInZone(timezone);

            if (lastLoginInZone.Date == nowInZone.Date)
                return "Today";

            return lastLoginInZone.ToString(DateTimeFormats._8Mar87);
        }

        public void AcceptInvitation()
        {
            HasAccepted = true;
            AcceptedOn = DateTime.Now;
        }

        public SetAllowLoginResult SetAllowLogin(bool allowLogin, AppContext context)
        {
            var result = new SetAllowLoginResult();

            if (LoggedInUserChangingOwnStaffProperty(context))
                return result;

            if (!AtLeastOneActiveAdminCanStillLogin(allowLogin, context))
            {
                result.Error = true;
                result.Message = "At least one active admin login required";
                return result;
            }

            if (allowLogin == AllowLogin) //no change
                return result;

            if (!HasAccepted)
            {
                if (allowLogin)
                    result.LinkShouldBeSent = true;
                else
                    result.LinkShouldBeDeleted = true;
            }

            AllowLogin = allowLogin;
            result.Changed = true;
            return result;
        }

        /// <summary>
        /// Sets the Email and UserId. This does not make any changes to the database.
        /// </summary>
        /// <param name="email">New email</param>
        /// <param name="context">WebAppContext</param>
        /// <returns></returns>
        public SetStaffEmailResult SetEmail(string email, AppContext context)
        {
            var result = new SetStaffEmailResult();
            email = email.ToLower();

            if (NewStaffMemberBeingCreated(email, context, ref result))
            {
                result.Changed = true;
                return result;
            }

            if (LoggedInUserChangingOwnStaffProperty(context))
            {
                return result;
            }

            if (NoChangeInEmail(email))
            {
                return result;
            }

            if (AnotherStaffWithSameEmailExists(email, context, ref result))
            {
                return result;
            }

            ChangeUser(email, context, ref result);
            Email = email;
            HasAccepted = false;
            CreatedOn = DateTime.Now;
            UserId = result.ChangedUser.Id;

            result.Changed = true;
            return result;
        }

        public void UserEmailChanged(string email)
        {
            Email = email;
        }

        public void SetAdminPrivileges()
        {
            Privileges = Privileges.GetAdminPrivileges();
        }

        private bool LoggedInUserChangingOwnStaffProperty(AppContext context)
        {
            if (context.UserId == UserId)
                return true;

            return false;
        }

        private bool AnotherStaffWithSameEmailExists(string email, AppContext context, ref SetStaffEmailResult result)
        {
            var duplicate = context.DB.Staffs.GetByEmail(email);
            if (duplicate != null)
            {
                result.Error = true;
                result.Message = "Another staff member uses same Email Id";
                return true;
            }

            return false;
        }

        private bool NoChangeInEmail(string email)
        {
            if (Email.ToLower() == email)
                return true;

            return false;
        }

        private bool NewStaffMemberBeingCreated(string email, AppContext context, ref SetStaffEmailResult result)
        {
            if (string.IsNullOrWhiteSpace(Email))
            {
                ChangeUser(email, context, ref result);
                Email = email;
                return true;
            }

            return false;
        }

        private bool ChangeUser(string email, AppContext context, ref SetStaffEmailResult result)
        {
            result.ChangedUser = context.DB.Users.GetByEmail(email);
            result.OldUser = context.DB.Users.GetByEmail(Email);

            if (result.ChangedUser == null)
            {
                result.ChangedUser = new User(Name, email, Mobile, context.PracticeId);
                result.NewUserCreated = true;
            }
            else
            {
                result.ChangedUser.PracticeIds.Add(context.PracticeId);
            }

            if (result.OldUser != null)
            {
                result.OldUser.PracticeIds.Remove(context.PracticeId);
            }

            UserId = result.ChangedUser.Id;
            return true;
        }

        private bool AtLeastOneActiveAdminCanStillLogin(bool allowLogin, AppContext context)
        {
            //only if login is being disabled for an admin, check this function further
            if (!(!allowLogin && Privileges.IsAdmin))
                return true;

            var anotherAdmin = context.DB.Staffs.GetOneActiveAdminOtherThan(Id);
            if (anotherAdmin == null)
                return false;

            return true;
        }
    }

    public enum StaffRole
    {
        Doctor,
        Staff
    }

    [BsonIgnoreExtraElements]
    public class Privileges
    {
        public bool IsAdmin { get; set; }
        public bool AppointmentsRead { get; set; }
        public bool AppointmentsWrite { get; set; }
        public bool PatientsRead { get; set; }
        public bool PatientsWrite { get; set; }
        public bool InventoryRead { get; set; }
        public bool InventoryWrite { get; set; }
        public bool FinanceRead { get; set; }
        public bool FinanceWrite { get; set; }

        public static Privileges GetAdminPrivileges()
        {
            return new Privileges
            {
                IsAdmin = true,
                AppointmentsRead = true,
                AppointmentsWrite = true,
                PatientsRead = true,
                PatientsWrite = true,
                InventoryRead = true,
                InventoryWrite = true,
                FinanceRead = true,
                FinanceWrite = true
            };
        }

        public static Privileges GetRegularPrivileges()
        {
            return new Privileges
            {
                IsAdmin = false,
                AppointmentsRead = true,
                AppointmentsWrite = true,
                PatientsRead = true,
                PatientsWrite = true,
                InventoryRead = true,
                InventoryWrite = true,
                FinanceRead = false,
                FinanceWrite = false
            };
        }

        public static Privileges GetEmptyPrivileges()
        {
            return new Privileges();
        }
    }
}
