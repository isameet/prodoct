﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using ProdoctCommon.Helpers;
using System;
using System.Collections.Generic;

namespace ProdoctCommon.Data
{
    [BsonIgnoreExtraElements]
    public class Item : PracticeEntity
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string Manufacturer { get; set; }
        public string StockingUnit { get; set; }
        public int ReorderLevel { get; set; }
        public ItemType Type { get; set; }
        public ItemStock Stock { get; set; }

        public Item()
        {
            Stock = new ItemStock();
        }

        public enum ItemType
        {
            Drug
        }
    }

    [BsonIgnoreExtraElements]
    public class ItemStock
    {
        public int Total { get; set; }
        public int Available { get; set; }
        public int Expired { get; set; }
        public List<ItemBatch> Batches { get; set; }

        public ItemStock()
        {
            Batches = new List<ItemBatch>();
        }

        public void AddBatch(ItemBatch batch)
        {
            if (Batches == null) Batches = new List<ItemBatch>();

            Batches.Add(batch);
            Total += batch.Total;
            Available += batch.Total;
        }

        public void Consume(ItemBatch batch, int quantity)
        {
            batch.Available -= quantity;
            Available -= quantity;

            if (batch.Available < 0) batch.Available = 0;
            if (Available < 0) Available = 0;
        }

        public void RemoveBatch(ItemBatch batch)
        {
            if (Batches.Remove(batch))
            {
                Total -= batch.Total;
                Available -= batch.Available;
                Expired -= batch.Expired;
            }
        }

        public void Expire(ItemBatch batch)
        {
            batch.HasExpired = true;
            batch.Expired = batch.Available;
            batch.Available = 0;

            Available -= batch.Expired;
            Expired += batch.Expired;
            if (Available < 0) Available = 0;
        }
    }

    [BsonIgnoreExtraElements]
    public class ItemBatch : Entity
    {
        public string Name { get; set; }
        public string Vendor { get; set; }
        public DateTime Added { get; set; }
        public DateTime Expiry { get; set; }
        public int Total { get; set; }
        public int Available { get; set; }
        public int Expired { get; set; }
        public bool HasExpired { get; set; }
    }
}
