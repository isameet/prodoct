﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using ProdoctCommon.Helpers;
using System;

namespace ProdoctCommon.Data
{
    [BsonIgnoreExtraElements]
    [BsonDiscriminator("Income", RootClass = true, Required = true)]
    [BsonKnownTypes(typeof(Fees))]
    public class Income : PracticeEntity
    {
        public string Category { get; set; }
        public DateTime Date { get; set; }

        [BsonRepresentation(BsonType.Double)]
        public decimal Amount { get; set; }

        public string Mode { get; set; }

        public string Notes { get; set; }

        public Income()
        {
            
        }
    }

    [BsonDiscriminator("Fees")]
    public class Fees : Income
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string PatientId { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        public string AppointmentId { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        public string DoctorId { get; set; }

        public Fees() : base()
        {
            Category = "Fees";
        }

        public Fees(Income income) : this()
        {
            Id = income.Id;
            Category = income.Category;
            Date = income.Date;
            Amount = income.Amount;
            Mode = income.Mode;
            Notes = income.Notes;
            CreatorId = income.CreatorId;
            PracticeId = income.PracticeId;
        }
    }
}
