﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using ProdoctCommon.Helpers;
using System;

namespace ProdoctCommon.Data
{
    public class Expense : PracticeEntity
    {
        public string Category { get; set; }
        public DateTime Date { get; set; }

        [BsonRepresentation(BsonType.Double)]
        public decimal Amount { get; set; }

        public string Mode { get; set; }

        public string Notes { get; set; }

        public Expense()
        {
            
        }
    }
}
