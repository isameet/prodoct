﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace ProdoctCommon.Data
{
    public class Practice : Entity
    {
        public string Name { get; set; }

        public string Email { get; set; }

        public string Website { get; set; }

        public string ContactNumber { get; set; }

        public string Timezone { get; set; }

        public List<string> IncomeCategories { get; set; }
        public List<string> ExpenseCategories { get; set; }

        public List<string> TransactionModes { get; set; }

        public Practice()
        {
            IncomeCategories = GetDefaultIncomeCategories();
            ExpenseCategories = GetDefaultExpenseCategories();
            TransactionModes = GetDefaultTransactionModes();
        }

        private List<string> GetDefaultIncomeCategories()
        {
            return new List<string>
            {
                "Donation",
                "Fees"
            };
        }

        private List<string> GetDefaultExpenseCategories()
        {
            return new List<string>
            {
                "Electricity Bill",
                "Telephone Bill",
                "Water Bill",
                "Salary",
                "Lab Charges",
                "Medicines",
                "Materials",
                "Personal",
                "Miscellaneous"
            };
        }

        private List<string> GetDefaultTransactionModes()
        {
            return new List<string>
            {
                "Cash", 
                "Cheque",
                "ECS",
                "Card",
                "Netbanking",
                "Other"
            };
        }
    }
}
