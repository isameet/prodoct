﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace ProdoctCommon.Data
{
    [BsonIgnoreExtraElements]
    public class Appointment : PracticeEntity
    {
        public DateTime Date { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        public string PatientId { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        public string DoctorId { get; set; }

        public string Notes { get; set; }

        public AppointmentStatus Status { get; set; }

        public Appointment()
        {

        }

        public bool IsCanceled()
        {
            return Status == AppointmentStatus.Canceled;
        }

        public void IsCanceled(bool isCanceled)
        {
            if (isCanceled)
            {
                Status = AppointmentStatus.Canceled;
                return;
            }

            Status = AppointmentStatus.Confirmed;
        }
    }

    [BsonIgnoreExtraElements]
    public class AppointmentSummary
    {
        public DateTime Date { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        [BsonIgnoreIfNull]
        public string DoctorId { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        [BsonIgnoreIfNull]
        public string PatientId { get; set; }
    }

    public enum AppointmentStatus
    {
        Confirmed,
        Canceled
    }

    [BsonIgnoreExtraElements]
    public class CalendarSettings
    {
        public bool ShowCanceledAppointments { get; set; }

        public CalendarSettings()
        {
            ShowCanceledAppointments = true;
        }

        public static CalendarSettings GetDefault()
        {
            return new CalendarSettings
            {
                ShowCanceledAppointments = true
            };
        }
    }
}
