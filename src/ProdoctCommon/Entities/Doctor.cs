﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace ProdoctCommon.Data
{
    [BsonDiscriminator("Doctor")]
    public class Doctor : Staff
    {
        public string CalendarColor { get; set; }

        public Doctor() : base()
        {
            Role = StaffRole.Doctor;
        }
    }
}
