﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using ProdoctCommon.Extensions;
using ProdoctCommon.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProdoctCommon.Data
{
    public class User : Entity
    {
        public string Name { get; set; }
        public string Mobile { get; set; }
        public string Email { get; private set; }
        public string PasswordHash { get; private set; }
        public bool IsVerified { get; private set; }
        public DateTime CreatedOn { get; set; }
        public DateTime VerifiedOn { get; private set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string DefaultPracticeId { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public List<string> PracticeIds { get; set; }

        public User()
        {
            PracticeIds = new List<string>();
        }

        public User(string name, string email, string mobile, string practiceId)
        {
            Name = name;
            Email = email;
            Mobile = mobile;
            IsVerified = false;
            CreatedOn = DateTime.Now;
            DefaultPracticeId = practiceId;
            PracticeIds = new List<string> { practiceId };
        }

        public void Verify()
        {
            IsVerified = true;
            VerifiedOn = DateTime.Now;
        }

        public void AddPractice(string practiceId)
        {
            if (!PracticeIds.Contains(practiceId))
                PracticeIds.Add(practiceId);

            if (string.IsNullOrWhiteSpace(DefaultPracticeId))
                DefaultPracticeId = practiceId;
        }

        public void RemovePractice(string practiceId)
        {
            PracticeIds.Remove(practiceId);
            if(DefaultPracticeId == practiceId)
                DefaultPracticeId = PracticeIds.FirstOrDefault();
        }

        public void SetPassword(string password)
        {
            PasswordHash = AuthenticationHelper.HashPassword(password);
        }

        public SetUserEmailResult SetEmail(string email, AppContext context)
        {
            var result = new SetUserEmailResult();
            email = email.ToLower();

            if (NoChangeInEmail(email))
                return result;

            if (EmailAlreadyInUse(email, context))
            {
                result.Error = true;
                result.Message = "Email Id already in use";
                return result;
            }

            ProcessStaffsAffected(email, context, ref result);

            Email = email;
            IsVerified = false;
            result.Changed = true;
            return result;
        }

        private bool EmailAlreadyInUse(string email, AppContext context)
        {
            var duplicate = context.DB.Users.GetByEmail(email);
            if (duplicate != null)
                return true;

            return false;
        }

        private void ProcessStaffsAffected(string email, AppContext context, ref SetUserEmailResult result)
        {
            result.StaffsAffected = context.DB.Staffs.GetAllByUserId(Id);
            foreach (var staff in result.StaffsAffected)
            {
                staff.UserEmailChanged(email);
            }
        }

        private bool NoChangeInEmail(string email)
        {
            return Email == email;
        }
    }
}
