﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using ProdoctCommon.Helpers;
using System;

namespace ProdoctCommon.Data
{
    [BsonIgnoreExtraElements]
    public class Patient : PracticeEntity
    {
        public string Name { get; private set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Locality { get; set; }
        public string Notes { get; set; }

        public DateTime CreatedOn { get; set; }

        public AppointmentSummary LastAppointment { get; set; }

        public Patient()
        {
            LastAppointment = new AppointmentSummary();
            CreatedOn = DateTime.Now;
        }

        public void SetName(string name)
        {
            Name = StringHelper.FirstLetterToUpper(name);
        }
    }
}
