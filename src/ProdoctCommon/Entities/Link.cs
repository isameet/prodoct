﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using ProdoctCommon.Helpers;
using System;
using System.Collections.Generic;

namespace ProdoctCommon.Data
{
    public class Link : PracticeEntity
    {
        public string Email { get; set; }
        public Dictionary<string, string> Globals { get; set; }
        public string Code { get; set; }
        public bool IsConsumed { get; private set; }
        public bool ToBeSent { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime SentOn { get; set; }
        public DateTime ConsumedOn { get; private set; }

        [BsonRepresentation(BsonType.ObjectId)]
        public string StaffId { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        public string UserId { get; set; }

        public LinkType Type { get; set; }

        public StandardResult Result { get; set; }

        public Link()
        {
            Id = ObjectIdHelper.GenerateNewIdString();
            Code = AuthenticationHelper.GenerateRandomToken();
            CreatedOn = DateTime.Now;
            Globals = new Dictionary<string, string>();
            Result = new StandardResult();
        }

        public void Consume()
        {
            IsConsumed = true;
            ConsumedOn = DateTime.Now;
        }
    }

    public enum LinkType
    {
        UserEmailVerify,
        StaffInvitation,
        ChangePassword
    }
}
