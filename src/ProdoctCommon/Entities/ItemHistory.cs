﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using ProdoctCommon.Helpers;
using System;
using System.Collections.Generic;

namespace ProdoctCommon.Data
{
    [BsonIgnoreExtraElements]
    public class ItemHistory : PracticeEntity
    {
        private Item item;
        private ItemBatch batch;

        [BsonRepresentation(BsonType.ObjectId)]
        public string ItemId { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        public string BatchId { get; set; }

        public TransactionType Type { get; set; }

        public string Description { get; set; }

        public DateTime Date { get; set; }

        public int Quantity { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        public string PatientId { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        public string DoctorId { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        public string AppointmentId { get; set; }

        public ItemHistory()
        {
        }

        public ItemHistory(Item item, ItemBatch batch)
        {
            Id = ObjectIdHelper.GenerateNewIdString();
            ItemId = item.Id;
            BatchId = batch.Id;
            PracticeId = item.PracticeId;
        }

        public enum TransactionType
        {
            Added,
            Consumed
        }

        public static ItemHistory Expired(Item item, ItemBatch batch)
        {
            return new ItemHistory(item, batch)
            {
                Type = ItemHistory.TransactionType.Consumed,
                Date = DateTime.Now.Date,
                Description = "Expired",
                Quantity = batch.Available
            };
        }
    }
}
